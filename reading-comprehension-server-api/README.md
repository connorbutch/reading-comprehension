# reading-comprehension-server-api
## Motivation
Writing an api that is true to a specification is difficult and error prone.  This module contains a (jaxrs) specification
that is generated directly from the open api spec (using a gradle plugin) to ensure that it is accurate.

## Functionality
This project offers an interface that can be implemented by any number of jaxrs implementations such as resteasy, quarkus, micronaut, etc.
It also ensures that the generated interface can easily be overwritten when specification changes, without overwriting
implementation code.

## Technologies Involved
This project uses various technologies, some of which include:
- java (11)
- gradle
- open api specification
- code generation
- jaxrs


## Usage Instructions
In order to utilize this module, run a "gradle clean build" from the parent directory (or this one).
This will generate the output to the build directory of this project, which we do not check in since we do not
want to see changes for every time code is modified (generation timestamp varies, so git detecs it as a difference).

If you would like to verify that the generation worked, first run the gradle build and ensure it says build succeeded, then visit the build/generates/sources directory under this module.
If you see com/connor/reading/.... java classes, then the command worked.


## General Notes
As always, feedback is greatly appreciated, so feel free to visit my youtube channel, or email me with feedback.
