# reading-comprehension-client
## Motivation
Connecting, and testing, webservices can be difficult.  This module provides a standardized way of
acting as a client to our webservice that helps ensure our tests use the same code as our applications
and mitigate the chance of errors that can occur when writing code by hand.

## Functionality
This project offers a standardized, generated client that can be used to both test our service,
and to call it from other applications.

## Technologies Involved
This project uses various technologies, some of which include:
- java (11)
- gradle
- open api specification
- code generation

## Usage Instructions
In order to utilize this module, run a "gradle clean build" from the parent directory (or this one).
This will generate the output to the build directory of this project, which we do not check in since we do not
want to see changes for every time code is modified (generation timestamp varies, so git detecs it as a difference).

If you would like to verify that the generation worked, first run the gradle build and ensure it says build succeeded, then visit the build/generates/sources directory under this module.
If you see com/connor/reading/client/.... java classes, then the command worked.


## General Notes
As always, feedback is greatly appreciated, so feel free to visit my youtube channel, or email me with feedback.
