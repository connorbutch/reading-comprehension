# ErrorsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getErrorById**](ErrorsApi.md#getErrorById) | **GET** /errors/{errorId} | This operation allows to retrieve more information about an error based on the (unique) error id



## getErrorById

> DetailedErrorInfo getErrorById(errorId)

This operation allows to retrieve more information about an error based on the (unique) error id

### Example

```java
// Import classes:
import com.connor.reading.ApiClient;
import com.connor.reading.ApiException;
import com.connor.reading.Configuration;
import com.connor.reading.models.*;
import com.connor.reading.client.ErrorsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        ErrorsApi apiInstance = new ErrorsApi(defaultClient);
        Integer errorId = 56; // Integer | The particular errorId to search on
        try {
            DetailedErrorInfo result = apiInstance.getErrorById(errorId);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling ErrorsApi#getErrorById");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **errorId** | **Integer**| The particular errorId to search on |

### Return type

[**DetailedErrorInfo**](DetailedErrorInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | The detailed error info with the given id |  * x-request-id -  <br>  * x-trace-id -  <br>  |
| **400** | This means that the error id passed was invalid |  -  |
| **404** | This means that no assessment exists for the error id passed |  -  |
| **0** | Unknown error |  -  |

