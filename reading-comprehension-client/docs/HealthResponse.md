

# HealthResponse

Response for a liveliness health check
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**healthStatus** | **String** | Will always be ok, for now. | 



