# HealthApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**livelinessCheck**](HealthApi.md#livelinessCheck) | **GET** /health/liveliness | Check the liveliness of the application
[**readinessCheck**](HealthApi.md#readinessCheck) | **GET** /health/readiness | Check the readiness of an app.



## livelinessCheck

> HealthResponse livelinessCheck()

Check the liveliness of the application

This will be invoked periodically to determine if a pod should service traffic.  However, it should not be invoked externally

### Example

```java
// Import classes:
import com.connor.reading.ApiClient;
import com.connor.reading.ApiException;
import com.connor.reading.Configuration;
import com.connor.reading.models.*;
import com.connor.reading.client.HealthApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        HealthApi apiInstance = new HealthApi(defaultClient);
        try {
            HealthResponse result = apiInstance.livelinessCheck();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling HealthApi#livelinessCheck");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**HealthResponse**](HealthResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | A health response.  Assuming you get a 200, the body can be ignored.  This must be a GET request (not a HEAD request) for kubernetes monitoring. |  * x-request-id -  <br>  * x-trace-id -  <br>  |
| **502** | Service unavailable |  * x-request-id -  <br>  * x-trace-id -  <br>  |
| **0** | Unknown error |  * x-request-id -  <br>  * x-trace-id -  <br>  |


## readinessCheck

> ReadinessHealthResponse readinessCheck(xTimeoutValue)

Check the readiness of an app.

Check the readiness of the application (used for kubernetes).  This should likely not be invoked by anyone but kubernetes itself (or potentially monitoring from the kubelet)

### Example

```java
// Import classes:
import com.connor.reading.ApiClient;
import com.connor.reading.ApiException;
import com.connor.reading.Configuration;
import com.connor.reading.models.*;
import com.connor.reading.client.HealthApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        HealthApi apiInstance = new HealthApi(defaultClient);
        Integer xTimeoutValue = 56; // Integer | The max allowed time (in milliseconds) before a health check request should determine a service is unavailable.
        try {
            ReadinessHealthResponse result = apiInstance.readinessCheck(xTimeoutValue);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling HealthApi#readinessCheck");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xTimeoutValue** | **Integer**| The max allowed time (in milliseconds) before a health check request should determine a service is unavailable. | [optional]

### Return type

[**ReadinessHealthResponse**](ReadinessHealthResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | A health response showing the status of dependencies.  When a 200 is returned, the application can successfully connect to other services/dependencies |  * x-request-id -  <br>  * x-trace-id -  <br>  |
| **502** | Service  -- this means at least one of the downstream dependencies was down, or we timed out.  The list can be parsed to examine which connections are failing |  * x-request-id -  <br>  * x-trace-id -  <br>  |
| **0** | Unknown error |  * x-request-id -  <br>  * x-trace-id -  <br>  |

