

# DownstreamDependency

Indicates whether or not we are able to connect to a downstream dependency
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | A unique, and descriptive name for this downstream dependency | 
**isAvailable** | **Boolean** | A value indicating whether or not we could connect to this dependency.  A timeout is considered false for now |  [optional]



