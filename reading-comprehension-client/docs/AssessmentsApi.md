# AssessmentsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAssessment**](AssessmentsApi.md#createAssessment) | **POST** /assessments | This operation allows to add an assessment for a given book
[**getAssessmentByIsbn**](AssessmentsApi.md#getAssessmentByIsbn) | **GET** /assessments/{isbn} | This operation allows to retrieve an assessment by isbn
[**getAssessments**](AssessmentsApi.md#getAssessments) | **GET** /assessments | This operation allows to retrieve various assessments
[**getQuestionsForAssessment**](AssessmentsApi.md#getQuestionsForAssessment) | **GET** /assessments/{isbn}/questions | This operation allows to retrieve the questions for a given assessment



## createAssessment

> Assessment createAssessment(assessment)

This operation allows to add an assessment for a given book

This allows a teacher to create an assessment.

### Example

```java
// Import classes:
import com.connor.reading.ApiClient;
import com.connor.reading.ApiException;
import com.connor.reading.Configuration;
import com.connor.reading.models.*;
import com.connor.reading.client.AssessmentsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        AssessmentsApi apiInstance = new AssessmentsApi(defaultClient);
        Assessment assessment = new Assessment(); // Assessment | The assessment to add
        try {
            Assessment result = apiInstance.createAssessment(assessment);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AssessmentsApi#createAssessment");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assessment** | [**Assessment**](Assessment.md)| The assessment to add |

### Return type

[**Assessment**](Assessment.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | The assessment was created successfully |  * x-request-id -  <br>  * x-trace-id -  <br>  |
| **400** | This means that there was an invalid assessment passed |  -  |
| **409** | This means that the user was attempting to create an assessment for an isbn that already has an isbn registered |  -  |
| **0** | Unknown error |  -  |


## getAssessmentByIsbn

> Assessment getAssessmentByIsbn(isbn)

This operation allows to retrieve an assessment by isbn

This allows a user to retrieve an assessment for a specific isbn

### Example

```java
// Import classes:
import com.connor.reading.ApiClient;
import com.connor.reading.ApiException;
import com.connor.reading.Configuration;
import com.connor.reading.models.*;
import com.connor.reading.client.AssessmentsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        AssessmentsApi apiInstance = new AssessmentsApi(defaultClient);
        Long isbn = 56L; // Long | The particular isbn to search on
        try {
            Assessment result = apiInstance.getAssessmentByIsbn(isbn);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AssessmentsApi#getAssessmentByIsbn");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isbn** | **Long**| The particular isbn to search on |

### Return type

[**Assessment**](Assessment.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | The assessment with the given isbn |  * x-request-id -  <br>  * x-trace-id -  <br>  * x-total-assessments -  <br>  |
| **400** | This means that the isbn passed was invalid |  -  |
| **404** | This means that no assessment exists for the isbn passed |  -  |
| **0** | Unknown error |  -  |


## getAssessments

> List&lt;Assessment&gt; getAssessments(limit, offset)

This operation allows to retrieve various assessments

This allows u user to get assessments.  The list should always be returned in the same order, and by using the limit and offset query parameters, pagination can be achieved (with the added functionality of a total number of assessments header indicating the total number of assessments)

### Example

```java
// Import classes:
import com.connor.reading.ApiClient;
import com.connor.reading.ApiException;
import com.connor.reading.Configuration;
import com.connor.reading.models.*;
import com.connor.reading.client.AssessmentsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        AssessmentsApi apiInstance = new AssessmentsApi(defaultClient);
        Integer limit = 56; // Integer | A limit on the max number of assessments returned.  If the limit exceeds a certain maximum, then only a certain amount will be passed back and the x-maximum-limit header value will indicate this.
        Integer offset = 56; // Integer | The offset used to retrieve assessments.  This can be used with the limit query parameter to achieve pagination.
        try {
            List<Assessment> result = apiInstance.getAssessments(limit, offset);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AssessmentsApi#getAssessments");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**| A limit on the max number of assessments returned.  If the limit exceeds a certain maximum, then only a certain amount will be passed back and the x-maximum-limit header value will indicate this. | [optional]
 **offset** | **Integer**| The offset used to retrieve assessments.  This can be used with the limit query parameter to achieve pagination. | [optional]

### Return type

[**List&lt;Assessment&gt;**](Assessment.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | A list of assessments, based on the limit and offset specified. |  * x-request-id -  <br>  * x-trace-id -  <br>  * x-total-assessments -  <br>  |
| **400** | This means that there was an invalid limit or offset (or both) |  -  |
| **0** | Unknown error |  -  |


## getQuestionsForAssessment

> List&lt;Question&gt; getQuestionsForAssessment(isbn)

This operation allows to retrieve the questions for a given assessment

This allows a user to retrieve questions for a specific assessment

### Example

```java
// Import classes:
import com.connor.reading.ApiClient;
import com.connor.reading.ApiException;
import com.connor.reading.Configuration;
import com.connor.reading.models.*;
import com.connor.reading.client.AssessmentsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        AssessmentsApi apiInstance = new AssessmentsApi(defaultClient);
        Long isbn = 56L; // Long | The isbn of the assessment
        try {
            List<Question> result = apiInstance.getQuestionsForAssessment(isbn);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AssessmentsApi#getQuestionsForAssessment");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isbn** | **Long**| The isbn of the assessment |

### Return type

[**List&lt;Question&gt;**](Question.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | The questions for this particular assessment |  * x-request-id -  <br>  * x-trace-id -  <br>  * x-total-assessments -  <br>  |
| **400** | This means that the isbn passed was invalid |  -  |
| **404** | This means that no assessment exists for the isbn passed |  -  |
| **0** | Unknown error |  -  |

