

# Question

A question is used to determine if a student understands a certain portion of the book
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**questionId** | **Long** | A unique id for this question |  [readonly]
**questionText** | **String** | The text for this question | 
**correctAnswer** | **String** | The correct answer to this question | 
**incorrectAnswers** | **List&lt;String&gt;** |  | 



