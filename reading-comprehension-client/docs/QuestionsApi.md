# QuestionsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createQuestion**](QuestionsApi.md#createQuestion) | **POST** /assessments/{isbn}/questions | This operation allows to add an assessment for a given book
[**getQuestionsForAssessment**](QuestionsApi.md#getQuestionsForAssessment) | **GET** /assessments/{isbn}/questions | This operation allows to retrieve the questions for a given assessment



## createQuestion

> Question createQuestion(isbn, question)

This operation allows to add an assessment for a given book

This allows a teacher to add a question for a given assessment

### Example

```java
// Import classes:
import com.connor.reading.ApiClient;
import com.connor.reading.ApiException;
import com.connor.reading.Configuration;
import com.connor.reading.models.*;
import com.connor.reading.client.QuestionsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        QuestionsApi apiInstance = new QuestionsApi(defaultClient);
        Long isbn = 56L; // Long | The isbn of the assessment
        Question question = new Question(); // Question | The question to add
        try {
            Question result = apiInstance.createQuestion(isbn, question);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling QuestionsApi#createQuestion");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isbn** | **Long**| The isbn of the assessment |
 **question** | [**Question**](Question.md)| The question to add |

### Return type

[**Question**](Question.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | The question was created successfully |  * x-request-id -  <br>  * x-trace-id -  <br>  |
| **400** | This means that there was an invalid assessment passed |  -  |
| **404** | This means that the user was attempting to add a question for a book that is not yet registered |  -  |
| **0** | Unknown error |  -  |


## getQuestionsForAssessment

> List&lt;Question&gt; getQuestionsForAssessment(isbn)

This operation allows to retrieve the questions for a given assessment

This allows a user to retrieve questions for a specific assessment

### Example

```java
// Import classes:
import com.connor.reading.ApiClient;
import com.connor.reading.ApiException;
import com.connor.reading.Configuration;
import com.connor.reading.models.*;
import com.connor.reading.client.QuestionsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        QuestionsApi apiInstance = new QuestionsApi(defaultClient);
        Long isbn = 56L; // Long | The isbn of the assessment
        try {
            List<Question> result = apiInstance.getQuestionsForAssessment(isbn);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling QuestionsApi#getQuestionsForAssessment");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isbn** | **Long**| The isbn of the assessment |

### Return type

[**List&lt;Question&gt;**](Question.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | The questions for this particular assessment |  * x-request-id -  <br>  * x-trace-id -  <br>  * x-total-assessments -  <br>  |
| **400** | This means that the isbn passed was invalid |  -  |
| **404** | This means that no assessment exists for the isbn passed |  -  |
| **0** | Unknown error |  -  |

