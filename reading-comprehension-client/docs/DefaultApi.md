# DefaultApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAssessment**](DefaultApi.md#createAssessment) | **POST** /assessments | This operation allows to add an assessment for a given book



## createAssessment

> Assessment createAssessment(assessment)

This operation allows to add an assessment for a given book

This allows a teacher to create an assessment.

### Example

```java
// Import classes:
import com.connor.reading.ApiClient;
import com.connor.reading.ApiException;
import com.connor.reading.Configuration;
import com.connor.reading.models.*;
import com.connor.reading.client.DefaultApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        DefaultApi apiInstance = new DefaultApi(defaultClient);
        Assessment assessment = new Assessment(); // Assessment | The assessment to add
        try {
            Assessment result = apiInstance.createAssessment(assessment);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling DefaultApi#createAssessment");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assessment** | [**Assessment**](Assessment.md)| The assessment to add |

### Return type

[**Assessment**](Assessment.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | The assessment was created successfully |  * x-request-id -  <br>  * x-trace-id -  <br>  |
| **400** | This means that there was an invalid assessment passed |  -  |
| **409** | This means that the user was attempting to create an assessment for an isbn that already has an isbn registered |  -  |
| **0** | Unknown error |  -  |

