

# Assessment

An assessment for a single book (where a book is determined by an isbn).  An assessment can have one or more questions associated with it
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isbn** | **Long** | The isbn (10) for the assessment | 
**numberOfPoints** | **Float** | The number of points assigned to this assessment.  Generally speaking, the higher the number of points, the longer/harder the book is to read. | 
**readingLevel** | **Float** | The reading level (on a scale of 0.0-12.0) for the given assessment.  The higher the reading level, the harder the book is to copmrehend (note, this is not correlated to length of book whatsoever). | 
**title** | **String** | The title of the book. | 
**authorFirstName** | **String** | The first name of the author of the book | 
**authorLastName** | **String** | The last name of the author of the book | 
**createdTeacherId** | **Integer** | The id of the teacher that wrote the assessment for this book.  It should link to the id assigned in the teacher service. | 



