

# ErrorReason

An error reason is a single, unique reason for an error.  Please note that in the case of multiple errors (usually with user input), there can be multiple error reasons returned In the future, there will be an endpoint given to list these errors, and to get more information on a specific error
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorId** | **Integer** | A unique id for an error reason.  This can be used in a get request to find more information | 
**explanation** | **String** | A brief explanation of the error.  Please note that this may change, and the errorId should be used to determine information | 



