# LivelinessApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**livelinessCheck**](LivelinessApi.md#livelinessCheck) | **GET** /health/liveliness | Check the liveliness of the application



## livelinessCheck

> HealthResponse livelinessCheck()

Check the liveliness of the application

This will be invoked periodically to determine if a pod should service traffic.  However, it should not be invoked externally

### Example

```java
// Import classes:
import com.connor.reading.ApiClient;
import com.connor.reading.ApiException;
import com.connor.reading.Configuration;
import com.connor.reading.models.*;
import com.connor.reading.client.LivelinessApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        LivelinessApi apiInstance = new LivelinessApi(defaultClient);
        try {
            HealthResponse result = apiInstance.livelinessCheck();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling LivelinessApi#livelinessCheck");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**HealthResponse**](HealthResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | A health response.  Assuming you get a 200, the body can be ignored.  This must be a GET request (not a HEAD request) for kubernetes monitoring. |  * x-request-id -  <br>  * x-trace-id -  <br>  |
| **502** | Service unavailable |  * x-request-id -  <br>  * x-trace-id -  <br>  |
| **0** | Unknown error |  * x-request-id -  <br>  * x-trace-id -  <br>  |

