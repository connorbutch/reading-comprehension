

# ReadinessHealthResponse

This is a health response for a readiness health check.  It contains a downstream dependency for each component this depends on (such as a database, system property, or external webservice) and an overall status.  If one or more dependencies are unreachable, or timeout, then the overall status will be down, otherwise it will show as up.
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isAvailable** | **Boolean** | Denotes whether the overall service is available.  Is true if all checks are available.  if even one dependency is unavailable (for either error or timeout) this will be false | 
**availableDependencies** | [**List&lt;DownstreamDependency&gt;**](DownstreamDependency.md) | This array contains the downstream dependencies that are available.  Pulled into a separate array to make parsing easier | 
**unavailableDependencies** | [**List&lt;DownstreamDependency&gt;**](DownstreamDependency.md) | This array contains the downstream dependencies that are NOT available (due to either timeout or error).  Pulled into a separate array to make parsing easier | 



