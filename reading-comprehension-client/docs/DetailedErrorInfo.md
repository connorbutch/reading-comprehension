

# DetailedErrorInfo

This provides additional information about an error that occurred.  Please note that this gives one example of what could be causing the issue (and your exact request may be slightly different).  However, this can be used to get more information about a failure
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorId** | **Integer** |  | 
**exampleUrl** | **String** | This is an example example url, that may be incorrect.  Compare with the example correct url below | 
**exampleRequestBody** | **String** | This is an example of an incorrect  | 
**exampleRequestMethod** | **String** | The http method | 
**exampleCorrectUrl** | **String** |  | 
**exampleCorrectRequestBody** | **String** |  | 
**errorReason** | [**ErrorReason**](ErrorReason.md) |  | 



