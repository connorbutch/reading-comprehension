#This script uses defaults for the readiness endpoint and timeout, but can be overwritten by passing the -t or -r flags (respectively)
#For example, ./wait-for-it-2.sh -t 10 -r localhost:8080/health/readiness will run the check with a timeout of 10 seconds (instead of default 90) and a url to check of localhost...
MAX_NUMBER_OF_SECONDS_BEFORE_FAIL=90
READINESS_ENDPOINT=192.168.0.8:8080/health/readiness #0.0.0.0:8080/health/readiness works for local and docker:8080/health/readiness  works for gitlab ci

while getopts t:r: option
do
case "${option}"
in
t) MAX_NUMBER_OF_SECONDS_BEFORE_FAIL=${OPTARG};;
r) READINESS_ENDPOINT=${OPTARG};;
*)
esac
done

OK_STATUS_CODE=200

HTTP_STATUS_CODE=0
NUM_SECONDS=0
while [ $HTTP_STATUS_CODE -ne $OK_STATUS_CODE ]
do
HTTP_STATUS_CODE=$(curl -s -o /dev/null -w "%{http_code}"  $READINESS_ENDPOINT )
echo "Made request to readiness endpoint ($READINESS_ENDPOINT) and received status code of $HTTP_STATUS_CODE"
if [ "$HTTP_STATUS_CODE" -ne "$OK_STATUS_CODE" ]; then
  curl -v $READINESS_ENDPOINT
fi
sleep 1
((NUM_SECONDS++))
#NOTE: starting the container takes less than a hundredth of a second, it is just gitlab itself is slow on the community edition, that is why I set the timeout to one minute for this
if [ "$NUM_SECONDS" -eq "$MAX_NUMBER_OF_SECONDS_BEFORE_FAIL" ]; then
  echo "Failed to start application in $MAX_NUMBER_OF_SECONDS_BEFORE_FAIL seconds.  We will now exit with a failure status"
  exit 1
fi
done ;

echo "Successfully reached application after $NUM_SECONDS seconds!  The maximum wait time would have been $MAX_NUMBER_OF_SECONDS_BEFORE_FAIL"