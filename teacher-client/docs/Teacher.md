

# Teacher

This represents a teacher
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**teacherId** | **Long** | A unique id for this teacher |  [readonly]
**firstName** | **String** | The teacher&#39;s first name | 
**lastName** | **String** | The teacher&#39;s last name | 
**gradeTaught** | [**BigDecimal**](BigDecimal.md) | The grade this teacher currently teacher (use 0 for kindergarden) | 



