

# ErrorResponse

Response for any error encountered by the application
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorReasons** | [**List&lt;ErrorReason&gt;**](ErrorReason.md) | A list of reasons why the request failed.  This may have only one error reason, or it could be multiple (in the case the client submits bad data) | 
**timestamp** | [**OffsetDateTime**](OffsetDateTime.md) | The timestamp of when the error occurred, generated on the server-side | 



