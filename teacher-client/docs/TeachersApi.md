# TeachersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTeacherById**](TeachersApi.md#getTeacherById) | **GET** /teachers/{teacherId} | This operation allows to retrieve a teacher by his/her (unique) teacher id



## getTeacherById

> Teacher getTeacherById(teacherId)

This operation allows to retrieve a teacher by his/her (unique) teacher id

This operation allows to retrieve a teacher by his/her (unique) teacher id

### Example

```java
// Import classes:
import com.connor.teacher.ApiClient;
import com.connor.teacher.ApiException;
import com.connor.teacher.Configuration;
import com.connor.teacher.models.*;
import com.connor.teacher.client.TeachersApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        TeachersApi apiInstance = new TeachersApi(defaultClient);
        Long teacherId = 56L; // Long | The particular teacher id to search on
        try {
            Teacher result = apiInstance.getTeacherById(teacherId);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling TeachersApi#getTeacherById");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **teacherId** | **Long**| The particular teacher id to search on |

### Return type

[**Teacher**](Teacher.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | The teacher with the given teacher id |  * x-request-id -  <br>  * x-trace-id -  <br>  |
| **400** | This means that the teacher id passed was invalid |  -  |
| **404** | This means that no teacher exists with the given teacher id |  -  |
| **0** | Unknown error |  -  |

