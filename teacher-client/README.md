# Teacher client
## Motivation
This is a generated client for the teacher service.  This gives an example of an application acting as a client to other services
as well as providing RESTful functionality of its own. 
## Functionality

## Technologies Involved
- gradle
- yaml downloader
- open api codegeneration

## Usage Instructions
Include this module as a (implementation) dependency to your project

## General Notes
As always, feedback is greatly appreciated, so feel free to visit my youtube channel, or email me with feedback.
