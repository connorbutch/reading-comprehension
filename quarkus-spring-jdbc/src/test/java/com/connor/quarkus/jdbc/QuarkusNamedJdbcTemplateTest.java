package com.connor.quarkus.jdbc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class QuarkusNamedJdbcTemplateTest {
    @Mock
    private QuarkusJdbcTemplate quarkusJdbcTemplate;
    @InjectMocks
    private QuarkusNamedJdbcTemplate quarkusNamedJdbcTemplate;

    @Captor
    private ArgumentCaptor<String> createdSqlCaptor;

    @Test
    public void whenGetSubstituteParameters() {
        final String expectedSql = "SELECT ISBN,AUTHOR_FIRST_NAME,AUTHOR_LAST_NAME,TITLE,NUMBER_OF_POINTS,IS_VERIFIED,READING_LEVEL,CREATED_TEACHER_ID FROM ASSESSMENT LIMIT 3 , 2 ORDER BY ISBN ASC";
        Map<String, ?> parameters = Map.of("LIMIT", 3, "OFFSET", 2);
        quarkusNamedJdbcTemplate.query("SELECT ISBN,AUTHOR_FIRST_NAME,AUTHOR_LAST_NAME,TITLE,NUMBER_OF_POINTS,IS_VERIFIED,READING_LEVEL,CREATED_TEACHER_ID FROM ASSESSMENT LIMIT :LIMIT , :OFFSET ORDER BY ISBN ASC", parameters, mock(QuarkusRowMapper.class));
        verify(quarkusJdbcTemplate, times(1)).query(createdSqlCaptor.capture(), any(QuarkusRowMapper.class));
       assertEquals("Created sql should have values substituted",expectedSql, createdSqlCaptor.getValue());
    }

    @Test
    public void whenGetSubsituteLotsOfParameters(){
        Map<String, ?> parameters = Map.of("ISBN", 12L
                , "CREATED_TEACHER_ID", 2
                , "AUTHOR_FIRST_NAME", "first"
                , "AUTHOR_LAST_NAME", "Last name"
                , "NUMBER_OF_POINTS", 12.0F
                , "READING_LEVEL", 8.0F
                , "TITLE", "Blahhhhh a title"
        );
        quarkusNamedJdbcTemplate.queryForObject("INSERT INTO ASSESSMENT(ISBN,AUTHOR_FIRST_NAME,AUTHOR_LAST_NAME,TITLE,NUMBER_OF_POINTS,READING_LEVEL,CREATED_TEACHER_ID) VALUES(:ISBN,:AUTHOR_FIRST_NAME,:AUTHOR_LAST_NAME,:TITLE,:NUMBER_OF_POINTS,:READING_LEVEL,:CREATED_TEACHER_ID)",
                parameters, (resultSet, rowNumber) -> null);
        verify(quarkusJdbcTemplate, times(1)).queryForObject(createdSqlCaptor.capture(), any(), anyBoolean());
        assertEquals("Created sql should have value replaced","INSERT INTO ASSESSMENT(ISBN,AUTHOR_FIRST_NAME,AUTHOR_LAST_NAME,TITLE,NUMBER_OF_POINTS,READING_LEVEL,CREATED_TEACHER_ID) VALUES(12,'first','Last name','Blahhhhh a title',12.0,8.0,2)", createdSqlCaptor.getValue());
    }

    @Test
    public void whenGetSubstituteOneParameterAtEnd(){
        Map<String, ?> parameters = Map.of("ISBN", 12L);
        quarkusNamedJdbcTemplate.queryForObject("SELECT ISBN,AUTHOR_FIRST_NAME,AUTHOR_LAST_NAME,TITLE,NUMBER_OF_POINTS,IS_VERIFIED,READING_LEVEL,CREATED_TEACHER_ID FROM ASSESSMENT WHERE ISBN=:ISBN",
                parameters, (resultSet, rowNumber) -> null);
        verify(quarkusJdbcTemplate, times(1)).queryForObject(createdSqlCaptor.capture(), any(), anyBoolean());
        assertEquals("Created sql should have value replaced","SELECT ISBN,AUTHOR_FIRST_NAME,AUTHOR_LAST_NAME,TITLE,NUMBER_OF_POINTS,IS_VERIFIED,READING_LEVEL,CREATED_TEACHER_ID FROM ASSESSMENT WHERE ISBN=12", createdSqlCaptor.getValue());
    }
}