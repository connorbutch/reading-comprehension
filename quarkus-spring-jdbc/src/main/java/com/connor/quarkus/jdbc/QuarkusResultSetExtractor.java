package com.connor.quarkus.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

@FunctionalInterface
public interface QuarkusResultSetExtractor<T> {
    T extractData(ResultSet rs) throws SQLException;
}
