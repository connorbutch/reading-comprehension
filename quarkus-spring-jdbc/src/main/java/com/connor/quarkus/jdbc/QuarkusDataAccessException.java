package com.connor.quarkus.jdbc;

public class QuarkusDataAccessException extends RuntimeException{
    public QuarkusDataAccessException(String message){
        super(message);
    }

    public QuarkusDataAccessException(String message, Throwable cause){
        super(message, cause);
    }
}
