package com.connor.quarkus.jdbc;

public class QuarkusKeyHolderLongImpl extends AbstractBaseQuarkusKeyHolder<Long> {
    public QuarkusKeyHolderLongImpl() {
        super(Long.class);
    }
    //intentionally blank -- all needed functionality is inherited from base
}
