package com.connor.quarkus.jdbc;

import java.util.*;

/**
 * This file contains a paired down version of spring named parameter jdbc template for use.
 *
 * This is more a proof of concept, and SHOULD NOT BE USED IN PRODUCTION (as it does not prepare statements to prevent sql injection
 */
public class QuarkusNamedJdbcTemplate {
    private static final int NOT_FOUND_INDEX = -1;
    private static final String VARIABLE_INDICATOR = ":";
    private static final Set<Character> END_VARIABLE_INDICATORS = Collections.unmodifiableSet(Set.of(' ', ',', ')'));
    private static final String STRING_PARAM_FORMAT_STR = "'%s'";

    private final QuarkusJdbcTemplate quarkusJdbcTemplate;

    public QuarkusNamedJdbcTemplate(QuarkusJdbcTemplate quarkusJdbcTemplate) {
        this.quarkusJdbcTemplate = quarkusJdbcTemplate;
    }

    public <T> List<T> query(String sql, Map<String, ?> parameters, QuarkusRowMapper<T> quarkusRowMapper) {
        validateQueryInput(sql, parameters, quarkusRowMapper);
        String sqlToExecute = buildSqlFromParameters(sql, parameters);
        System.out.println("Sql created from parameters " + sqlToExecute);
        return quarkusJdbcTemplate.query(sqlToExecute, quarkusRowMapper);
    }

    public int update(String sql, Map<String, ?> paramMap) {
        validateUpdateInput(sql, paramMap);
        String sqlToExecute = buildSqlFromParameters(sql, paramMap);
        System.out.println("Sql created from parameters " + sqlToExecute);
        return quarkusJdbcTemplate.update(sqlToExecute);
    }

    public <T> T queryForObject(String sql, Map<String, ?> paramMap, QuarkusRowMapper<T> rowMapper) {
        return queryForObject(sql, paramMap, rowMapper, false);
    }

    public <T> T queryForObject(String sql, Map<String, ?> paramMap, QuarkusRowMapper<T> rowMapper, boolean readUncommited) {
        validateQueryInput(sql, paramMap, rowMapper);
        String sqlToExecute = buildSqlFromParameters(sql, paramMap);
        return quarkusJdbcTemplate.queryForObject(sqlToExecute, rowMapper, readUncommited);
    }

    public <T> T query(String sql, Map<String, ?> paramMap, QuarkusResultSetExtractor<T> resultSetExtractor) {
        return query(sql, paramMap, resultSetExtractor, false);
    }

    public <T> T query(String sql, Map<String, ?> paramMap, QuarkusResultSetExtractor<T> resultSetExtractor, boolean readUncommited) {
        validateQueryInput(sql, paramMap, resultSetExtractor);
        String sqlToExecute = buildSqlFromParameters(sql, paramMap);
        return quarkusJdbcTemplate.query(sqlToExecute, resultSetExtractor, readUncommited);
    }

    public int update(String sql, Map<String, ?> paramMap, QuarkusKeyHolder<?> generatedKeyHolder){
        validateUpdateInput(sql, paramMap, generatedKeyHolder);
        String sqlToExecute = buildSqlFromParameters(sql, paramMap);
        return quarkusJdbcTemplate.update(sqlToExecute, generatedKeyHolder);
    }

    protected void validateQueryInput(String sql, Map<String, ?> parameters, QuarkusRowMapper<?> quarkusRowMapper) {
        //no validation being done for now, can change in future
    }

    protected void validateQueryInput(String sql, Map<String, ?> parameters, QuarkusResultSetExtractor<?> quarkusRowMapper) {
        //no validation being done for now, can change in future
    }

    protected void validateUpdateInput(String sql, Map<String, ?> paramMap, QuarkusKeyHolder<?> generatedKeyHolder) {
        //no validation being done for now, can change in future
    }

    protected void validateUpdateInput(String sql, Map<String, ?> paramMap) {
        //no validation being done for now
    }

    protected String buildSqlFromParameters(String sql, Map<String, ?> parameters) {
        int index = sql.indexOf(VARIABLE_INDICATOR);
        while (index != NOT_FOUND_INDEX) {
            System.out.println("Sql " + sql);
            int endIndex = findVariableStopIndex(sql, index);
            String variableName = sql.substring(index, endIndex);
            String replacementValue = getReplacementValueForKey(parameters, variableName.substring(1));
            sql = sql.replaceFirst(variableName, replacementValue);
            index = sql.indexOf(VARIABLE_INDICATOR, index + replacementValue.length());
        }
        return sql;
    }

    protected String getReplacementValueForKey(Map<String, ?> parameters, String key) {
        return getValueAsOptional(parameters, key)
                .map(this::convertToString)
                .orElseGet(() -> null);
    }

    protected Optional<?> getValueAsOptional(Map<String, ?> parameters, String key) {
        return Optional.ofNullable(parameters.get(key));
    }

    protected String convertToString(Object objectToConvert) {
        return objectToConvert.getClass() == String.class ? String.format(STRING_PARAM_FORMAT_STR, objectToConvert) :
                String.valueOf(objectToConvert);
    }

    protected int findVariableStopIndex(String sql, int startIndex) {
        return END_VARIABLE_INDICATORS.stream()
                .map(endCharacter -> sql.indexOf(endCharacter, startIndex))
                .filter(index -> index != -1)
                .min(Integer::compareTo)
                .orElseGet(sql::length);
    }
}
