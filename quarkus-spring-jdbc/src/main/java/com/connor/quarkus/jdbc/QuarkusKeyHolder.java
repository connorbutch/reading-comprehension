package com.connor.quarkus.jdbc;

/**
 * Use this to hold a generated key from the database.
 * @param <T>
 */
public interface QuarkusKeyHolder<T extends Number> {

    public void setKeyValue(T keyValue);

    public T getKeyValue();

    Class<T> getKeyType();
}
