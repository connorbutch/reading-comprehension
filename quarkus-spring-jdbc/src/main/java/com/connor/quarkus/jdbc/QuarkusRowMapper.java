package com.connor.quarkus.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface QuarkusRowMapper<T> {
    T mapRow(ResultSet rs, int rowNum) throws SQLException;
}