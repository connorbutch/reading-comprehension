package com.connor.quarkus.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

public class KeyHolderUtil {
    private static final int INDEX_OF_KEY_IN_RS = 1;

    public void setKeyValueFromResultSet(QuarkusKeyHolder<?> quarkusKeyHolder, ResultSet rs) {
        try {
            if (Integer.class.equals(quarkusKeyHolder.getKeyType())) {
                ((QuarkusKeyHolder<Integer>) quarkusKeyHolder).setKeyValue(rs.getInt(INDEX_OF_KEY_IN_RS));
            } else if (Long.class.equals(quarkusKeyHolder.getKeyType())) {
                ((QuarkusKeyHolder<Long>) quarkusKeyHolder).setKeyValue(rs.getLong(INDEX_OF_KEY_IN_RS));
            } else {
                throw new QuarkusDataAccessException("No generated key holder implementation found for type " + quarkusKeyHolder.getKeyType());
            }
        } catch (SQLException e) {
            throw new QuarkusDataAccessException("No generated key found in first index", e);
        }
    }
}
