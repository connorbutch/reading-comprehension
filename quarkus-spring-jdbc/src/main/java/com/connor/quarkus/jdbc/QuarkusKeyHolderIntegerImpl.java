package com.connor.quarkus.jdbc;

public class QuarkusKeyHolderIntegerImpl extends AbstractBaseQuarkusKeyHolder<Integer> {
       public QuarkusKeyHolderIntegerImpl(){
        super(Integer.class);
    }
    //intentionally blank -- all needed functionality is inherited from base
}
