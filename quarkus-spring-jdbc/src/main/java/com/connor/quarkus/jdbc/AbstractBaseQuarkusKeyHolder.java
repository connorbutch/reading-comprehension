package com.connor.quarkus.jdbc;

public abstract class AbstractBaseQuarkusKeyHolder<T extends Number> implements QuarkusKeyHolder<T>{

    private final Class<T> classType;

    private T generatedKey;

    protected AbstractBaseQuarkusKeyHolder(Class<T> classType){
        this.classType = classType;
    }

    @Override
    public void setKeyValue(T keyValue) {
        this.generatedKey = keyValue;
    }

    @Override
    public T getKeyValue() {
        return this.generatedKey;
    }

    @Override
    public Class<T> getKeyType() {
        return this.classType;
    }
}
