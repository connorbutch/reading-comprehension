package com.connor.quarkus.jdbc;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class QuarkusJdbcTemplate {
    //first column in result set is 1 (it is 1 based, not 0 based)
    private static final int INDEX_OF_FIRST_COLUMN_IN_RS = 1;

    private static final boolean DEFAULT_DO_READ_UNCOMMITTED = false;
    private static final boolean DEFAULT_DO_AUTO_COMMIT = true;

    private final DataSource dataSource;
    private final KeyHolderUtil keyHolderUtil;

    public QuarkusJdbcTemplate(DataSource dataSource, KeyHolderUtil keyHolderUtil){
        this.dataSource = dataSource;
        this.keyHolderUtil = keyHolderUtil;
    }

    public QuarkusJdbcTemplate(DataSource dataSource) {
        this(dataSource, new KeyHolderUtil());
    }

    public <T> List<T> query(String sql, QuarkusRowMapper<T> quarkusRowMapper) {
        return query(sql, quarkusRowMapper, false);
    }

    public <T> List<T> query(String sql, QuarkusRowMapper<T> quarkusRowMapper, boolean readUncommitted) {
        validateQueryInput(sql, quarkusRowMapper);
        List<T> queryResult = new ArrayList<>();
        try (Connection connection = getConnection(readUncommitted);
             Statement statement = createStatement(connection);
             ResultSet resultSet = getResultSet(statement, sql)) {
            int rowNum = 0;
            while (resultSet.next()) {
                queryResult.add(quarkusRowMapper.mapRow(resultSet, rowNum));
                ++rowNum;
            }
        } catch (SQLException e) {
            //TODO could translate here based on e.getErrorCode()
            throw new QuarkusDataAccessException("An error occurred", e);
        }
        return queryResult;
    }

    public int queryForInt(String sql) {
        try (Connection connection = getConnection();
             Statement statement = createStatement(connection);
             ResultSet resultSet = getResultSet(statement, sql)) {
            resultSet.next();
            return resultSet.getInt(INDEX_OF_FIRST_COLUMN_IN_RS);
        } catch (SQLException e) {
            //TODO could translate here based on e.getErrorCode()
            throw new QuarkusDataAccessException("An error occurred when querying for int", e);
        }
    }

    public int update(String sql) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new QuarkusDataAccessException("There was an error when executing sql and finding how many rows are updated", e);
        }
    }

    public <T> T queryForObject(String sql, QuarkusRowMapper<T> rowMapper) {
        return queryForObject(sql, rowMapper, false);
    }

    public <T> T queryForObject(String sql, QuarkusRowMapper<T> rowMapper, boolean readUncommitted ) {
        T valueToReturn = null;
        try(Connection connection = getConnection(readUncommitted);
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery()){
            if(resultSet.next()){
                valueToReturn = rowMapper.mapRow(resultSet, 0);
            }
        }catch(SQLException e){
            throw new QuarkusDataAccessException("Error when querying for object", e);
        }
        return valueToReturn;
    }

    public <T> T query(String sql, QuarkusResultSetExtractor<T> resultSetExtractor){
      return query(sql, resultSetExtractor, false);
    }

    public <T> T query(String sql, QuarkusResultSetExtractor<T> resultSetExtractor, boolean readUncommitted){
        T valueToReturn = null;
        try(Connection connection = getConnection(readUncommitted);
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery()){
            valueToReturn = resultSetExtractor.extractData(resultSet);
        }catch(SQLException e){
            throw new QuarkusDataAccessException("Error when querying for object", e);
        }
        return valueToReturn;
    }

    public int update(String sql, QuarkusKeyHolder<?> keyHolder){
        int numberOfRowsUpdated = -1;
        try(Connection connection = getConnection();
        Statement statement = connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
                java.sql.ResultSet.CONCUR_UPDATABLE);) {
            numberOfRowsUpdated = statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            try(ResultSet rs = statement.getGeneratedKeys()){
                rs.next();
                keyHolderUtil.setKeyValueFromResultSet(keyHolder, rs);
            }
        } catch (SQLException e) {
           throw new QuarkusDataAccessException("There was an error when executing sql " + sql, e);
        }
        return numberOfRowsUpdated;
    }

    protected void validateQueryInput(String sql, QuarkusRowMapper<?> quarkusRowMapper) {
        Objects.requireNonNull(quarkusRowMapper, "Rowmapper cannot be null");
        if (sql == null) {
            throw new RuntimeException(); //TODO
        } else if (sql.isEmpty()) {
            throw new RuntimeException(); //TODO
        } else if (sql.isBlank()) {
            throw new RuntimeException(); //TODO
        }
    }

    protected Connection getConnection() throws SQLException {
        return getConnection(DEFAULT_DO_READ_UNCOMMITTED);
    }

    protected Connection getConnection(boolean doReadUncommited) throws SQLException {
        return getConnection(doReadUncommited, DEFAULT_DO_AUTO_COMMIT);
    }

    protected Connection getConnection(boolean doReadUncommitted, boolean doAutoCommit) throws SQLException {
        System.out.println("Attempting to get connection");
        Connection connection = dataSource.getConnection();
        //no need to use transaction isolation level here now, but passed as arg in case we need it in the future
        System.out.println("Successfully got connection");
        return connection;
    }

    protected Statement createStatement(Connection connection) throws SQLException {
        System.out.println("Attempting to create statement");
        Statement statement = connection.createStatement();
        System.out.println("Successfully created statement");
        return statement;
    }

    protected ResultSet getResultSet(Statement statement, String sql) throws SQLException {
        System.out.println("Attempting to execute query");
        ResultSet resultSet = statement.executeQuery(sql);
        System.out.println("Successfully executed query");
        return resultSet;
    }
}
