# Overview
(Jdbc) database interactions are an essential part of most applications.  One of the most used database libraries in the java ecoystem
is spring jdbc, mostly known for its JdbcTemplate and NamedJdbcTemplate classes.  However, these do not support ahead-of-time compilation, 
and hence are incompatible with quarkus.  This module offers a quarkus-compatible, paired down alternative with the same functionality as
the beloved spring-jdbc library.


## Important Note
**In an enterprise-world, this would be in its own project, but since I wanted users to be able to run this locally 
with no external dependencies, I placed it in the same project as a submodule.**