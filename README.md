# reading-comprehension
## Motivation
Hi, welcome to gitlab.  I'm glad you found this project.  This is a personal project
I have been working on (and will be working on) which I hope will help demonstrate
the capabilities of modern technology.  This will (likely) be accompanied by a youtube
tutorial series.

## Functionality
This project offers functionality for teachers/students to create, update, and retrieve
assessments for books to gauge students' understanding.

## Technologies Involved
This project uses various technologies, some of which include:
- java (11)
- gradle
- open api specification
- code generation
- cucumber (integration tests)
- quarkus
- graal vm
- jax rs (specification)
- cdi (jsr 330)
- Docker
- Kubernetes
- Docker-compose (for integration tests only)
- Knative
- Gitlab
- Junit
- Mockito
- MySQL
- Flyaway

## Processes Used
When developing this project, I have also attempted to use modern processes.  I've listed a few of these below:
- (Acceptance) Test Driven Development (TDD)
- Continuous Integration Continuous Delivery (CI/CD)
- (Unit) Test Driven Development (TDD)
- Behavior Driven Development (BDD)
- Automated Testing

## Module Overview
This project is broken down into various submodules.  A brief overview of each is given here, 
and if you would like more information, please do visit the README.md inside each of the respective directories.
Additionally, feel free to visit the git commit history to see how this project has been, and is, being developed.


## Running the application
As this is a tutorial, I've made the program easily runnable by executing a single script.  From this directory (the root of the project),
simply execute *./run-it.sh* on any linux based machine (with docker installed), and the program should run for you.

To run this within a CI/CD server (we use gitlab ci, but this would also work within Jenkins, Bamboo, Travis, etc.), start the run script
in another window, then call *./wait-for-it.sh localhost:8080/health/readiness --timeout=180* (please note, application starts in under two one hundredths of a second
the time is for the build box server to run) before running *./gradlew acceptanceTest*

Please note that this does the entire build process (including the ahead of time compilation, which saves a ton of time on startup, but can take time)
, if you would just like to run the application, you can run it with *docker run -i --rm -p 8080:8080 connorbutch/reading-comprehension-server-quarkus-impl*

One way to verify the application works (after you have built the docker image), is to run: 
*docker run -i --rm -p 8080:8080 connorbutch/reading-comprehension-server-quarkus-impl & ./wait-for-it.sh localhost:8080 --timeout=10 --strict -- ./gradlew acceptanceTest*
## General Notes
As mentioned above, this project contains various readme for more information.  Also, if you see any opportunities for this to be improved,
please do reach out.  This is also a chance for me to experiment with new technologies, so I appreciate any feedback you may have.
Thanks for reading, and happy coding.
