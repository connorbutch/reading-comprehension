# Postman Setup
This folder contains postman related items that can easily be used to visualize the api requests 
and responses.  

## Setup
1. Download postman for free (https://www.postman.com/downloads/)
2. Import the collection(s) present in this folder to your postman application (click the big import button, or go file -> import)
3. Set the base url you wish to use (if running locally, use localhost:8080)
4. Click send on the requests, and watch the results come back.