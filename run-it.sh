#!/bin/bash
echo "Have you made code changes since the last time you built the docker image (running this script creates a docker image)?"
echo "1 - Yes, I have made changes (this will require a build of the image, will take a long time)"
echo "2 - No, I have NOT made changes since the last time running this script (this will start in hundreths of a second)"
read -p "Enter your choice (1 or 2): "  choice

if [ "$choice" == "1" ]; then
    echo "You have chosen a full rebuilt.  Please be patient, as compiling the native image performs ahead of time compilation, which takes a minute or two."

if ./gradlew clean quarkusBuild; then
  echo gradle quarkus build succeeded
else
  echo gradle quarkus build failed
  exit 1
fi

if ./gradlew clean build -Dquarkus.package.type=native -Dquarkus.native.container-build=true; then
  echo gradle graalvm compile succeeded
else
  echo gradle graalvm compile failed
  exit 2
fi

cd reading-comprehension-server-quarkus-impl || exit
if docker build -f infrastructure/Dockerfile -t registry.gitlab.com/connorbutch/reading-comprehension .;  then
  echo docker build succeeded
else
echo docker build failed
cd ..
exit 3
fi

cd ..
else
    echo "You have chosen to skip rebuilding the image"
fi

echo attempting to run docker-compose specification \(database and application\)
cd reading-comprehension-server-quarkus-impl/infrastructure || exit
echo setting environment variables
export SUBNET_STATIC="172.18.0.0/24"
export DATABASE_STATIC_IP="172.18.0.8"
export CONTAINER_STATIC_IP="172.18.0.16"
export TEACHER_STATIC_IP="172.18.0.18"
export IMAGE_TAG="latest"
echo shutting down docker compose from last build
docker-compose down -v
echo killing all other docker containers that may interfere with this one running
docker container kill $(docker ps -q)
echo removing cached image of mock teacher service to ensure we get the latest version
docker rmi docker.io/connorbutch/teacher-service:mock
echo The docker-compose yaml is being printed below for debugging
docker-compose config
if  docker-compose up; then
    echo docker-compose succeeded
else
  echo running docker-compose failed
  exit 4
fi