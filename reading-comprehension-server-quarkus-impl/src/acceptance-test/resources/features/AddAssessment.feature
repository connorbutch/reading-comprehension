# --------------------------------------------------------------------------
# User story
# As a teacher
# I want to be able to create a new assessment
# so that I can determine if my students are understanding a book
# --------------------------------------------------------------------------
#NOTE: this test will run fifth out of all tests
Feature: Adding an assessment

  @AddAssessment
    @Insert
    @NegativeTest
  Scenario Outline: Attempting to add an assessment with invalid values should return an error response with an appropriate message
    Given the assessment to be added has an isbn of "<isbn>"
    And the assessment to be added has a title of "<title>"
    And the assessment to be added has an author first name of "<authorFirstName>"
    And the assessment to be added has an author last name of "<authorLastName>"
    And the assessment to be added has a reading level of "<readingLevel>"
    And the assessment to be added has a number of points "<numberOfPoints>"
    And the assessment to be added has a created teacher id of "<createdTeacherId>"
    When a request is made to the add assessment endpoint
    Then the response status code should be 400
    And the response body should be an error response
    And the response body error should an error reason with errorId "<errorId>"
    And the error reason in the response body with the given error reason should have a message containing the string "<errorMessage>"
    Examples:
      | isbn | title                                                                                                                                                                          | authorFirstName                               | authorLastName                                | readingLevel | createdTeacherId | numberOfPoints | errorId | errorMessage                                                                      |
      | null | a                                                                                                                                                                              | a                                             | a                                             | 8.0          | 1                | 11.5           | 4       | Isbn must be a not null value                                                     |
      | 0    | a                                                                                                                                                                              | a                                             | a                                             | 8.0          | 1                | 11.5           | 5       | Isbn must be greater than or equal to 1                                           |
      | 1    | a                                                                                                                                                                              | null                                          | a                                             | 8.0          | 1                | 11.5           | 7       | Author first name must be a not null value                                        |
      | 1    | a                                                                                                                                                                              |                                               | a                                             | 8.0          | 1                | 11.5           | 8       | Author first name must be a non-blank string                                      |
      | 1    | a                                                                                                                                                                              | waytooooooooooooooooooooooooolongyeahtooolong | a                                             | 8.0          | 1                | 11.5           | 9       | Author first name must have a length less than 32                                 |
      | 1    | a                                                                                                                                                                              | first                                         | null                                          | 8.0          | 1                | 11.5           | 10      | Author last name must be a not null value                                         |
      | 1    | a                                                                                                                                                                              | first                                         |                                               | 8.0          | 1                | 11.5           | 11      | Author last must be a non-blank string                                            |
      | 1    | a                                                                                                                                                                              | first                                         | waytooooooooooooooooooooooooolongyeahtooolong | 8.0          | 1                | 11.5           | 12      | Author last name must have a length less than 32                                  |
      | 1    | null                                                                                                                                                                           | first                                         | last                                          | 8.0          | 1                | 11.5           | 13      | Title must be a not null value                                                    |
      | 1    |                                                                                                                                                                                | first                                         | last                                          | 8.0          | 1                | 11.5           | 14      | Title must be a non-blank string                                                  |
      | 1    | tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt | first                                         | last                                          | 8.0          | 1                | 11.5           | 15      | Title must have a length less than 128                                            |
      | 1    | a                                                                                                                                                                              | a                                             | a                                             | 8.0          | null             | 11.5           | 16      | Created teacher id must be a non-null value                                       |
      | 1    | a                                                                                                                                                                              | a                                             | a                                             | 8.0          | 4                | 11.5           | 17      | Created teacher id is not valid.  Please check with owners of the teacher service |
      | 1    | a                                                                                                                                                                              | a                                             | a                                             | 8.0          | 2                | 11.5           | 18      | Teacher with id does not exist.  Please check with owners of the teacher service  |
      | 1    | a                                                                                                                                                                              | a                                             | a                                             | 8.0          | 1                | null           | 19      | Number of points must be a non-null value                                         |
      | 1    | a                                                                                                                                                                              | a                                             | a                                             | 8.0          | 1                | .4             | 20      | Number of points must be greater than 0.50 and less than 125.00                   |
      | 1    | a                                                                                                                                                                              | a                                             | a                                             | 8.0          | 1                | 126            | 20      | Number of points must be greater than 0.50 and less than 125.00                   |
      | 1    | a                                                                                                                                                                              | a                                             | a                                             | null         | 1                | 11.5           | 21      | Reading level must be a not null value                                            |
      | 1    | a                                                                                                                                                                              | a                                             | a                                             | .3           | 1                | 11.5           | 22      | Reading level must be greater than 0.50 and less than 12.00                       |
      | 1    | a                                                                                                                                                                              | a                                             | a                                             | 12.01        | 1                | 11.5           | 22      | Reading level must be greater than 0.50 and less than 12.00                       |

  @AddAssessment
    @Insert
    @NegativeTest
  Scenario Outline: Attempting to add an assessment for an isbn that already exists should return an error with an appropriate message
    Given the assessment to be added has an isbn of "<isbn>"
    And the other fields in the request are valid
    When a request is made to the add assessment endpoint
    Then the response status code should be 409
    And the response body should be an error response
    And the response body error should an error reason with errorId "<errorId>"
    And the error reason in the response body with the given error reason should have a message containing the string "<errorMessage>"
    Examples:
      | isbn          | errorId | errorMessage                            |
      | 9781976530739 | 25      | Assessment already exists for that isbn |

  @AddAssessment
    @Insert
    @PositiveTest
  Scenario Outline: Adding a valid assessment should add the assessment and return a valid response
    Given the assessment to be added has an isbn of "<isbn>"
    And the assessment to be added has a title of "<title>"
    And the assessment to be added has an author first name of "<authorFirstName>"
    And the assessment to be added has an author last name of "<authorLastName>"
    And the assessment to be added has a reading level of "<readingLevel>"
    And the assessment to be added has a number of points "<numberOfPoints>"
    And the assessment to be added has a created teacher id of "<createdTeacherId>"
    When a request is made to the add assessment endpoint
    Then the response status code should be 201
    And the response body should be an assessment
    And the response body should have the isbn specified above
    And the response body should have the title specified above
    And the response body should have the first name specified above
    And the response body should have the last name specified above
    And the response body should have the reading level specified above
    And the response body should have the number of points specified above
    And the response body should have the created teacher id specified above
    Examples:
      | isbn          | title                | authorFirstName | authorLastName | readingLevel | numberOfPoints | createdTeacherId |
      | 9780805385656 | The science of sound | Moore           | Rossing        | 7.5          | 7.0            | 1                |


  @AddAssessment
  @ConnectivityCheck
    Scenario: When a connectivity check is received from a service that depends on ours (as part of their health check), the request receives a mock response.
    Given the connectivity request is valid
    When a request is made to the add assessment endpoint
    Then the request should not be processed (and instead receive a mock response)
    Then the response status code should be 201
    And the response body should be an assessment

  #This exists to show we have full control of our (acceptance test) environment.  Here we mock a 500 from the teacher service, which causes us to return a 500 as well.
  @AddAssessment
  @Insert
  @NegativeTest
  @OtherServicesAreDown
  Scenario: When the teacher service is down, the client should get an error message
    Given the assessment to be added has an isbn of 1234567890
    And the teacher service is temporarily down
    And the other fields in the request are valid
    When a request is made to the add assessment endpoint
    Then the response status code should be 500
    And the response body should be an error response
    And the response body error should an error reason with errorId 999999999
    And the error reason in the response body with the given error reason should have a message containing the string Unexpected error occurred



