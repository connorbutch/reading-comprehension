# --------------------------------------------------------------------------
# User story
# As a student
# I want to be able to list all assessments
# so that I can decide which book(s) to checkout from the library
# --------------------------------------------------------------------------
#NOTE: this test will run third out of all tests
Feature: Getting multiple assessments based on values passed in.

  @GetAllAssessments
    @Query
    @NegativeTest
  Scenario Outline: Get all assessments should return an error when invalid parameter values are passed.
    Given a limit value of <limit> is passed in the request
    And an offset value of <offset> is passed in the request
    When a request is made to the get all assessments endpoint
    Then the response status code should be 400
    And the response body should be an error response
    And the response body error should an error reason with errorId "<errorId>"
    And the error reason in the response body with the given error reason should have a message containing the string "<errorMessage>"
    Examples:
      | limit | offset | errorMessage                                                          |
      | -1    | 2      | Limit must be greater than 0.  The value passed was -1                |
      | 0     | 3      | Limit must be greater than 0.  The value passed was 0                 |
      | 2     | -422   | Offset must be greater than or equal to 0.  The value passed was -422 |


  @GetAllAssessments
    @Query
    @PositiveTest
  Scenario Outline: Get all assessments should return an empty list when offset is greater than the number of assessments
    Given a limit value of <limit> is passed in the request
    And an offset value of <offset> is passed in the request
    When a request is made to the get all assessments endpoint
    Then the response status code should be 200
    And the response body should be an empty list
    And the x-total-assessments header value should be returned as <totalAssessments>
    Examples:
      | limit | offset | totalAssessments |
      | 2     | 4      | 4                |
      | 2     | 100    | 4                |

  @GetAllAssessments
    @Query
    @PositiveTest
  Scenario Outline: Get all assessments should return a populated list when valid search parameters are used
    Given a limit value of <limit> is passed in the request
    And an offset value of <offset> is passed in the request
    When a request is made to the get all assessments endpoint
    Then the response status code should be 200
    And the response body should be a populated list
    And the list should have <numEntries> assessments in it
    And each entry in the list should have a non null isbn
    And each entry in the list should have a non null title
    And each entry in the list should have a non null created teacher id
    And each entry in the list should have a non null reading level
    And each entry in the list should have a non null number of points assigned
    And each entry in the list should have a non null author
    And one of the assessments should have isbn of <oneIsbnInList>
    And the x-total-assessments header value should be returned as <totalAssessments>
    Examples:
      | limit | offset | numEntries | oneIsbnInList | totalAssessments |
      | 1     | 0      | 1          | 486282112     | 4                |
      | 6     | 0      | 4          | 1503275922    | 4                |

  @GetAllAssessments
  @ConnectivityCheck
  Scenario: When a connectivity check is received from a service that depends on ours (as part of their health check), the request receives a mock response.
    Given the connectivity request is valid
    When a request is made to the get all assessments endpoint
    Then the request should not be processed (and instead receive a mock response)
    Then the response status code should be 200
    And the response body should be a list of assessments
    And the list should have one assessment in it
