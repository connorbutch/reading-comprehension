# --------------------------------------------------------------------------
# User story
# As a developer
# I want my application to have a readiness health check
# so that it can be monitored and so that kubernetes does not route traffic to a pod until it is ready
# --------------------------------------------------------------------------
#NOTE: this test will run second out of all tests
Feature: Readiness health check

@Health
@Readiness
@PositiveTest
  Scenario: Readiness health check should return up when application is running.
    Given The application is deployed and running
    When a request is made to the readiness endpoint
    Then the response status code should be 200
    And the response body have a status of is available
    And the response body should not have any dependencies unavailable
    And the response body should have eight available dependencies
    And the available dependencies should be in alphabetical order based on name
    And one of the available dependencies should have a name of 'GetTeacherByIdChecker'
    And another available dependency should have a name of 'InsertAndReturnAssessmentChecker'
    And another available dependency should have a name of 'InsertAndReturnQuestionChecker'
  And another available dependency should have a name of 'MemoryChecker'
  And another available dependency should have a name of 'QueryForAssessmentsChecker'
  And another available dependency should have a name of 'QueryForErrorByIdChecker'
  And another available dependency should have a name of 'QueryQuestionsChecker'
  And another available dependency should have a name of 'SystemPropertyChecker'