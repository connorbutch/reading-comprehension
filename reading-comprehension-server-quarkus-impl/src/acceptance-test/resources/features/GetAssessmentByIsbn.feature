# --------------------------------------------------------------------------
# User story
# As a student
# I want to be able to get an assessment for a certain book
# so that I can show my teacher I read (and understood) the book
# --------------------------------------------------------------------------
#NOTE: this test will run fourth out of all tests
Feature: Retrieving an assessment by isbn.

  Scenario Outline: Attempting to retrieve an assessment and passing in invalid isbn should return an error response with an appropriate message
    Given the isbn used as a path parameter is "<isbn>"
    When a request is made to the get assessment by isbn endpoint
    Then the response status code should be 400
    And the response body error message should contain "<errorMessageSubstring>"
    Examples:
      | isbn | errorMessageSubstring                   |
      | 0    | Isbn must be greater than or equal to 1 |
      | -23  | Isbn must be greater than or equal to 1 |

  Scenario Outline: Attempting to retrieve an assessment for an isbn that doesn't yet have one should return an error response with an appropriate message
    Given the isbn used as a path parameter is "<isbn>"
    When a request is made to the get assessment by isbn endpoint
    Then the response status code should be 404
    And the response body error message should contain "<errorMessageSubstring>"
    Examples:
      | isbn      | errorMessageSubstring               |
      | 23        | Assessment not found for given isbn |
      | 111111111 | Assessment not found for given isbn |

  Scenario Outline: A valid request to get an existing isbn should return the assessment details
    Given the isbn used as a path parameter is "<isbn>"
    When a request is made to the get assessment by isbn endpoint
    Then the response status code should be 200
    And the response body should be an assessment
    And the response body should have isbn specified above
    And the response body should have reading level "<readingLevel>"
    And the response body should have number of points "<numberOfPoints>"
    And the response body should have created teacher id "<createdTeacherId>"
    And the response body should have title "<title>"
    And the response body should have author first name "<authorFirstName>"
    And the response body should have author last name "<authorLastName>"
    Examples:
      | isbn          | readingLevel | numberOfPoints | createdTeacherId | title             | authorFirstName | authorLastName |
      | 9781976530739 | 10.8         | 65             | 1                | Moby Dick         | Herman          | Melleville     |
      | 1503275922    | 12.5         | 36             | 328              | Heart of Darkness | Joseph          | Conrad         |


  @ConnectivityCheck
  Scenario: When a connectivity check is received from a service that depends on ours (as part of their health check), the request receives a mock response.
    Given the connectivity request is valid
    When a request is made to the get assessment by isbn endpoint
    Then the request should not be processed (and instead receive a mock response)
    Then the response status code should be 200
    And the response body should be a (not null) assessment
