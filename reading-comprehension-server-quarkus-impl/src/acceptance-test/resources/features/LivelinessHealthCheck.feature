# --------------------------------------------------------------------------
# User story
# As a developer
# I want my application to have a liveliness health check
# so that it can be monitored and automatically restarted by kubernetes.
# --------------------------------------------------------------------------
#NOTE: this test will run first out of all tests
Feature: Liveliness health check

  @Health
  @Liveliness
  @PositiveTest
  Scenario: Liveliness health check should return up when application is running.
    Given The application is deployed and running
    When a request is made to the liveliness endpoint
    Then the response status code should be 200
    And the response body should indicate the application is healthy