# --------------------------------------------------------------------------
# User story
# As a student
# I want to be able to retrieve questions for an assessment
# so that I can take the test to show my teacher I read (and understood) the book
# --------------------------------------------------------------------------
#NOTE: this test will run sixth out of all tests
Feature: Retrieving questions for a given assessment.

  @NegativeTest
  @Query
  @GetQuestions
  Scenario: Attempting to retrieve questions for an invalid isbn will return an error response with an appropriate error message
    Given the isbn used as a path parameter is -2
    When a request is made to retrieve questions for this isbn
    Then the response status code should be 400
    And the response body error message should contain "Isbn must be greater than or equal to 1"

  @NegativeTest
  @Query
  @GetQuestions
  Scenario: Attempting to retrieve questions for a non-existing assessment will return an error response with an appropriate error message
    Given the isbn used as a path parameter is 9876543210
    When a request is made to retrieve questions for this isbn
    Then the response status code should be 404
    And the response body error message should contain "Assessment not found for given isbn"

  @PositiveTest
  @Query
  @GetQuestions
  Scenario: Retrieving questions for an assessment that exists, but does not yet have questions will return a success response with an empty list
    Given the isbn used as a path parameter is 9781976530739
    When a request is made to retrieve questions for this isbn
    Then the response status code should be 200
    And the response body should be a list of questions
    And the list of questions should be empty

  @PositiveTest
    @Query
    @GetQuestions
  Scenario Outline: Retrieving questions for an assessment that exists should return questions associated with the assessment
    Given the isbn used as a path parameter is "<isbn>"
    When a request is made to retrieve questions for this isbn
    Then the response status code should be 200
    And the response body should be a list of questions
    And there should be "<numberOfQuestions>" questions in the returned list
    And one of the questions in the list should have question text "<questionTextOfOneEntryInList>"
    And the correct answer for that question should be "<correctAnswerForThatQuestion>"
    And there should be "<numberOfIncorrectAnswersForThatQuestion>" incorrect answers for that question
    And none of the incorrect answers should be blank
    Examples:
      | isbn       | numberOfQuestions | questionTextOfOneEntryInList | correctAnswerForThatQuestion          | numberOfIncorrectAnswersForThatQuestion |
      | 2679732764 | 2                 | Who is Mr. Norton?           | A rich, white trustee for the college |  3                                      |

  @ConnectivityCheck
    @Query
    @GetQuestions
  Scenario: When a connectivity check is received from a service that depends on ours (as part of their health check), the request receives a mock response.
    Given the connectivity request is valid
    When a request is made to retrieve questions for this isbn
    Then the request should not be processed (and instead receive a mock response)
    Then the response status code should be 200
    And the response body should be a list of questions
    And there should be one question in the list
    And that question should not be null