package com.connor.reading.health.liveliness;//package com.connor.health;

import com.connor.reading.ApiException;
import com.connor.reading.client.HealthApi;
import com.connor.reading.client.dto.HealthResponse;
import com.connor.reading.util.ClientBuilder;
import io.cucumber.java8.En;
import io.cucumber.java8.Scenario;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"}, features = {"src/acceptance-test/resources/features/LivelinessHealthCheck.feature"})
public class RunLivelinessIT implements En {
    private final HealthApi healthApi = ClientBuilder.getApiForTests();
    private Scenario scenario;
    private HealthResponse responseFromMakingLivelinessRequest;

    public RunLivelinessIT(){
        Before((Scenario scenario) -> this.scenario = scenario);
        Given("The application is deployed and running", () -> {//intentionally blank
        });
        When("a request is made to the liveliness endpoint", () -> {
            try{
                responseFromMakingLivelinessRequest = healthApi.livelinessCheck();
            }catch (ApiException e){
                String errorMessage = new StringBuilder("Error making call to liveliness endpoint.")
                        .append("\ncode:")
                        .append(e.getCode())
                        .append("\nresponse body")
                        .append(e.getResponseBody())
                        .append("\nresponse headers")
                        .append(e.getResponseHeaders())
                        .toString();
                throw new RuntimeException(errorMessage, e);
            }
        });
        Then("the response status code should be {int}", (Integer expectedResponseStatusCode) -> {
            //intentionally blank -- no assertions need to be made here, if we got here, we got a 200
        });
        Then("the response body should indicate the application is healthy", () -> {
            assertNotNull("Response should not be null", responseFromMakingLivelinessRequest);
            assertEquals("Health status should be the word ok", "ok", responseFromMakingLivelinessRequest.getHealthStatus());
        });
    }
}