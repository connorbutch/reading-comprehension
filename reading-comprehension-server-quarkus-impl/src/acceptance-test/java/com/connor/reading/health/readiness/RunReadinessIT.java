package com.connor.reading.health.readiness;

import com.connor.reading.client.HealthApi;
import com.connor.reading.client.dto.DownstreamDependency;
import com.connor.reading.client.dto.ReadinessHealthResponse;
import com.connor.reading.util.ClientBuilder;
import io.cucumber.java8.En;
import io.cucumber.java8.Scenario;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"}, features = {"src/acceptance-test/resources/features/ReadinessHealthCheck.feature"})
public class RunReadinessIT implements En {
    private final HealthApi healthApi = ClientBuilder.getApiForTests();
    private Scenario scenario;
    private ReadinessHealthResponse readinessHealthResponse;

    public RunReadinessIT(){
        Before((Scenario scenario) -> this.scenario = scenario);
        Given("The application is deployed and running", () -> {

        });
        When("a request is made to the readiness endpoint", () -> readinessHealthResponse = healthApi.readinessCheck(null));
        Then("the response status code should be {int}", (Integer expectedResponseStatusCode) -> {
            //don't need to do anything here, if no exception, then response was 200
        });
        Then("the response body have a status of is available", () -> assertEquals("Response should indicate it is up", Boolean.TRUE, readinessHealthResponse.getIsAvailable()));
        Then("the response body should not have any dependencies unavailable", () -> assertTrue("The list of unavailable dependencies should be an empty list", readinessHealthResponse.getUnavailableDependencies().isEmpty()));
        Then("the response body should have eight available dependencies", () -> assertEquals("The number of available dependencies should be four", 8, readinessHealthResponse.getAvailableDependencies().size()));
        Then("one of the available dependencies should have a name of {string}", (String nameOfOneDependency) -> assertTrue("There should be an available dependency with name " + nameOfOneDependency, readinessHealthResponse.getAvailableDependencies().stream().anyMatch(downstreamDependency -> nameOfOneDependency.trim().equals(downstreamDependency.getName()))));
        Then("the available dependencies should be in alphabetical order based on name", () -> {
            List<DownstreamDependency> fromResponse = readinessHealthResponse.getAvailableDependencies();
            List<DownstreamDependency> expected = readinessHealthResponse.getAvailableDependencies();
            expected.sort(Comparator.comparing(DownstreamDependency::getName));
            assertEquals("Sorted list should equal the list gotten in the response", expected, fromResponse);
        });
        Then("another available dependency should have a name of {string}", (String nameOfOtherDependency) -> assertTrue("There should be an available dependency with name " + nameOfOtherDependency, readinessHealthResponse.getAvailableDependencies().stream().anyMatch(downstreamDependency -> nameOfOtherDependency.trim().equals(downstreamDependency.getName()))));
        Then("the last available dependency should have a name of {string}", (String nameOfOtherDependency) -> assertTrue("There should be an available dependency with name " + nameOfOtherDependency, readinessHealthResponse.getAvailableDependencies().stream().anyMatch(downstreamDependency -> nameOfOtherDependency.trim().equals(downstreamDependency.getName()))));
    }
}
