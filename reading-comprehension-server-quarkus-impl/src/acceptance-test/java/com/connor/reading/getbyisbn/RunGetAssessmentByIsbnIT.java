package com.connor.reading.getbyisbn;

import com.connor.reading.ApiException;
import com.connor.reading.client.AssessmentsApi;
import com.connor.reading.client.dto.Assessment;
import com.connor.reading.client.dto.ErrorReason;
import com.connor.reading.client.dto.ErrorResponse;
import com.connor.reading.util.ClientBuilder;
import com.connor.reading.util.ErrorResponseUtil;
import io.cucumber.java8.En;
import io.cucumber.java8.Scenario;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"}, features = {"src/acceptance-test/resources/features/GetAssessmentByIsbn.feature"})
public class RunGetAssessmentByIsbnIT implements En {
    private static final int OK_STATUS_CODE = 200;

    private AssessmentsApi assessmentsApi;
    private Scenario scenario;
    private Long isbnUsedInRequest;
    private Assessment responseBody;
    private ApiException exceptionThrownOnNonSuccess;

    public RunGetAssessmentByIsbnIT(){
        Before((Scenario scenario) ->{
            this.scenario = scenario;
            this.scenario.log("Getting new generated client instance that is not used for health checks");
            assessmentsApi = ClientBuilder.getAssessmentApiForTests();
            isbnUsedInRequest = null;
            responseBody = null;
            exceptionThrownOnNonSuccess = null;
        });
        Given("the isbn used as a path parameter is {string}", (String isbnString) -> this.isbnUsedInRequest = isbnString == null || isbnString.isBlank()? null: Long.parseLong(isbnString));
        Given("the connectivity request is valid", () -> {
            scenario.log("Getting assessment api that is used for health check");
            assessmentsApi = ClientBuilder.getAssessmentApiForTests(true);
            scenario.log("Setting isbn path parameter to arbitrary number, so that it routes to the proper jaxrs endpoint");
            isbnUsedInRequest = 123L;
        });
        When("a request is made to the get assessment by isbn endpoint", () -> {
            try{
                responseBody = assessmentsApi.getAssessmentByIsbn(isbnUsedInRequest);
                scenario.log("Successfully made request (and got response with 2xx status code)");
            }catch (ApiException e){
                exceptionThrownOnNonSuccess = e;
                scenario.log("Exception thrown when making request (NOTE: doesn't mean bad, if running negative tests this is actually expected behavior).  The response body was " + e.getResponseBody());
            }
        });
        Then("the response status code should be {int}", (Integer expectedStatusCode) -> {
            Integer actualStatusCode = exceptionThrownOnNonSuccess !=  null? exceptionThrownOnNonSuccess.getCode(): OK_STATUS_CODE;
            scenario.log("Expected status code " + expectedStatusCode);
            scenario.log("Actual status code " + actualStatusCode);
            assertEquals("Status code in response should match", expectedStatusCode, actualStatusCode);
        });
        Then("the response body error message should contain {string}", (String expectedSubstringInErrorBody) -> {
            if(expectedSubstringInErrorBody != null){
                expectedSubstringInErrorBody = expectedSubstringInErrorBody.trim();
            }
            ErrorResponse errorResponse = ErrorResponseUtil.getResponseAsType(exceptionThrownOnNonSuccess, ErrorResponse.class);
            scenario.log("Error response received: " + errorResponse.toString());
            scenario.log("Looking for error reason " + expectedSubstringInErrorBody);
            String finalExpectedSubstringInErrorBody = expectedSubstringInErrorBody;
            assertTrue("Should contain an error message with a message", errorResponse.getErrorReasons()
                    .stream()
                    .map(ErrorReason::getExplanation)
                    .anyMatch(errorExplanation -> errorExplanation.contains(finalExpectedSubstringInErrorBody)));
        });
        Then("the response body should be an assessment", () ->  assertNotNull("Response body should not be null.  If it is, there was an error, or empty body returned", responseBody));
        Then("the response body should have isbn specified above", () -> assertEquals("Isbn in response body should equal the one passed in the request", isbnUsedInRequest, responseBody.getIsbn()));
        Then("the response body should have reading level {string}", (String readingLevelStr) -> {
            Float expectedReadingLevel = Float.parseFloat(readingLevelStr);
            assertEquals("Reading level in response body should match that specified in feature file", expectedReadingLevel, responseBody.getReadingLevel());
        });
        Then("the response body should have number of points {string}", (String numberOfPointsStr) -> {
            Float expectedNumberOfPoints = Float.parseFloat(numberOfPointsStr);
            assertEquals("Number of points in response body should match that specified in feature file", expectedNumberOfPoints, responseBody.getNumberOfPoints());
        });
        Then("the response body should have created teacher id {string}", (String createdTeacherIdStr) -> {
            Integer expectedCreatedTeacherId = Integer.parseInt(createdTeacherIdStr);
            assertEquals("The created teacher id in the response body should equal the expected created teacher id specified in the feature file", expectedCreatedTeacherId, responseBody.getCreatedTeacherId());
        });
        Then("the response body should have title {string}", (String expectedTitleString) -> {
            expectedTitleString = expectedTitleString.trim();
            assertEquals("Title in response body should match that specified in the feature file", expectedTitleString, responseBody.getTitle().trim());
        });
        Then("the response body should have author first name {string}", (String expectedAuthorFirstName) -> {
            expectedAuthorFirstName = expectedAuthorFirstName.trim();
            assertEquals("The response body should have author first name that matches that specified in feature file", expectedAuthorFirstName, responseBody.getAuthorFirstName());
        });
        Then("the response body should have author last name {string}", (String expectedAuthorLastName) -> {
            expectedAuthorLastName = expectedAuthorLastName.trim();
            assertEquals("The response body should have the author last name specified in the feature file", expectedAuthorLastName, responseBody.getAuthorLastName());
        });
        Then("the request should not be processed \\(and instead receive a mock response)", () ->
             assertNotNull("Response body being null indicates health check failed", responseBody)
        );
        Then("the response body should be a \\(not null) assessment", () ->
            assertNotNull("Response body being null indicates health check failed", responseBody)
        );
    }
}
