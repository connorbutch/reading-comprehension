package com.connor.reading.util;

import com.connor.reading.ApiException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;

public class ErrorResponseUtil {

    //NEVER, EVER use static methods except for testing!!!!!
    public static <T> T getResponseAsType(ApiException e, Class<T> clazz){
        try {
            return new ObjectMapper()
                    .registerModule(new JavaTimeModule())
                    .readValue(e.getResponseBody(), clazz);
        } catch (IOException ioException) {
            throw new RuntimeException("Error reading error response body as type", ioException);
        }
    }
}
