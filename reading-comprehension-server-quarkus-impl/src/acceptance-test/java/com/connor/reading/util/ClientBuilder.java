package com.connor.reading.util;

import com.connor.reading.ApiClient;
import com.connor.reading.client.AssessmentsApi;
import com.connor.reading.client.HealthApi;
import com.connor.reading.model.HeaderConstants;

import java.net.http.HttpHeaders;
import java.util.Objects;
import java.util.Optional;

public class ClientBuilder {
    private static final String BASE_URL_SYS_PROPERTY = "BASE_URL";
    private static final String SCHEME = "http";
    private static final int PORT = 8080;

    //store this off as it contains the only way of getting headers in the native java 11 client (via the response interceptors)
    private static HttpHeaders httpHeadersFromLastRequest;
    private static int httpStatusCodeFromLastRequest;

    //NEVER, EVER use static methods except for convenience when testing!!!!!
    public static HealthApi getApiForTests() {
        ApiClient apiClient = getApiClient(false);
        return new HealthApi(apiClient);
    }

    //NEVER, EVER use static methods except for convenience when testing!!!!!
    public static AssessmentsApi getAssessmentApiForTests() {
        return getAssessmentApiForTests(false);
    }

    public static AssessmentsApi getAssessmentApiForTests(boolean isConnectivityRequest) {
        ApiClient apiClient = getApiClient(isConnectivityRequest);
        return new AssessmentsApi(apiClient);
    }

    public static HttpHeaders getHttpHeadersFromLastResponse() {
        return httpHeadersFromLastRequest;
    }

    public static int getHttpStatusCodeFromLastRequest() {
        return httpStatusCodeFromLastRequest;
    }

    private static Optional<String> getHost() {
        return Optional.ofNullable(System.getProperty(BASE_URL_SYS_PROPERTY));
    }

    private static ApiClient getApiClient(boolean isConnectivityRequest) {
        String host = getHostOrThrow();
        ApiClient apiClient = new ApiClient()
                .setScheme(SCHEME)
                .setHost(host)
                .setPort(PORT)
                .setResponseInterceptor(inputStreamHttpResponse -> {
                    httpStatusCodeFromLastRequest = inputStreamHttpResponse.statusCode();
                    httpHeadersFromLastRequest = inputStreamHttpResponse.headers();
                });
        if (isConnectivityRequest) {
            apiClient.setRequestInterceptor(request -> {
                        System.out.println("This is a connectivity check, so setting " + HeaderConstants.HEALTH_CHECK_REQUEST_HEADER + " on outgoing request");
                        request.setHeader(HeaderConstants.HEALTH_CHECK_REQUEST_HEADER, Boolean.TRUE.toString());
                    }
            );
        }
        return apiClient;
    }

    private static String getHostOrThrow() {
        return getHost()
                .filter(Objects::nonNull)
                .filter(string -> !string.isBlank())
                .orElseThrow(() -> new RuntimeException(String.format("Please set the %s system property", BASE_URL_SYS_PROPERTY)));
    }
}
