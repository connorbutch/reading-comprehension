package com.connor.reading.query.question;

import com.connor.reading.ApiException;
import com.connor.reading.client.AssessmentsApi;
import com.connor.reading.client.dto.Question;
import com.connor.reading.util.ClientBuilder;
import io.cucumber.java8.En;
import io.cucumber.java8.Scenario;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"}, features = {"src/acceptance-test/resources/features/GetQuestionsForAssessment.feature"})
public class RunGetQuestionsForAssessmentIT implements En {
    private AssessmentsApi assessmentsApi;
    private Scenario scenario;
    private Long isbnPathParameter;
    private List<Question> responseBody;
    private ApiException exceptionThrownOnNonSuccess;
    private Question questionMatchingCriteria;

    public RunGetQuestionsForAssessmentIT(){
        Before((Scenario scenario) -> {
           this.scenario = scenario;
           this.scenario.log("Getting new (non-health check) generated client");
           this.assessmentsApi = ClientBuilder.getAssessmentApiForTests();
           isbnPathParameter = null;
           responseBody = null;
           exceptionThrownOnNonSuccess = null;
           questionMatchingCriteria = null;
        });
        Given("the isbn used as a path parameter is {string}", (String isbnStr) ->
            isbnPathParameter = Long.parseLong(isbnStr)
        );
        Given("the isbn used as a path parameter is {long}", (Long isbnPathParameter) ->
            this.isbnPathParameter = isbnPathParameter
        );
        Given("the connectivity request is valid", () -> {
            scenario.log("Getting generated client for health check");
            assessmentsApi = ClientBuilder.getAssessmentApiForTests(true);
            scenario.log("Setting isbn path parameter to an arbitrary number so that it routes to the correct endpoint");
            isbnPathParameter = 123L;
        });
        When("a request is made to retrieve questions for this isbn", () -> {
            try {
                responseBody = assessmentsApi.getQuestionsForAssessment(isbnPathParameter);
            }catch (ApiException e){
                this.exceptionThrownOnNonSuccess = e;
            }
        });
        Then("the response status code should be {int}", (Integer expectedHttpStatusCode) -> {
            if(expectedHttpStatusCode > 199 && expectedHttpStatusCode < 300){
                assertNotNull("Response body not null indicates 2xx response code", responseBody);
            }else{
                assertEquals("", expectedHttpStatusCode, Integer.valueOf(exceptionThrownOnNonSuccess.getCode()));
            }
        });
        Then("the response body error message should contain {string}", (String string) -> {
            //TODO
        });
        Then("the response body should be a list of questions", () ->
            assertNotNull("Response body being not null indicates valid return type", responseBody)
        );
        Then("the list of questions should be empty", () ->
            assertEquals("Returned list should have size of 0", 0, responseBody.size())
        );
        Then("there should be {string} questions in the returned list", (String sizeStr) ->
            assertEquals("Number of questions should match expected size", Integer.parseInt(sizeStr), responseBody.size())
        );
        Then("one of the questions in the list should have question text {string}", (String expectedQuestionText) -> {
            questionMatchingCriteria = responseBody.stream()
                    .filter(question -> question.getQuestionText().trim().equals(expectedQuestionText.trim()))
                    .findFirst()
                    .get(); //let this throw if not present -- will fail tests anyway
            assertNotNull("Should have question with matching text", questionMatchingCriteria);
        });
        Then("the correct answer for that question should be {string}", (String expectedCorrectAnswer) ->
            assertEquals("Expected correct answer should match", expectedCorrectAnswer.trim(), questionMatchingCriteria.getCorrectAnswer().trim())
        );
        Then("there should be {string} incorrect answers for that question", (String expectedNumberOfIncorrectAnswersStr) ->
            assertEquals("Number of incorrect answers should match expected", Integer.parseInt(expectedNumberOfIncorrectAnswersStr), questionMatchingCriteria.getIncorrectAnswers().size())
        );
        Then("none of the incorrect answers should be blank", () ->
            assertTrue("No incorrect answer text should be empty", questionMatchingCriteria.getIncorrectAnswers().stream().noneMatch(String::isBlank))
        );
        Then("the request should not be processed \\(and instead receive a mock response)", () ->
            assertNotNull("Response body being null indicates health check failed", responseBody)
        );
        Then("there should be one question in the list", () ->
            assertEquals("There should be exactly one entry in the response body", 1, responseBody.size())
        );
        Then("that question should not be null", () ->
            assertNotNull("Entry in list should not be null", responseBody.get(0))
        );
    }
}
