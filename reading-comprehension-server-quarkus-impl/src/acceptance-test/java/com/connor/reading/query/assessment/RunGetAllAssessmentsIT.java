package com.connor.reading.query.assessment;


import com.connor.reading.ApiException;
import com.connor.reading.client.AssessmentsApi;
import com.connor.reading.client.dto.Assessment;
import com.connor.reading.client.dto.ErrorResponse;
import com.connor.reading.util.ClientBuilder;
import com.connor.reading.util.ErrorResponseUtil;
import io.cucumber.java8.En;
import io.cucumber.java8.Scenario;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import java.net.http.HttpHeaders;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.*;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"}, features = {"src/acceptance-test/resources/features/GetAllAssessments.feature"})
public class RunGetAllAssessmentsIT implements En {
    private static final int OK_STATUS_CODE = 200;

    private AssessmentsApi assessmentsApi;
    private Scenario scenario;
    private Integer limitUsedInRequest;
    private Integer offsetUsedInRequest;
    private List<Assessment> successResponseBody;
    private ApiException exceptionThrownOnNonSuccess;

    public RunGetAllAssessmentsIT() {
        Before((Scenario scenario) -> {
            this.scenario = scenario;
            this.scenario.log("Getting a new instance of the generated client that does NOT set the connectivity header");
            assessmentsApi = ClientBuilder.getAssessmentApiForTests();
            this.limitUsedInRequest = null;
            this.offsetUsedInRequest = null;
            this.successResponseBody = null;
            this.exceptionThrownOnNonSuccess = null;
        });
        Given("a limit value of {int} is passed in the request", (Integer limitPassedInRequest) -> {
            this.limitUsedInRequest = limitPassedInRequest;
        });
        Given("an offset value of {int} is passed in the request", (Integer offsetPassedInRequest) -> {
            this.offsetUsedInRequest = offsetPassedInRequest;
        });
        Given("the connectivity request is valid", () -> {
            scenario.log("Getting generated client that adds connectivity request header");
            assessmentsApi = ClientBuilder.getAssessmentApiForTests(true);
        });
        When("a request is made to the get all assessments endpoint", () -> {
            try {
                successResponseBody = assessmentsApi.getAssessments(limitUsedInRequest, offsetUsedInRequest);
            } catch (ApiException e) {
                this.exceptionThrownOnNonSuccess = e;
            }
        });
        Then("the response status code should be {int}", (Integer expectedHttpResponseCode) -> {
            Integer actualHttpResponseCode = successResponseBody != null ? OK_STATUS_CODE : exceptionThrownOnNonSuccess.getCode();
            assertEquals("The http response code should be the expected code", expectedHttpResponseCode, actualHttpResponseCode);
        });
        Then("the response body should be a populated list", () -> {
            assertNotNull("Response body should not be null", successResponseBody);
        });
        Then("the list should have {int} assessments in it", (Integer expectedNumberOfAssessments) -> {
            assertEquals("The list should have the expected number of assessments in it", expectedNumberOfAssessments.intValue(), successResponseBody.size());
        });
        Then("each entry in the list should have a non null isbn", () -> {
            successResponseBody.forEach(assessment -> assertNotNull("Isbn should not be null", assessment.getIsbn()));
        });
        Then("each entry in the list should have a non null title", () -> {
            successResponseBody.forEach(assessment -> assertNotNull("Title should not be null", assessment.getTitle()));
        });
        Then("each entry in the list should have a non null created teacher id", () -> {
            successResponseBody.forEach(assessment -> assertNotNull("Created teacher id should not be null", assessment.getCreatedTeacherId()));
        });
        Then("each entry in the list should have a non null reading level", () ->
            successResponseBody.forEach(assessment -> assertNotNull("Reading level should not be null", assessment.getReadingLevel()))
        );
        Then("each entry in the list should have a non null number of points assigned", () ->
            successResponseBody.forEach(assessment -> assertNotNull("Number of points should not be null", assessment.getNumberOfPoints()))
        );
        Then("each entry in the list should have a non null author", () ->
            successResponseBody.forEach(assessment -> assertNotNull("Author should not be null", assessment.getAuthorFirstName()))
        );
        Then("one of the assessments should have isbn of {long}", (Long oneExpectedIsbn) -> {
            assertThat("One of the assessments returned should have the expected isbn",
                    successResponseBody.stream().map(Assessment::getIsbn).collect(Collectors.toList()),
                    hasItem(oneExpectedIsbn));
        });
        Then("the response body should be an empty list", () -> {
            assertNotNull("Response body should not be null, but rather an EMPTY list", successResponseBody);
            assertTrue("Response body should be empty list", successResponseBody.isEmpty());
        });
        Then("the x-total-assessments header value should be returned as {int}", (Integer expectedTotalAssessmentHeader) -> {
            HttpHeaders httpHeaders = ClientBuilder.getHttpHeadersFromLastResponse();
            assertNotNull("Headers should not be null", httpHeaders);
            List<String> xTotalAssessmentHeader = httpHeaders.map().get("x-total-assessments");
            assertNotNull("x-total-assessments header should not be null", xTotalAssessmentHeader);
            assertEquals("Total number of assessment header should match the expected value", expectedTotalAssessmentHeader,
                    Integer.valueOf(xTotalAssessmentHeader.get(0)));
        });
        Then("the response body should be an error response", () -> {
            assertNotNull("There should be an error response", exceptionThrownOnNonSuccess);
            ErrorResponse e = ErrorResponseUtil.getResponseAsType(exceptionThrownOnNonSuccess, ErrorResponse.class);
        });
        Then("the response body error message should contain the string {string}", (String expectedErrorSubstring) -> {
            ErrorResponse e = ErrorResponseUtil.getResponseAsType(exceptionThrownOnNonSuccess, ErrorResponse.class);
            //assertThat("The error message should contain the expected string", expectedErrorSubstring, containsString(e.getMessage()));
        });
        //I'd like to remove these, but for some reason, it doesn't pick the one above this up
        Then("the response body error message should contain the string Limit must be greater than zero", () -> {
            ErrorResponse e = ErrorResponseUtil.getResponseAsType(exceptionThrownOnNonSuccess, ErrorResponse.class);
            //assertThat("The error message should contain the expected string", e.getMessage(), containsString("Limit must be greater than zero"));
        });
        Then("the response body error message should contain the string Offset must be zero or greater", () -> {
            ErrorResponse e = ErrorResponseUtil.getResponseAsType(exceptionThrownOnNonSuccess, ErrorResponse.class);
            //assertThat("The error message should contain the expected string", e.getMessage(), containsString("Offset must be zero or greater"));
        });
        Then("the response body error should an error reason with errorId {string}", (String string) -> {
            // TODO
        });
        Then("the error reason in the response body with the given error reason should have a message containing the string {string}", (String string) -> {
            // TODO
        });
        Then("the request should not be processed \\(and instead receive a mock response)", () ->
                assertNotNull("Response body not being null indicates success of health check", successResponseBody)
        );
        Then("the response body should be a list of assessments", () ->
                assertNotNull("Response body not being null indicates success of health check", successResponseBody)
        );
        Then("the list should have one assessment in it", () ->
           assertEquals("List should have exactly one assessment in it", 1, successResponseBody.size())
        );
    }
}
