package com.connor.reading.add;

import com.connor.reading.ApiException;
import com.connor.reading.client.AssessmentsApi;
import com.connor.reading.client.dto.Assessment;
import com.connor.reading.client.dto.ErrorReason;
import com.connor.reading.client.dto.ErrorResponse;
import com.connor.reading.util.ClientBuilder;
import com.connor.reading.util.ErrorResponseUtil;
import io.cucumber.java8.En;
import io.cucumber.java8.Scenario;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.hamcrest.Matchers;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.*;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"}, features = {"src/acceptance-test/resources/features/AddAssessment.feature"})
public class RunAddAssessmentIT implements En {
    private static final String NULL = "NULL";
    private static final int TEACHER_ID_FOR_WHICH_TEACHER_SERVICE_THROWS_500 = 3;

    private AssessmentsApi assessmentsApi;
    private Scenario scenario;
    private Assessment requestBody;
    private Assessment responseBody;
    private ApiException exceptionThrownOnNonSuccess;
    private ErrorReason reasonWithGivenErrorId;

    public RunAddAssessmentIT() {
        Before((Scenario scenario) -> {
            this.scenario = scenario;
            assessmentsApi = ClientBuilder.getAssessmentApiForTests();
            requestBody = null;
            responseBody = null;
            exceptionThrownOnNonSuccess = null;
            reasonWithGivenErrorId = null;
        });
        Given("the assessment to be added has an isbn of {string}", (String isbnStr) -> {
            initializeRequestBodyIfNeeded();
            Long isbn = NULL.equalsIgnoreCase(isbnStr) ? null : Long.parseLong(isbnStr);
            requestBody.setIsbn(isbn);
        });
        Given("the teacher service is temporarily down", () -> {
            initializeRequestBodyIfNeeded();
            requestBody.setCreatedTeacherId(TEACHER_ID_FOR_WHICH_TEACHER_SERVICE_THROWS_500);
        });
        Given("the other fields in the request are valid", () -> {
            initializeRequestBodyIfNeeded();
            requestBody = requestBody.authorFirstName("First")
                    .authorLastName("Last")
                    .numberOfPoints(7.0F)
                    .readingLevel(8.0F)
                    .title("Title");
            if(requestBody.getCreatedTeacherId() == null){
                scenario.log("The created teacher id is null so setting to a valid value");
                requestBody.setCreatedTeacherId(1);
            }else{
                scenario.log("Created teacher id isn't null, so not setting value to valid");
            }
        });
        Given("the assessment to be added has a title of {string}", (String title) -> {
            initializeRequestBodyIfNeeded();
            title = NULL.equalsIgnoreCase(title) ? null : title.trim();
            requestBody.setTitle(title);
        });
        Given("the assessment to be added has an author first name of {string}", (String authorFirstName) -> {
            initializeRequestBodyIfNeeded();
            authorFirstName = NULL.equalsIgnoreCase(authorFirstName) ? null : authorFirstName.trim();
            requestBody.setAuthorFirstName(authorFirstName);
        });
        Given("the assessment to be added has an author last name of {string}", (String authorLastName) -> {
            initializeRequestBodyIfNeeded();
            authorLastName = NULL.equalsIgnoreCase(authorLastName) ? null : authorLastName.trim();
            requestBody.setAuthorLastName(authorLastName);
        });
        Given("the assessment to be added has a reading level of {string}", (String readingLevelString) -> {
            initializeRequestBodyIfNeeded();
            Float readingLevel = NULL.equalsIgnoreCase(readingLevelString) ? null : Float.parseFloat(readingLevelString.trim());
            requestBody.setReadingLevel(readingLevel);
        });
        Given("the assessment to be added has a number of points {string}", (String numberOfPointsString) -> {
            initializeRequestBodyIfNeeded();
            Float numberOfPoints = NULL.equalsIgnoreCase(numberOfPointsString) ? null : Float.parseFloat(numberOfPointsString.trim());
            requestBody.setNumberOfPoints(numberOfPoints);
        });
        Given("the assessment to be added has a created teacher id of {string}", (String createdTeacherIdString) -> {
            initializeRequestBodyIfNeeded();
            Integer createdTeacherId = NULL.equalsIgnoreCase(createdTeacherIdString) ? null : Integer.parseInt(createdTeacherIdString.trim());
            requestBody.setCreatedTeacherId(createdTeacherId);
        });
        Given("the assessment to be added has an isbn of {long}", (Long isbnToBeAdded) -> {
            initializeRequestBodyIfNeeded();
            requestBody.setIsbn(isbnToBeAdded);
        });
        Given("a connectivity request with the connectivity header has been received", () ->
            assessmentsApi = ClientBuilder.getAssessmentApiForTests(true)
        );
        Given("the connectivity request is valid", () ->
            requestBody = new Assessment()
                    .isbn(12L)
                    .title("Title")
                    .readingLevel(6.0F)
                    .numberOfPoints(13.0F)
                    .authorFirstName("First")
                    .authorLastName("Last")
                    .createdTeacherId(1)
        );
        When("a request is made to the add assessment endpoint", () -> {
            try {
                responseBody = assessmentsApi.createAssessment(requestBody);
                scenario.log("Successfully made request for body " + requestBody + " and received response " + responseBody);
            } catch (ApiException e) {
                exceptionThrownOnNonSuccess = e;
                scenario.log("Received an error response when making request: ." + requestBody + "  This doesn't mean anything bad, it could be the client supplied bad data");
            }
        });
        Then("the response status code should be {int}", (Integer expectedHttpStatusCode) -> {
            scenario.log("Looking for response status code for " + expectedHttpStatusCode);
            if (expectedHttpStatusCode > 199 && expectedHttpStatusCode < 300) {
                scenario.log("Looking for a success response.  We will check response body is not null");
                assertNotNull("Response body being not null indicates we got a 2xx status code", responseBody);
            } else {
                scenario.log("We are looking for a non-success response status code (" + expectedHttpStatusCode + ") so we will check the code from the exception");
                if(exceptionThrownOnNonSuccess == null){
                    scenario.log("Exception thrown from request is null; that likely means we are looking for an error response");
                }
                assertEquals("Status code should match", expectedHttpStatusCode, Integer.valueOf(exceptionThrownOnNonSuccess.getCode()));
            }
        });
        Then("the response body should be an error response", () -> {
            assertNotNull("There should be an exception thrown", exceptionThrownOnNonSuccess);
            ErrorResponse errorResponse = ErrorResponseUtil.getResponseAsType(exceptionThrownOnNonSuccess, ErrorResponse.class);
            assertNotNull("The error response body should be able to be converted to an error response", errorResponse);
        });
        Then("the response body error should an error reason with errorId {string}", (String expectedErrorIdString) -> {
            Integer expectedErrorId = Integer.parseInt(expectedErrorIdString);
            ErrorResponse errorResponse = ErrorResponseUtil.getResponseAsType(exceptionThrownOnNonSuccess, ErrorResponse.class);
            if (errorResponse == null || errorResponse.getErrorReasons() == null || errorResponse.getErrorReasons().isEmpty()) {
                String messageForLog = "Error response did not have expected information";
                scenario.log(messageForLog);
                fail(messageForLog);
            } else {
                scenario.log("Error ids present " + errorResponse.getErrorReasons().stream().map(ErrorReason::getErrorId).map(Object::toString).collect(Collectors.joining(", ")) + " error id expected " + expectedErrorId);
                reasonWithGivenErrorId = errorResponse.getErrorReasons()
                        .stream()
                        .filter(errorReason -> errorReason != null && errorReason.getErrorId().equals(expectedErrorId))
                        .findFirst()
                        .orElse(null);
                List<Integer> errorIds = errorResponse.getErrorReasons()
                        .stream()
                        .map(ErrorReason::getErrorId)
                        .collect(Collectors.toList());
                assertThat("Should contain expected error id", errorIds, hasItem(expectedErrorId));
            }
        });
        Then("the error reason in the response body with the given error reason should have a message containing the string {string}", (String expectedErrorSubstring) ->
                assertThat("Error reason with matching id should contain the substring", reasonWithGivenErrorId.getExplanation(), Matchers.containsString(expectedErrorSubstring.trim()))
        );
        Then("the response body should be an assessment", () ->
                assertNotNull("Response body should be a not null assessment", responseBody)
        );
        Then("the response body should have the isbn specified above", () ->
                assertEquals("Response body should have isbn specified", requestBody.getIsbn(), responseBody.getIsbn())
        );
        Then("the response body should have the title specified above", () ->
                assertEquals("Response body should have title specified", requestBody.getTitle(), responseBody.getTitle())
        );
        Then("the response body should have the first name specified above", () ->
                assertEquals("Response body should have first name specified", requestBody.getAuthorFirstName(), responseBody.getAuthorFirstName())
        );
        Then("the response body should have the last name specified above", () ->
                assertEquals("Response body should have last name specified", requestBody.getAuthorLastName(), responseBody.getAuthorLastName())
        );
        Then("the response body should have the reading level specified above", () ->
                assertEquals("Response body should have reading level specified", requestBody.getReadingLevel(), responseBody.getReadingLevel())
        );
        Then("the response body should have the number of points specified above", () ->
                assertEquals("Number of points in response should match that in request", requestBody.getNumberOfPoints(), responseBody.getNumberOfPoints())
        );
        Then("the response body should have the created teacher id specified above", () -> {
            assertEquals("Created teacher id in response should match that in request", requestBody.getCreatedTeacherId(), responseBody.getCreatedTeacherId());
        });
        Then("the response body error should an error reason with errorId {int}", (Integer expectedErrorId) -> {
            ErrorResponse errorResponse = ErrorResponseUtil.getResponseAsType(exceptionThrownOnNonSuccess, ErrorResponse.class);
            reasonWithGivenErrorId = errorResponse.getErrorReasons()
                    .stream()
                    .filter(errorReason -> errorReason != null && errorReason.getErrorId().equals(expectedErrorId))
                    .findFirst()
                    .orElse(null);
            assertNotNull("An error reason with the given id should be present", reasonWithGivenErrorId);
        });
        Then("the error reason in the response body with the given error reason should have a message containing the string Unexpected error occurred", () ->
                assertThat("Error reason with matching id should contain the substring", reasonWithGivenErrorId.getExplanation(), Matchers.containsString("Unexpected error"))
        );
        Then("the request should not be processed \\(and instead receive a mock response)", () ->
            assertNotNull("Response should be  mocked response", responseBody)
        );
    }

    private void initializeRequestBodyIfNeeded() {
        if (requestBody == null) {
            requestBody = new Assessment();
        }
    }
}
