package com.connor.reading.cdi;

import com.connor.quarkus.jdbc.QuarkusJdbcTemplate;
import com.connor.reading.controller.HealthApiImpl;
import org.jboss.logging.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import javax.enterprise.inject.spi.InjectionPoint;
import javax.sql.DataSource;
import java.lang.reflect.Member;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CdiProducerUtilTest {

    @InjectMocks
    private CdiProducerUtil cdiProducerUtil;

    @Test
    public void whenGetLoggerThenReturnLogger(){
        Member mockedMember = mock(Member.class);
        InjectionPoint mockedInjectionPoint = mock(InjectionPoint.class);
        Class mockedClass = HealthApiImpl.class; //generics/wildcards don't work with mockito well
        when(mockedMember.getDeclaringClass()).thenReturn(mockedClass);
        when(mockedInjectionPoint.getMember()).thenReturn(mockedMember);

        Logger logger = cdiProducerUtil.getLogger(mockedInjectionPoint);

        assertNotNull("Returned logger should not be null", logger);
    }

    @Test
    public void whenGetManagedExecutorReturnNotNull(){
        assertNotNull("Managed executor should not be null", cdiProducerUtil.getManagedExecutor(null));
    }

    @Test
    public void whenGetJdbcTemplateReturnNotNull(){
        assertNotNull("Returned jdbc template should not be null", cdiProducerUtil.getQuarkusJdbcTemplate(mock(DataSource.class)));
    }

    @Test
    public void whenGetNamedTemplateReturnNotNull(){
        assertNotNull("Named tempalte should not be null", cdiProducerUtil.getQuarkusNamedJdbcTemplate(mock(QuarkusJdbcTemplate.class)));
    }

    @Test
    public void whenGetMapThenReturnNotNull(){
        assertNotNull("Produced map should not be null", cdiProducerUtil.getCache(null));
    }

    @Test
    public void whenGetErroReasonsMapThenREturnNotNull(){
        assertNotNull("Map shouldn't be null", cdiProducerUtil.getErrorReasonsMap(mock(InjectionPoint.class)));
    }
}