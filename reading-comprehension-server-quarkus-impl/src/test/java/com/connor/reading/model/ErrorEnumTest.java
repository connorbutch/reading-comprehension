package com.connor.reading.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class ErrorEnumTest {

    @Test
    public void testNoEnumShouldHaveDuplicateId(){
        Set<Integer> existingIds = new TreeSet<>();
        for(ErrorEnum errorEnum: ErrorEnum.values()){
            boolean didGetAdded = existingIds.add(errorEnum.getErrorId());
            assertTrue("Each error enum should have a unique id.  Testing id " + errorEnum.getErrorId(), didGetAdded);
        }
    }

}