package com.connor.reading;

/**
 * Since we are forced (because of quarkus bom enforced implementation) to use junit 4.12, which does not
 * have support for assert throws, I have written a custom utility that works the same as Assert.assertThrows(...._)
 */
public class CustomAssert {

    public static <T extends Throwable> T assertThrows(Class<T> clazz, Executable methodToExecute){
        try{
            methodToExecute.execute();
        }catch(Throwable thrown){
            if(thrown.getClass() == clazz){
                return (T) thrown;
            }
        }
        throw new AssertionError("Didn't throw expected exception");
    }
}
