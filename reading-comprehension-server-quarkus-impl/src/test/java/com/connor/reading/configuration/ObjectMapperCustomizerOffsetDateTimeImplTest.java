package com.connor.reading.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ObjectMapperCustomizerOffsetDateTimeImplTest {
    @InjectMocks
    private ObjectMapperCustomizerOffsetDateTimeImpl objectMapperCustomizerOffsetDateTime;

    @Captor
    private ArgumentCaptor<SerializationFeature> serializationFeatureArgumentCaptor;

    @Test
    public void whenModifyObjectMapperDisableWriteDateAsTimestamp(){
        ObjectMapper objectMapper = mock(ObjectMapper.class);
        objectMapperCustomizerOffsetDateTime.customize(objectMapper);
        verify(objectMapper, times(1)).disable(serializationFeatureArgumentCaptor.capture());
        assertEquals("Serialization feature for writing date as timestamp should be disabled", SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, serializationFeatureArgumentCaptor.getValue());
    }
}