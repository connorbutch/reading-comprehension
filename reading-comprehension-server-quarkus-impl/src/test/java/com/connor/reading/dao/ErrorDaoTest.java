package com.connor.reading.dao;

import com.connor.reading.dto.ErrorReason;
import com.connor.reading.model.ErrorEnum;
import org.jboss.logging.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ErrorDaoTest {
    @Mock
    private Logger logger;
    @Mock
    private Map<Integer, ErrorReason> errorReasonsMappedToId;
    @InjectMocks
    private ErrorDao errorDao;

    @Captor
    private ArgumentCaptor<Integer> errorIdCaptor;
    @Captor
    private ArgumentCaptor<ErrorReason> errorReasonCaptor;

    @Test
    public void whenConstructMapInsertErrorEnum(){
        verify(errorReasonsMappedToId, times(ErrorEnum.values().length)).put(errorIdCaptor.capture(), errorReasonCaptor.capture());
    }

    @Test
    public void whenGetErrorReasonThenReturnValueFromMap(){
//        when(errorReasonsMappedToId.get(12)).thenReturn(null);
//        final ErrorReason errorReason = new ErrorReason().errorId(992);
//        when(errorReasonsMappedToId.get(992)).thenReturn(errorReason);
//        Optional<ErrorReason> resultFromGet12 = errorDao.getDetailedErrorInfoById(12);
//        assertTrue("When map is empty then return value",resultFromGet12.isEmpty());
//        Optional<ErrorReason> optionalFrom992 = errorDao.getDetailedErrorInfoById(992);
//        assertTrue("Should be non-empty when exists", optionalFrom992.isPresent());
    }
}