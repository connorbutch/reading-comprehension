package com.connor.reading.dao;

import com.connor.reading.dao.extractor.QuarkusRowMapperAssessmentImpl;
import com.connor.reading.dto.Assessment;
import org.jboss.logging.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class QuarkusRowMapperAssessmentImplTest {
    @Mock
    private Logger logger;
    @InjectMocks
    private QuarkusRowMapperAssessmentImpl quarkusRowMapperAssessment;

    @Test
    public void whenMapRowThenReturnPopulatedObject() throws SQLException {
        final String authorFirstName = "FirstName";
        final String authorLastName = "Last-Name";
        final Integer createdTeacherId = 33231;
        final Long isbn = 9989L;
        final Float numberOfPoints = 3.2F;
        final Float readingLevel = 4.5F;
        final String title = "some title; yeah!";
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.getString("AUTHOR_FIRST_NAME")).thenReturn(authorFirstName);
        when(resultSet.getString("AUTHOR_LAST_NAME")).thenReturn(authorLastName);
        when(resultSet.getObject("CREATED_TEACHER_ID", Integer.class)).thenReturn(createdTeacherId);
        when(resultSet.getObject("ISBN", Long.class)).thenReturn(isbn);
        when(resultSet.getFloat("NUMBER_OF_POINTS")).thenReturn(numberOfPoints);
        when(resultSet.getFloat("READING_LEVEL")).thenReturn(readingLevel);
        when(resultSet.getString("TITLE")).thenReturn(title);
        Assessment expectedAssessment = new Assessment()
                .authorFirstName(authorFirstName)
                .authorLastName(authorLastName)
                .createdTeacherId(createdTeacherId)
                .isbn(isbn)
                .numberOfPoints(numberOfPoints)
                .readingLevel(readingLevel)
                .title(title);
        Assessment actual = quarkusRowMapperAssessment.mapRow(resultSet, 0);
        assertEquals("Value should be mapped correctly", expectedAssessment, actual);
    }

}