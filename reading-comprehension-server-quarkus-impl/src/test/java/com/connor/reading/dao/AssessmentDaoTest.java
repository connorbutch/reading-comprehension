package com.connor.reading.dao;

import com.connor.quarkus.jdbc.QuarkusDataAccessException;
import com.connor.quarkus.jdbc.QuarkusJdbcTemplate;
import com.connor.quarkus.jdbc.QuarkusNamedJdbcTemplate;
import com.connor.quarkus.jdbc.QuarkusRowMapper;
import com.connor.reading.CustomAssert;
import com.connor.reading.bean.HealthCheckBean;
import com.connor.reading.dto.Assessment;
import com.connor.reading.exception.ReadingSystemException;
import com.connor.reading.util.SqlProvider;
import org.hamcrest.Matchers;
import org.jboss.logging.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AssessmentDaoTest {
    @Mock
    private Logger logger;
    @Mock
    private QuarkusJdbcTemplate jdbcTemplate;
    @Mock
    private QuarkusNamedJdbcTemplate quarkusNamedJdbcTemplate;;
    @Mock
    private QuarkusRowMapper<Assessment> assessmentRowMapper;
    @Mock
    private SqlProvider sqlProvider;
    @Mock
    private HealthCheckBean healthCheckBean;
    @InjectMocks
    private AssessmentDao assessmentDao;

    @Captor
    private ArgumentCaptor<String> sqlCaptor;
    @Captor
    private ArgumentCaptor<Map<String, ?>> mapArgumentCaptor;
    @Captor
    private ArgumentCaptor<QuarkusRowMapper<?>> rowMapperCaptor;
    @Captor
    private ArgumentCaptor<Boolean> isHealthCheckCaptor;

    @Test
    public void whenProviderReturnsEmptyThenThrow(){
        when(sqlProvider.apply(any())).thenReturn(Optional.empty());
        ReadingSystemException thrown = CustomAssert.assertThrows(ReadingSystemException.class, () -> assessmentDao.getAllAssessments());
        assertThat(thrown.getCause(), nullValue());
        assertEquals("Error message should be equals", "No sql found for key GET_ALL_ASSESSMENTS", thrown.getMessage());
    }

    @Test
    public void whenGetAllAssessmentsCallTemplate(){
        final String sql = "Some sql here";
        when(sqlProvider.apply(any())).thenReturn(Optional.ofNullable(sql));
       assessmentDao.getAllAssessments();
       verify(sqlProvider, times(1)).apply(any());
       verify(jdbcTemplate, times(1)).query(sqlCaptor.capture(), any(QuarkusRowMapper.class));
       assertEquals("Sql ran should match sql from provider", sql, sqlCaptor.getValue());
    }

    @Test
    public void whenGetAllAssessmentsThrowsExceptionThenRethrow(){
        final String sql = "Some sql here";
        when(sqlProvider.apply(any())).thenReturn(Optional.of(sql));
        final QuarkusDataAccessException dataAccessException = mock(QuarkusDataAccessException.class);
        when(jdbcTemplate.query(any(String.class), any(QuarkusRowMapper.class))).thenThrow(dataAccessException);
        ReadingSystemException thrown = CustomAssert.assertThrows(ReadingSystemException.class, () -> assessmentDao.getAllAssessments());
        assertThat("", thrown.getMessage(), Matchers.containsString("There was an error when querying the database for all assessments.  The sql ran was Some sql here"));
        assertEquals("Inner exception should propagate as cause", thrown.getCause(), dataAccessException);
    }

    @Test
    public void whenGetAllAssessmentsReturnFromJdbcTemplate(){
        final String sql = "Some sql here";
        when(sqlProvider.apply(any())).thenReturn(Optional.ofNullable(sql));
        final List<Assessment> expectedAssessments = Collections.unmodifiableList(Collections.emptyList());
        when(jdbcTemplate.query(anyString(), any(QuarkusRowMapper.class))).thenReturn(expectedAssessments);
        List<Assessment> actualAssessmentsReturned = assessmentDao.getAllAssessments();
        assertThat("List returned should be that from jdbc template", actualAssessmentsReturned, equalTo(expectedAssessments));
    }

    @Test
    public void whenGetAllWithLimitAndProviderReturnsEmptyThenThrow(){
        when(sqlProvider.apply(any())).thenReturn(Optional.empty());
        ReadingSystemException thrown = CustomAssert.assertThrows(ReadingSystemException.class, () ->
                assessmentDao.getAssessmentsForLimitAndOffset(0, 0));
        assertNull("Cause should be null", thrown.getCause());
        assertThat("Should contain expected error message", thrown.getMessage(), Matchers.containsString("No sql found for key GET_ASSESSMENT_LIMIT_OFFSET"));
    }

    @Test
    public void whenGetAllWIthLimitThenPassCorrectArgs(){
        final String sql = "SomeSqlBlah";
        when(sqlProvider.apply(any())).thenReturn(Optional.of(sql));
        final int limit = 2;
        final int offset=3;
        assessmentDao.getAssessmentsForLimitAndOffset(limit, offset);
        verify(quarkusNamedJdbcTemplate, times(1)).query(sqlCaptor.capture(), mapArgumentCaptor.capture(), any(QuarkusRowMapper.class));
        assertEquals("Sql ran should be that gotten from provider", sql, sqlCaptor.getValue());
        assertEquals("Number of args in map should be two", 2, mapArgumentCaptor.getValue().size());
        assertEquals("Limit value passed to template should be value passed in", limit, mapArgumentCaptor.getValue().get("LIMIT"));
        assertEquals("Offset value passed to template should be value passed in", offset, mapArgumentCaptor.getValue().get("OFFSET"));
    }

    @Test
    public void whenGetWithLimitThrowsThenRethrow(){
        final String sql = "Some sql here";
        when(sqlProvider.apply(any())).thenReturn(Optional.of(sql));
        final QuarkusDataAccessException dataAccessException = mock(QuarkusDataAccessException.class);
        when(quarkusNamedJdbcTemplate.query(any(), any(), any(QuarkusRowMapper.class))).thenThrow(dataAccessException);
        ReadingSystemException thrown = CustomAssert.assertThrows(ReadingSystemException.class, () ->
                assessmentDao.getAssessmentsForLimitAndOffset(2,3));
        assertEquals("Root exception should be used as cause", dataAccessException, thrown.getCause());
        assertThat("Should contain string", thrown.getMessage(), Matchers.containsString("There was an error when querying for assessments for limit and offset.  The sql ran was Some sql here"));
        //no guaranteed ordering when iterating over a map, so must do as separate asserts, which works fine
        assertThat("Should contain string", thrown.getMessage(), Matchers.containsString("OFFSET: 3"));
        assertThat("Should contain string", thrown.getMessage(), Matchers.containsString("LIMIT: 2"));
    }

    @Test
    public void whenGetWithLimitReturnFromTemplate(){
        when(sqlProvider.apply(any())).thenReturn(Optional.of("notempty"));
        when(quarkusNamedJdbcTemplate.query(any(), any(), any(QuarkusRowMapper.class))).thenReturn(Collections.emptyList());
        List<Assessment> actual = assessmentDao.getAssessmentsForLimitAndOffset(2,3);
        assertThat("Returned value should match that gotten from template", actual, equalTo(Collections.emptyList()));

    }

    @Test
    public void whenGetTotalNumberOfAssessmentsProviderReturnsEmptyThenThrow(){
        when(sqlProvider.apply(any())).thenReturn(Optional.empty());
        ReadingSystemException thrown = CustomAssert.assertThrows(ReadingSystemException.class, () ->
                assessmentDao.getTotalNumberOfAssessments());
        assertEquals("Should ahve expected message", "No sql found for key GET_ASSESSMENT_COUNT", thrown.getMessage());
        assertNull("Shoudl have null cause", thrown.getCause());
    }

    @Test
    public void whenGetTotalCallsTemplate(){
        final String sql = "SELECT COUNT(1) FROM ASSESSMENT";
        when(sqlProvider.apply(any())).thenReturn(Optional.ofNullable(sql));
        assessmentDao.getTotalNumberOfAssessments();
        verify(jdbcTemplate, times(1)).queryForInt(sqlCaptor.capture());
        assertEquals("Sql executed should match that gotten from the provider",  sql, sqlCaptor.getValue());
    }

    @Test
    public void whenGetTotalJdbcExceptionThenRethrow(){
        final String sql = "Some sql here";
        when(sqlProvider.apply(any())).thenReturn(Optional.of(sql));
        final QuarkusDataAccessException dataAccessException = mock(QuarkusDataAccessException.class);
        when(jdbcTemplate.queryForInt(anyString())).thenThrow(dataAccessException);
        ReadingSystemException thrown = CustomAssert.assertThrows(ReadingSystemException.class, () ->
                assessmentDao.getTotalNumberOfAssessments());
        assertEquals("Should wrap causing exception", dataAccessException, thrown.getCause());
        assertThat("Should contain error message", thrown.getMessage(), Matchers.containsString("There was an error when getting assessment count.  The sql ran was Some sql here"));
    }

    @Test
    public void whenAddAssessmentCallProviderAndNamedTemplate(){
        final String sql = "yeah; some sql";
        when(sqlProvider.apply(any())).thenReturn(Optional.ofNullable(sql));
        final Assessment assessment = new Assessment()
                .isbn(1234L)
                .title("Title of some kind")
                .readingLevel(2.0F)
                .numberOfPoints(2.5F)
                .createdTeacherId(4)
                .authorFirstName("FirstName")
                .authorLastName("Last Name ....");
        assessmentDao.addAssessment(assessment);
        verify(sqlProvider, times(1)).apply(any());
        verify(quarkusNamedJdbcTemplate, times(1)).update(sqlCaptor.capture(), mapArgumentCaptor.capture());
        assertEquals("Sql ran should be that gotten from provider", sql, sqlCaptor.getValue());
        assertEquals("Map should contain entry for each column being inserted", 7, mapArgumentCaptor.getValue().size());
    }

    @Test
    public void whenAddAssessmentThrowsThenWrapAndRethrow(){
        QuarkusDataAccessException quarkusDataAccessException = new QuarkusDataAccessException("Oh no, some exception when adding to db", null);
        when(quarkusNamedJdbcTemplate.update(any(), any())).thenThrow(quarkusDataAccessException);
        when(sqlProvider.apply(any())).thenReturn(Optional.of("sql"));
        ReadingSystemException thrown = CustomAssert.assertThrows(ReadingSystemException.class, () ->
                assessmentDao.addAssessment(new Assessment()
                        .isbn(1234L)
                        .title("Title of some kind")
                        .readingLevel(2.0F)
                        .numberOfPoints(2.5F)
                        .createdTeacherId(4)
                        .authorFirstName("FirstName")
                        .authorLastName("Last Name ....")));
        assertEquals("Inner exception should be cause", quarkusDataAccessException, thrown.getCause());
   }

   @Test
   public void whenAddAssessmentAndDuplicateThenReturnFalse(){
        final Throwable cause =  mock(SQLIntegrityConstraintViolationException.class);
        final QuarkusDataAccessException quarkusDataAccessException = new QuarkusDataAccessException("", cause);
        when(quarkusNamedJdbcTemplate.update(any(), any())).thenThrow(quarkusDataAccessException);
        when(sqlProvider.apply(any())).thenReturn(Optional.of("dummySql"));
        boolean didAdd = assessmentDao.addAssessment(new Assessment().authorLastName("last").authorFirstName("first").title("title ").numberOfPoints(8.0F).readingLevel(5.6F).createdTeacherId(8).isbn(12L));
        assertFalse("Should be false in case of duplicate key exception", didAdd);
    }

    @Test
    public void whenGetAssessmentByIsbnSqlEmptyThenThrow(){
        when(sqlProvider.apply(any())).thenReturn(Optional.empty());
        ReadingSystemException thrown = CustomAssert.assertThrows(ReadingSystemException.class, () ->
                assessmentDao.getAssessmentByIsbn(0));
        assertNull("Should have null cause", thrown.getCause());
    }

    @Test
    public void whenGetAssessmentThenCallNamedTemplate(){
        final String sql = "blah";
        when(sqlProvider.apply(any())).thenReturn(Optional.of(sql));
        when(quarkusNamedJdbcTemplate.queryForObject(any(), any(), any(), anyBoolean())).thenReturn(null);
        assessmentDao.getAssessmentByIsbn(12L);
        verify(quarkusNamedJdbcTemplate, times(1)).queryForObject(sqlCaptor.capture(), mapArgumentCaptor.capture(), rowMapperCaptor.capture(), isHealthCheckCaptor.capture());
        assertFalse("Is health check should be value passed in", isHealthCheckCaptor.getValue());
    }

    @Test
    public void whenGetAssessmentByIsbnThenRethrow(){
        final String sql = "blah";
        when(sqlProvider.apply(any())).thenReturn(Optional.of(sql));
        QuarkusDataAccessException quarkusDataAccessException = new QuarkusDataAccessException("Test", null);
        when(quarkusNamedJdbcTemplate.queryForObject(any(), any(), any(), anyBoolean())).thenThrow(quarkusDataAccessException);
        ReadingSystemException thrown = CustomAssert.assertThrows(ReadingSystemException.class, () ->
                assessmentDao.getAssessmentByIsbn(12L));
        assertEquals("Should have inner exception as cause", quarkusDataAccessException, thrown.getCause());
    }

    @Test
    public void whenGetAssessmentByIsbnReturnValueFromTemplate(){
        final String sql = "blah";
        when(sqlProvider.apply(any())).thenReturn(Optional.of(sql));
        when(quarkusNamedJdbcTemplate.queryForObject(any(), any(), any(), anyBoolean())).thenReturn(null);
        Optional<Assessment> assessmentOptional = assessmentDao.getAssessmentByIsbn(12L);
        assertFalse("Optional should be empty", assessmentOptional.isPresent());
    }
}