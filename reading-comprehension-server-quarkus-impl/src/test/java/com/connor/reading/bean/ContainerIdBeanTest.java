package com.connor.reading.bean;

import org.jboss.logging.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ContainerIdBeanTest {
    @Mock
    private Logger logger;
    @InjectMocks
    private ContainerIdBean applicationIdBean;

    @Test
    public void whenGetIdIsNotNull(){
        assertNotNull("Id cannot be null", applicationIdBean.getContainerId());
    }
}