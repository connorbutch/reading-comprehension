package com.connor.reading.bean;

import org.jboss.logging.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class UniqueIdBeanTest {
    @Mock
    private Logger logger;
    @InjectMocks
    private UniqueRequestIdBean uniqueRequestIdBean;

    @Test
    public void whenGetRequestIdThenReturnNotNull(){
        assertNotNull("Request id should be auto populated", uniqueRequestIdBean.getRequestId());
    }

    @Test
    public void whenSetTraceIdThenGetReturnsIt(){
       //assertNull("Trace should be null until it is set", uniqueRequestIdBean.getTraceId());
       final UUID traceId = UUID.randomUUID();
       uniqueRequestIdBean.setTraceId(traceId);
       assertEquals("Get should return value set", traceId, uniqueRequestIdBean.getTraceId());
    }
}