package com.connor.reading.bean;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class HealthCheckBeanTest {
    @InjectMocks
    private HealthCheckBean healthCheckBean;

    @Test
    public void whenSetThenGetReturnsValue(){
        healthCheckBean.setHealthCheck(true);
        assertTrue("Should be true when set to true", healthCheckBean.isHealthCheck());
    }
}