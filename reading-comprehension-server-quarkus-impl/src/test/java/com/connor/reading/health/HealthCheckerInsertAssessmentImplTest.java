package com.connor.reading.health;

import com.connor.reading.dto.Assessment;
import com.connor.reading.health.impl.HealthCheckerInsertAssessmentImpl;
import com.connor.reading.service.AssessmentService;
import com.connor.reading.util.HealthCheckDataUtil;
import org.jboss.logging.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.transaction.TransactionManager;

@RunWith(MockitoJUnitRunner.class)
public class HealthCheckerInsertAssessmentImplTest {
    @Mock
    private Logger logger;
    @Mock
    private AssessmentService assessmentService;
    @Mock
    private TransactionManager transactionManager;
    @Mock
    private HealthCheckDataUtil healthCheckDataUtil;

    @InjectMocks
    private HealthCheckerInsertAssessmentImpl healthCheckerInsertAssessment;

    @Captor
    private ArgumentCaptor<Assessment> assessmentCaptor;

    @Test
    public void whenGetValueCallService(){
//        healthCheckerInsertAssessment.getValue();
//        verify(assessmentService, times(1)).addAssessment(assessmentCaptor.capture());
//        //could add other assertions on saved assessment here when in final format
    }

    @Test
    public void whenIsEqualToBuiltValueReturnIsValid(){
        //assertTrue(healthCheckerInsertAssessment.isValid(healthCheckerInsertAssessment.buildAssessmentForHealthCheck()));
    }
}