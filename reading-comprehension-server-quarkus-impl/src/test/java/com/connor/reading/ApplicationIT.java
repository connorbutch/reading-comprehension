package com.connor.reading;

import com.connor.quarkus.jdbc.QuarkusJdbcTemplate;
import com.connor.reading.controller.AssessmentsApiImpl;
import com.connor.reading.util.TeachersApiFactory;
import com.connor.teacher.client.TeachersApi;
import org.mockito.ArgumentCaptor;

import javax.inject.Inject;

/**
 * This file shows how to use quarkus test (similar to spring boot test) to be able to
 * boot the application using context, and replace various components in the cdi container (arc).
 * This allows us to have a deterministic test in which we can control the behavior of external
 * components (such as an external webservice).  We can also easily verify the invocation of external
 * components, as well as the data used to invoke them.
 *
 * However, please note that I would greatly prefer to actually boot the application
 * as this quarkus test tests the jvm version, not the graalvm.  Quarkus does contain
 * native test to test a native image, however, this does not allow the substitution of
 * cdi beans within the cdi container
 *
 * This shows two examples, how to replace a "regular" scoped bean using quarkus mock
 * and replacing dependent scoped beans using a replacement for a default bean.
 */
//@QuarkusTest
public class ApplicationIT { //this file works and can be ran, but it breaks acceptance tests.  To make it work, uncomment the junit 5 dependencies in the build.gradle
    private static final String STATUS_CODE = "STATUS_CODE";

    //can't use @Mock (either mockito or quarkus here)
    QuarkusJdbcTemplate mockedTemplate;

    //these must be static to avoid being cleared by the time test runs!
    //error message: Argument passed to verify() should be a mock but is null!
    static TeachersApiFactory mockedTeachersApiFactory;
    static TeachersApi mockedTeachersApi;

    @Inject
    AssessmentsApiImpl assessmentsApi;

    ArgumentCaptor<String> sqlCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Long> teacherIdCaptor = ArgumentCaptor.forClass(Long.class);

//    @BeforeEach
//    public void init() {
//        System.out.println("init");
//        mockedTemplate = mock(QuarkusJdbcTemplate.class);
//        when(mockedTemplate.queryForObject(any(), any(), anyBoolean())).thenReturn(new Assessment());
//        QuarkusMock.installMockForType(mockedTemplate, QuarkusJdbcTemplate.class);
//    }
//
//    @Test
//    public void whenGetAssessmentThenCallMockJdbcTemplate() {
//        System.out.println("whenGetAssessmentThenCallMockJdbcTemplate");
//        assessmentsApi.getAssessmentByIsbn(12L);
//        verify(mockedTemplate, only()).queryForObject(sqlCaptor.capture(), any(), anyBoolean());
//        assertEquals("SELECT ISBN,AUTHOR_FIRST_NAME,AUTHOR_LAST_NAME,TITLE,NUMBER_OF_POINTS,IS_VERIFIED,READING_LEVEL,CREATED_TEACHER_ID FROM ASSESSMENT WHERE ISBN=12", sqlCaptor.getValue(), "Sql ran should be captured");
//    }
//
//    @Test
//    public void whenAddAssessmentThenCallMockClient() {
//        System.setProperty(STATUS_CODE, "200");
//        System.out.println("whenAddAssessmentThenCallMockClient");
//        final Integer createdTeacherId = 13329;
//        assessmentsApi.createAssessment(new Assessment().isbn(12L).createdTeacherId(createdTeacherId).readingLevel(4.0F).numberOfPoints(6.2F).title("Title").authorFirstName("Fi").authorLastName("Last"));
//        verify(mockedTeachersApi, only()).getTeacherById(teacherIdCaptor.capture());
//        assertEquals(Long.valueOf(createdTeacherId), teacherIdCaptor.getValue(), "Should be value passed in");
//    }
//
//    @Test
//    public void whenTeacherServiceReturns400ThenReturn400() {
//        System.setProperty(STATUS_CODE, "400");
//        System.out.println("whenTeacherServiceReturns400ThenReturn400");
//        ReadingBadRequestException thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> assessmentsApi.createAssessment(new Assessment().isbn(12L).createdTeacherId(12).readingLevel(4.0F).numberOfPoints(6.2F).title("Title").authorFirstName("Fi").authorLastName("Last")));
//        assertTrue(thrown.getErrorReasons().contains(ErrorEnum.INVALID_CREATED_TEACHER), "Error reason should contain the expected error");
//    }
//
//    //In order to access injection point, since this must be dependent scope (not a regular scope like application scoped or request scoped)
//    //we cannot replace with quarkus mock, and must instead replace the default bean
//    @Produces
//    public TeachersApiFactory getClientFactory(InjectionPoint ip) {
//        mockedTeachersApi = mock(TeachersApi.class);
//        when(mockedTeachersApi.getTeacherById(any())).thenAnswer((Answer<Teacher>) invocation -> {
//            Teacher teacher = null;
//            String sysProperty = System.getProperty(STATUS_CODE);
//            if (sysProperty != null) {
//                switch (System.getProperty(STATUS_CODE)) {
//                    case "200":
//                        teacher = new Teacher();
//                        break;
//                    case "400":
//                        throw new ApiException(400, "Teacher id is invalid for some reason");
//                    case "404":
//                        throw new ApiException(404, "Teacher with given id does not exist");
//                    case "500":
//                        throw new ApiException(500, "Oh no, unexpected error in teacher service");
//                    default:
//                        //intentionally blank -- for now
//                        break;
//                }
//            }
//            return teacher;
//        });
//        mockedTeachersApiFactory = mock(TeachersApiFactory.class);
//        when(mockedTeachersApiFactory.get()).thenReturn(mockedTeachersApi);
//        System.out.println("Replaced teacher factory");
//        return mockedTeachersApiFactory;
//    }
}
