package com.connor.reading;

/**
 * This mirrors junit 5 (and junit 4.13) functionality, since we are unable to upgrade with quarkus enforced platform.
 * It is similar to runnable, except that it can throw a checked exception.
 */
@FunctionalInterface
public interface Executable {
    void execute() throws Throwable;
}
