package com.connor.reading.controller;

import com.connor.reading.bean.HealthCheckBean;
import com.connor.reading.dto.DownstreamDependency;
import com.connor.reading.dto.HealthResponse;
import com.connor.reading.dto.ReadinessHealthResponse;
import com.connor.reading.service.HealthService;
import org.jboss.logging.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class HealthApiImplTest {
    @Mock
    private Logger logger;
    @Mock
    private HealthService healthService;
    @Mock
    private HealthCheckBean healthCheckBean;
    @InjectMocks
    private HealthApiImpl healthApi;

    @Captor
    private ArgumentCaptor<Integer> timeoutCaptor;
    @Captor
    private ArgumentCaptor<Boolean> isHealthCheckCaptor;

    @Test
    public void pathAnnotationMustMatchThatDeclaredInInterface(){
        //because path annotation is required on implementation (not just interface), there is potential for these to get "out of wack"
        String pathValueFromImplementation = healthApi.getClass().getAnnotation(Path.class).value();
        String pathValueFromInterface = healthApi.getClass().getInterfaces()[0].getAnnotation(Path.class).value();
        assertEquals("Value of path annotation should match that declared in interface", pathValueFromInterface, pathValueFromImplementation);
    }

    @Test
    public void whenLivelinessCheckThenInvokeLogger(){
        healthApi.livelinessCheck();
        verify(logger, times(1)).info(any());
    }

    @Test
    public void whenLivelinessCheckThenReturnOk(){
        Response response = healthApi.livelinessCheck();
        assertEquals("Status code should be ok (200)", Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals("Response body should be health response", HealthResponse.class, response.getEntity().getClass());
        assertEquals("Response body should have a status of ok", "ok", ((HealthResponse)response.getEntity()).getHealthStatus());
    }

    @Test
    public void whenReadinessCheckThenInvokeLogger(){
        healthApi.readinessCheck(1500);
        verify(logger, times(1)).info(any());
    }

    @Test
    public void whenReadinessCheckInvokeServiceWithSameArg(){
        healthApi.readinessCheck(50);
        verify(healthService, times(1)).checkDownstreamDependencies(timeoutCaptor.capture());
        assertEquals("Timeout value passed to be service should be argument used to invoke api", Integer.valueOf(50), timeoutCaptor.getValue());

    }

    @Test
    public void whenReadinessCheckThenReturnFromService(){
        ReadinessHealthResponse returnValueFromService = new ReadinessHealthResponse()
                .availableDependencies(Arrays.asList(new DownstreamDependency()
                        .isAvailable(true)
                        .name("TestName")));
        when(healthService.checkDownstreamDependencies(any())).thenReturn(returnValueFromService);
        Response response = healthApi.readinessCheck(null);
        assertNotNull("Response shouldn't be null", response);
        assertEquals("Response body should be that returned from service", response.getEntity(), returnValueFromService);
        assertEquals("Status code should be ok (200)", Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @Test
    public void whenReadinessCheckThenInvokeHealthCheckBean(){
        healthApi.readinessCheck(12);
        verify(healthCheckBean).setHealthCheck(isHealthCheckCaptor.capture());
        assertTrue("Is health check should be set to true", isHealthCheckCaptor.getValue());
    }
}