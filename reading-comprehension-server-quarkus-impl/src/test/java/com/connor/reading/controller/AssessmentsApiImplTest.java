package com.connor.reading.controller;

import com.connor.reading.dto.Assessment;
import com.connor.reading.service.AssessmentService;
import org.hamcrest.Matchers;
import org.jboss.logging.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AssessmentsApiImplTest {

    @Mock
    private Logger logger;
    @Mock
    private AssessmentService assessmentService;
    @InjectMocks
    private AssessmentsApiImpl assessmentsApi;

    @Captor
    private ArgumentCaptor<Integer> limitCaptor;
    @Captor
    private ArgumentCaptor<Integer> offsetCaptor;
    @Captor
    private ArgumentCaptor<Assessment> assessmentCaptor;
    @Captor
    private ArgumentCaptor<Long> isbnCaptor;

    @Test
    public void pathAnnotationMustMatchThatDeclaredInInterface(){
        //because path annotation is required on implementation (not just interface), there is potential for these to get "out of wack"
        String pathValueFromImplementation = assessmentsApi.getClass().getAnnotation(Path.class).value();
        String pathValueFromInterface = assessmentsApi.getClass().getInterfaces()[0].getAnnotation(Path.class).value();
        assertEquals("Value of path annotation should match that declared in interface", pathValueFromInterface, pathValueFromImplementation);
    }

    @Test
    public void whenGetAssessmentsThenCallServiceWithArgsPassed(){
        final Integer expectedLimit = 20;
        final Integer expectedOffset = 3;
        assessmentsApi.getAssessments(expectedLimit, expectedOffset);
        verify(assessmentService, times(1)).getAssessments(limitCaptor.capture(), offsetCaptor.capture());
        assertEquals("Limit passed to service should be the same as query parameter", expectedLimit, limitCaptor.getValue());
        assertEquals("Offset passed to service should be the same as query parameter", expectedOffset, offsetCaptor.getValue());
        verify(assessmentService, times(1)).getTotalNumberOfAssessments();
    }

    @Test
    public void whenGetAssessmentsThenReturnBodyFromService(){
        final List<Assessment> assessmentListFromService = Collections.unmodifiableList(Arrays.asList(
              new Assessment().authorFirstName("TestFirstName")
              .authorLastName("SomeLastName")
              .isbn(1234123412L)
              .createdTeacherId(443322)
        ));
        when(assessmentService.getAssessments(any(), any())).thenReturn(assessmentListFromService);
        final Integer expectedTotalNumberOfAssessments = 2310;
        when(assessmentService.getTotalNumberOfAssessments()).thenReturn(expectedTotalNumberOfAssessments);
        Response response = assessmentsApi.getAssessments(null, null);
        assertEquals("Status code should be 200", Response.Status.OK.getStatusCode(), response.getStatus());
        assertThat("Body should be that returned from service", response.getEntity(), Matchers.is(assessmentListFromService));
        assertEquals("Response should contain total headers same as what was returned from service layer", expectedTotalNumberOfAssessments, Integer.valueOf(response.getHeaderString("x-total-assessments")));
    }

    @Test
    public void whenCreateAssessmentThenCallService(){
        when(assessmentService.addAssessment(any())).thenReturn(new Assessment().isbn(12L));
        final Assessment assessment = new Assessment().title("Something unique for this book");
        assessmentsApi.createAssessment(assessment);
        verify(assessmentService, times(1)).addAssessment(assessmentCaptor.capture());
        assertThat("Should invoke service with the arg passed to it", assessment, Matchers.is(assessmentCaptor.getValue()));
    }

    @Test
    public void whenCreateAssessmentReturnCreatedWithStatusCodeBodyAndHeader(){
        final Assessment resultFromService = new Assessment().isbn(12L);
        when(assessmentService.addAssessment(any())).thenReturn(resultFromService);
        Response result = assessmentsApi.createAssessment(new Assessment());
        assertEquals("Response should have created status code", result.getStatus(), Response.Status.CREATED.getStatusCode());
        assertThat("Response body should be that returned from service", resultFromService, Matchers.is(result.getEntity()));
        assertTrue("Should contain the created header", result.getHeaders().containsKey("location"));
        assertEquals("Location header should contain isbn", "/assessments/12", result.getHeaders().get("location").get(0).toString());
    }

    @Test
    public void whenGetAssessmentByIsbnCallService(){
        final Long isbn = 13L;
        assessmentsApi.getAssessmentByIsbn(isbn);
        verify(assessmentService, times(1)).getAssessmentByIsbn(isbnCaptor.capture());
        assertEquals("Value passed to service should be value used to invoke handler", isbn, isbnCaptor.getValue());
    }

    @Test
    public void whenGetAssessmentByIsbnThenReturnWhatServiceReturns(){
        final Assessment assessmentFromService = new Assessment().isbn(142342L).title("Very interesting book I'm sure");
        when(assessmentService.getAssessmentByIsbn(any())).thenReturn(assessmentFromService);
        Response result = assessmentsApi.getAssessmentByIsbn(634288823923L);
        assertEquals("Response should have status code of ok", Response.Status.OK.getStatusCode(), result.getStatus());
        assertEquals("Response body should be that returned from service layer", assessmentFromService, result.getEntity());
    }
}