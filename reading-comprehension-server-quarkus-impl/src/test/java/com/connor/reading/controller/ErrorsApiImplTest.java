package com.connor.reading.controller;

import com.connor.reading.dto.DetailedErrorInfo;
import com.connor.reading.service.ErrorService;
import org.jboss.logging.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ErrorsApiImplTest {
    @Mock
    private Logger logger;
    @Mock
    private ErrorService errorService;
    @InjectMocks
    private ErrorsApiImpl errorsApi;

    @Captor
    private ArgumentCaptor<Integer> errorIdCaptor;

    @Test
    public void whenReceiveRequestInvokeService(){
        final Integer errorIdPassedToController =133;
        errorsApi.getErrorById(errorIdPassedToController);
        verify(errorService, times(1)).getErrorReasonById(errorIdCaptor.capture());
        assertEquals("Value used to invoke api controller should get passed to service", errorIdPassedToController, errorIdCaptor.getValue());
    }

    @Test
    public void whenGetErrorByIdThenReturnOk(){
        DetailedErrorInfo detailedErrorInfoFromService = new DetailedErrorInfo().errorId(12);
        when(errorService.getErrorReasonById(any())).thenReturn(detailedErrorInfoFromService);
        Response response = errorsApi.getErrorById(12);
        assertEquals("Status code should be 200", Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals("Response body should be that from service", detailedErrorInfoFromService, response.getEntity());
    }
}