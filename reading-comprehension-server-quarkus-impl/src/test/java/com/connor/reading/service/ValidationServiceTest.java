package com.connor.reading.service;

import com.connor.reading.CustomAssert;
import com.connor.reading.bean.HealthCheckBean;
import com.connor.reading.dto.Assessment;
import com.connor.reading.exception.ReadingBadRequestException;
import com.connor.reading.model.ErrorEnum;
import com.connor.reading.util.TeachersApiFactory;
import com.connor.teacher.client.TeachersApi;
import org.jboss.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ValidationServiceTest {
    @Mock
    private Logger logger;
    @Mock
    private HealthCheckBean healthCheckBean;
    @Mock
    private TeachersApiFactory teachersApiFactory;
    @InjectMocks
    private ValidationService validationService;

    @Before
    public void init(){
        when(teachersApiFactory.get()).thenReturn(mock(TeachersApi.class));
    }

    @Test
    public void whenLimitInvalidThenThrow() {
        ReadingBadRequestException thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateGetAllAssessments(-1, 3));
        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.INVALID_LIMIT));
    }

    @Test
    public void whenOffsetInvalidThenThrow() {
        ReadingBadRequestException thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateGetAllAssessments(4, -1));
        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.INVALID_OFFSET));
    }

    @Test
    public void whenValidThenLimitAndOffsetThenSucceed() {
        validationService.validateGetAllAssessments(2,3);
        //get here without exception is success
    }

    @Test
    public void whenAddAssessmentNullThenThrow(){
        ReadingBadRequestException thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateAddAssessment(null));
        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.ASSESSMENT_REQUIRED));
    }

    @Test
    public void whenAddAssessmentInvalidFirstNameThenThrow(){
        ReadingBadRequestException thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateAddAssessment(new Assessment()));
        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.FIRST_NAME_REQUIRED));

        thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateAddAssessment(new Assessment().authorFirstName("    ")));
        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.FIRST_NAME_NOT_BLANK));

        thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateAddAssessment(new Assessment().authorFirstName("abcaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")));
        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.FIRST_NAME_EXCEEDS_LENGTH));
    }

    @Test
    public void whenAddAssessmentInvalidLastNameThenThrow(){
        ReadingBadRequestException thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateAddAssessment(new Assessment()));
        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.LAST_NAME_REQUIRED));

        thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateAddAssessment(new Assessment().authorLastName("    ")));
        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.LAST_NAME_NOT_BLANK));

        thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateAddAssessment(new Assessment().authorLastName("abcaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")));
        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.LAST_NAME_EXCEEDS_LENGTH));
    }

    @Test
    public void whenAddAssessmentInvalidTeacherIdThenThrow(){
        //TODO change this to handle when teacher service throws 404
//        ReadingBadRequestException thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateAddAssessment(new Assessment()));
//        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.CREATED_TEACHER_ID_REQUIRED));
//
//        thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateAddAssessment(new Assessment().createdTeacherId(-12)));
//        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.INVALID_CREATED_TEACHER));
    }

    //TODO add test for when teacher service throws 400

    @Test
    public void whenAddAssessmentInvalidNumberOfPointsThenThrow(){
        ReadingBadRequestException thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateAddAssessment(new Assessment()));
        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.NUMBER_OF_POINTS_REQUIRED));

        thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateAddAssessment(new Assessment().numberOfPoints(-10F)));
        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.INVALID_NUMBER_OF_POINTS));
    }

    @Test
    public void whenAddAssessmentInvalidReadingLevelThenThrow(){
        ReadingBadRequestException thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateAddAssessment(new Assessment()));
        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.READING_LEVEL_REQUIRED));

        thrown = CustomAssert.assertThrows(ReadingBadRequestException.class, () -> validationService.validateAddAssessment(new Assessment().readingLevel(-10F)));
        assertThat("Should contain an expected error reason", thrown.getErrorReasons(), hasItems(ErrorEnum.INVALID_READING_LEVEL));
    }

    @Test
    public void whenAddAssessmentValidThenSucceed(){
        Assessment validAssessment = new Assessment()
                .authorFirstName("First")
                .authorLastName("Last")
                .createdTeacherId(8)
                .isbn(12L)
                .numberOfPoints(10.0F)
                .readingLevel(8.0F)
                .title("Some title");
        validationService.validateAddAssessment(validAssessment);
        //getting here with no exception means it passes
    }
}