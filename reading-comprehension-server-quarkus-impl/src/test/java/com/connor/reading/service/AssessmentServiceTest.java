package com.connor.reading.service;

import com.connor.reading.CustomAssert;
import com.connor.reading.dao.AssessmentDao;
import com.connor.reading.dto.Assessment;
import com.connor.reading.exception.ReadingDuplicateResourceException;
import com.connor.reading.exception.ReadingNotFoundException;
import com.connor.reading.exception.ReadingSystemException;
import org.hamcrest.Matchers;
import org.jboss.logging.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AssessmentServiceTest {
    @Mock
    private Logger logger;
    @Mock
    private ValidationService validationService;
    @Mock
    private AssessmentDao assessmentDao;
    @InjectMocks
    private AssessmentService assessmentService;

    @Captor
    private ArgumentCaptor<Integer> limitCaptor;
    @Captor
    private ArgumentCaptor<Integer> offsetCaptor;
    @Captor
    private ArgumentCaptor<Assessment> assessmentCaptor;
    @Captor
    private ArgumentCaptor<Long> isbnCaptor;
    @Captor
    private ArgumentCaptor<Long> dbIsbnCaptor;


    @Test
    public void whenGetAssessmentsThenCallValidationService() {
        final Integer passedLimit = 14;
        final Integer passedOffset = 12;
        assessmentService.getAssessments(passedLimit, passedOffset);
        verify(validationService, times(1)).validateGetAllAssessments(limitCaptor.capture(), offsetCaptor.capture());
        assertEquals("Limit passed should equal that passed to validation service", passedLimit, limitCaptor.getValue());
        assertEquals("Offset passed should equal that passed to validation service", passedOffset, offsetCaptor.getValue());
    }

    @Test
    public void whenLimitNullThenInvokeDaoAndReturn() {
        final List<Assessment> assessmentListFromDao = Collections.emptyList();
        when(assessmentDao.getAllAssessments()).thenReturn(assessmentListFromDao);
        List<Assessment> actualAssessments = assessmentService.getAssessments(null, 13);
        verify(assessmentDao, times(1)).getAllAssessments();
        assertThat("Return value should be that of calling the get all assessments in dao", assessmentListFromDao, is(actualAssessments));
    }

    @Test
    public void whenLimitNotNullInvokeDaoAndReturnResult(){
        final List<Assessment> assessmentListFromDao = Collections.emptyList();
        when(assessmentDao.getAssessmentsForLimitAndOffset(anyInt(), anyInt())).thenReturn(assessmentListFromDao);
        final Integer passedLimit = 14;
        final Integer passedOffset = 12;
        List<Assessment> actualAssessments = assessmentService.getAssessments(passedLimit, passedOffset);
        verify(assessmentDao, times(1)).getAssessmentsForLimitAndOffset(limitCaptor.capture(), offsetCaptor.capture());
        assertThat("Return value should be that of calling the get all assessments in dao", assessmentListFromDao, is(actualAssessments));
        assertEquals("Limit passed to service should be that passed to dao", passedLimit, limitCaptor.getValue());
        assertEquals("Offset passed to service should be that passed to dao", passedOffset, offsetCaptor.getValue());
    }

    @Test
    public void whenGetTotalNumberOfAssessmentsInvokeDaoAndReturn(){
        final int expectedTotalNumberOfAssessments = 12;
        when(assessmentDao.getTotalNumberOfAssessments()).thenReturn(expectedTotalNumberOfAssessments);
        int actualNumberOfAssesssmentsReturned = assessmentService.getTotalNumberOfAssessments();
        verify(assessmentDao, times(1)).getTotalNumberOfAssessments();
        assertEquals("Number of assessments returned from service should match that returned from dao", expectedTotalNumberOfAssessments, actualNumberOfAssesssmentsReturned);
    }

    @Test
    public void whenOffsetNullPassAsZero(){
        final Integer limit = 0;
        final Integer offset = null;
        assessmentService.getAssessments(limit, offset);
        verify(assessmentDao, times(1)).getAssessmentsForLimitAndOffset(limitCaptor.capture(), offsetCaptor.capture());
        assertEquals("Offset should be passed as zero when null", (Integer)0, offsetCaptor.getValue());
    }

    @Test
    public void whenAddAssessmentThenCallValidation(){
        when(assessmentDao.getAssessmentByIsbn(anyLong())).thenReturn(Optional.of(new Assessment()));
        final Long isbn = 12L;
        when(assessmentDao.addAssessment(any())).thenReturn(true);
        assessmentService.addAssessment(new Assessment().isbn(isbn));
        verify(validationService, times(1)).validateAddAssessment(assessmentCaptor.capture());
        verify(assessmentDao, times(1)).getAssessmentByIsbn(isbnCaptor.capture());
        assertEquals("Isbn searched should be the isbn provided in the input", isbn, isbnCaptor.getValue());
    }

    @Test
    public void whenDbReturnsFalseForAddThenThrowDuplicateException(){
        ReadingDuplicateResourceException thrown = CustomAssert.assertThrows(ReadingDuplicateResourceException.class, () ->
                assessmentService.addAssessment(new Assessment().isbn(824112332L)));
        assertNull("Cause should be null", thrown.getCause());
    }

    @Test
    public void whenAfterAddGetThrowsExceptionThenWrapAsSystemException(){
        when(assessmentDao.getAssessmentByIsbn(anyLong())).thenReturn(Optional.empty());
        when(assessmentDao.addAssessment(any())).thenReturn(true);
        ReadingSystemException thrown = CustomAssert.assertThrows(ReadingSystemException.class, () ->
                assessmentService.addAssessment(new Assessment().isbn(824112332L)));
        assertThat("Inner cause should be wrapped", thrown.getCause(), Matchers.instanceOf(ReadingNotFoundException.class));
    }

    @Test
    public void whenAddAssessmentThenReturnFromDao(){
        Optional<Assessment> expectedAssessment = Optional.of(new Assessment().title("Blah").isbn(444442L));
        when(assessmentDao.getAssessmentByIsbn(anyLong())).thenReturn(expectedAssessment);
        when(assessmentDao.addAssessment(any())).thenReturn(true);
        Assessment result = assessmentService.addAssessment(new Assessment().isbn(51232L));
        assertThat("Returned value should come from dao get by isbn", result, is(expectedAssessment.get()));
    }

    @Test
    public void whenGetByIsbnInvokeValidationServiceAndDao(){
        when(assessmentDao.getAssessmentByIsbn(anyLong())).thenReturn(Optional.of(new Assessment()));
        final Long isbn = 1299999L;
        assessmentService.getAssessmentByIsbn(isbn);
        verify(validationService, times(1)).validateGetAssessmentByIsbn(isbnCaptor.capture());
        verify(assessmentDao, times(1)).getAssessmentByIsbn(dbIsbnCaptor.capture());
        assertEquals("Value passed should to validation service be that used to invoke function", isbn, isbnCaptor.getValue());
        assertEquals("Value passed should to db be that used to invoke function", isbn, dbIsbnCaptor.getValue());
    }

    @Test
    public void whenDbReturnEmptyOptionalThenThrowNotFoundException(){
        when(assessmentDao.getAssessmentByIsbn(anyLong())).thenReturn(Optional.empty());
        ReadingNotFoundException thrown = CustomAssert.assertThrows(ReadingNotFoundException.class, () ->
                assessmentService.getAssessmentByIsbn(142342422L));
        assertNull("Cause should be null, as there is no inner exception", thrown.getCause());
    }

    @Test
    public void whenDbReturnNonEmptyThenReturnValue(){
        final Assessment assessmentFromDao = new Assessment().title("Blah");
        when(assessmentDao.getAssessmentByIsbn(anyLong())).thenReturn(Optional.of(assessmentFromDao));
        Assessment result = assessmentService.getAssessmentByIsbn(12L);
        assertEquals("Value returned should be equal to value from dao", assessmentFromDao, result);
    }
}