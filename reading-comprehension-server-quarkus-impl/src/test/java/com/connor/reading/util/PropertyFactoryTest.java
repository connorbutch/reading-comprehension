package com.connor.reading.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PropertyFactoryTest {

    @InjectMocks
    private PropertyFactory propertyFactory;

    @Test
    public void whenGetPropertiesThenReturnNotNullProperties(){
        assertNotNull("Properties returned should never be null", propertyFactory.getNewPropertiesInstance());
    }

}