package com.connor.reading.util;

import org.jboss.logging.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ExceptionMapperImplTest {
    @Mock
    private Logger logger;
    @InjectMocks
    private ExceptionMapperImpl exceptionMapperImpl;

    @Captor
    private ArgumentCaptor<Throwable> throwableArgumentCaptor;

    @Test
    public void whenExceptionThenLogToError(){
//        final Throwable throwable = new RuntimeException("Blah");
//        exceptionMapperImpl.toResponse(throwable);
//        verify(logger, times(1)).error(any(), throwableArgumentCaptor.capture());
//        assertEquals("Throwable passed should be logged to error level", throwable, throwableArgumentCaptor.getValue());
    }
//
//    @Test
//    public void whenServiceUnavailableExceptionThenReturn502(){
//        final List<DownstreamDependency> downstreamDependencies = Collections.emptyList();
//        Throwable throwable = new ReadingServiceUnavailableException(downstreamDependencies);
//        Response response = exceptionMapperImpl.toResponse(throwable);
//        ReadinessHealthResponse expectedResponseBody = new ReadinessHealthResponse()
//                .isAvailable(false)
//                .availableDependencies(Collections.emptyList())
//                .unavailableDependencies(Collections.emptyList());
//        assertNotNull("Response should not be null", response);
//        assertEquals("Status code should be that of service unavailable", Response.Status.SERVICE_UNAVAILABLE.getStatusCode(), response.getStatus());
//        assertEquals("Response body should be the status from the exception", expectedResponseBody, response.getEntity());
//    }
//
//    @Test
//    public void whenIllegalArgumentExceptionThenReturn400(){
//        final String errorMessage = "Oh no, you sent me bad data.   This would never happen in real life.....";
//        Throwable t = new IllegalArgumentException(errorMessage);
//        Response response = exceptionMapperImpl.toResponse(t);
//        assertEquals("Status code should be that of bad request", Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
//        assertEquals("Response body should have status code of 2", BigDecimal.valueOf(2), ((ErrorResponse)response.getEntity()).getCode());
//    }
//
//    @Test
//    public void whenUnexpectedExceptionThenReturn500(){
//        final Throwable throwable = new RuntimeException("Blah");
//        Response response = exceptionMapperImpl.toResponse(throwable);
//        assertNotNull("Response should not be null", response);
//        assertEquals("Status code should be that of internal server error", Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
//        assertEquals("Response body should have status code of 1", BigDecimal.valueOf(1), ((ErrorResponse)response.getEntity()).getCode());
//    }
//
//    @Test
//    public void whenReadingNotFoundThenReturn404(){
//        final Throwable throwable = new ReadingNotFoundException("");
//        Response result = exceptionMapperImpl.toResponse(throwable);
//        assertEquals("Status code should be not found", Response.Status.NOT_FOUND.getStatusCode(), result.getStatus());
//    }
}