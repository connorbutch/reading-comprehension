package com.connor.reading.util;

import com.connor.reading.exception.ReadingSystemException;
import org.jboss.logging.Logger;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SqlProviderPropertiesFileImplTest {
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Mock
    private Logger logger;
    @Mock
    private InputStreamFactory inputStreamFactory;
    @Mock
    private Map<String, String> sqlCache;
    @Mock
    private PropertyFactory propertyFactory;
    @InjectMocks
    private SqlProviderPropertiesFileImpl sqlProviderPropertiesFile;

    @Captor
    private ArgumentCaptor<String> stringCaptor;

    @Test
    public void whenInputStreamNullThenThrow(){
        when(sqlCache.isEmpty()).thenReturn(true);
        when(inputStreamFactory.getInputStreamForPath(any())).thenReturn(Optional.empty());
        exceptionRule.expect(ReadingSystemException.class);
        exceptionRule.expectMessage("No properties file present at sql.properties");
        sqlProviderPropertiesFile.apply("someKey");
        verify(inputStreamFactory, times(1)).getInputStreamForPath(stringCaptor.capture());
        assertEquals("FIle path should be sql.properties", "sql.properties", stringCaptor.getValue());
    }

    @Test
    public void whenWhenExceptionLoadingPropertiesThenThrow() throws IOException {
        when(sqlCache.isEmpty()).thenReturn(true);
        InputStream mockedInputStream = mock(InputStream.class);
        when(inputStreamFactory.getInputStreamForPath(any())).thenReturn(Optional.ofNullable(mockedInputStream));
        Properties mockedProperties = mock(Properties.class);
        final IOException thrownIoException = new IOException("Oh no, IOException");
        doThrow(thrownIoException).when(mockedProperties).load(any(InputStream.class));
        when(propertyFactory.getNewPropertiesInstance()).thenReturn(mockedProperties);
        exceptionRule.expect(ReadingSystemException.class);
        exceptionRule.expectMessage("There was an error when reading in input stream for filepath sql.properties");
        exceptionRule.expectCause(equalTo(thrownIoException));
        sqlProviderPropertiesFile.apply("key");
    }

    @Test
    public void whenPropertyNotPresentThenReturnEmptyOptional(){
        final String key = "key";
        Optional<String> result = sqlProviderPropertiesFile.apply(key);
        verify(sqlCache, times(1)).get(stringCaptor.capture());
        assertEquals("Gotten from cache should be the value passed in", key, stringCaptor.getValue());
        assertFalse("Optional should be empty when not present in map", result.isPresent());
    }

    @Test
    public void whenPropertyIsPresentThenReturnPopulatedOptional(){
        final String returnedSql = "select ...";
        when(sqlCache.get(any())).thenReturn(returnedSql);
        final String key = "key";
        Optional<String> result = sqlProviderPropertiesFile.apply(key);
        assertTrue("Optional should be present when exists in cache", result.isPresent());
        assertEquals("Returned value in optional should be that from cache", returnedSql, result.get());
    }

}