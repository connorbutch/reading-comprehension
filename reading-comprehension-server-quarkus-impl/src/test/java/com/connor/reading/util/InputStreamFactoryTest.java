package com.connor.reading.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class InputStreamFactoryTest {
    @InjectMocks
    private InputStreamFactory inputStreamFactory;

    @Test
    public void whenGetInputStreamThenReturn() {
        inputStreamFactory.getInputStreamForPath("");
    }
}