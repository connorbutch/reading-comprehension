package com.connor.reading;

import com.connor.quarkus.jdbc.QuarkusJdbcTemplate;
import com.connor.quarkus.jdbc.QuarkusRowMapper;

//@Mock //with this uncommented, it works, but verifying is hard
public class QuarkusJdbcTemplateExtension extends QuarkusJdbcTemplate {
    public QuarkusJdbcTemplateExtension() {
        super(null, null);
    }

    @Override
    public <T> T queryForObject(String sql, QuarkusRowMapper<T> rowMapper, boolean readUncommitted) {
        return (T) null;
    }
}
