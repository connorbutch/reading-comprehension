package com.connor.reading.util;

import com.connor.reading.bean.HealthCheckBean;
import com.connor.reading.bean.UniqueRequestIdBean;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.model.HeaderConstants;
import com.connor.teacher.ApiClient;
import com.connor.teacher.client.TeachersApi;
import org.jboss.logging.Logger;

import java.net.MalformedURLException;
import java.net.http.HttpRequest;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * The issue with trying to instantiate the client is that it has (dependent) scope by default
 * so when the instance has created, the is health check value is not set on the request scoped bean
 * Hence, we cannot determine if it is a health check of not.  This way, by lazily instantiating the bean
 * when needed, we can correctly read the is health check off the request scoped bean.
 */
//intentionally don't annotate this for bean discovery.  Instead, get from producer so we can read annotation for base url
@Debug
public class TeachersApiFactory implements Supplier<TeachersApi> {
    private static final Set<String> ALLOWED_SCHEMES = Collections.unmodifiableSet(Set.of("http", "https"));
    private static final Set<Integer> ALLOWED_PORTS = Collections.unmodifiableSet(Set.of(8080, 443));

    private final Logger logger;
    private final HealthCheckBean healthCheckBean;
    private final UniqueRequestIdBean uniqueRequestIdBean;
    private final String host;
    private final String scheme;
    private final int port;

    public TeachersApiFactory(Logger logger, HealthCheckBean healthCheckBean, UniqueRequestIdBean uniqueRequestIdBean, String scheme, String host, int port) {
        validateInput(scheme, host, port);
        this.logger = logger;
        this.healthCheckBean = healthCheckBean;
        this.uniqueRequestIdBean = uniqueRequestIdBean;
        this.scheme = scheme;
        this.host = host;
        this.port = port;
        this.logger.info("scheme " + this.scheme + " host " + this.host + " port " + this.port);
    }

    @Override
    public TeachersApi get() {
        boolean isHealthCheck = healthCheckBean.isHealthCheck();
        ApiClient apiClient = buildApiClient(isHealthCheck);
        return new TeachersApi(apiClient);
    }

    protected void validateInput(String scheme, String host, int port) {
        StringBuilder errorStringBuilder = new StringBuilder();
        if (!ALLOWED_PORTS.contains(port)) {
            errorStringBuilder.append("Port is not valid.  Valid ports include ")
                    .append(ALLOWED_PORTS.stream().map(Objects::toString).collect(Collectors.joining(",")));
        }
        if (!ALLOWED_SCHEMES.contains(scheme)) {
            errorStringBuilder.append("Scheme is not valid.  Valid schemes include ")
                .append(String.join(",", ALLOWED_SCHEMES));
        }
        if (host == null || host.isBlank()) {
            errorStringBuilder.append("Host cannot be null/blank");
        }
        if (errorStringBuilder.length() > 0) {
            throw new IllegalArgumentException(errorStringBuilder.toString());
        }
    }

    protected ApiClient buildApiClient(boolean isHealthCheck) {
        ApiClient apiClient = new ApiClient()
                .setScheme(scheme)
                .setPort(port);
        Consumer<HttpRequest.Builder> interceptor = buildRequestInterceptor(isHealthCheck);
        apiClient.setRequestInterceptor(interceptor);
        setBaseUrlIfNeeded(apiClient);
        return apiClient;
    }

    protected Consumer<HttpRequest.Builder> buildRequestInterceptor(boolean isHealthCheck) {
        return request -> {
            logUrl(request);
            if (isHealthCheck) {
                logger.info("This is a health check, so setting the " + HeaderConstants.HEALTH_CHECK_REQUEST_HEADER + " on outgoing request to teachers api");
                request.setHeader(HeaderConstants.HEALTH_CHECK_REQUEST_HEADER, Boolean.TRUE.toString());
            } else {
                logger.info("This is not a health check, so not setting health check header on outgoing requests");
            }
            //NOTE: must generate a new request id for outgoing requests, don't reuse the one coming in to this application
            request.setHeader(HeaderConstants.REQUEST_ID_HEADER_KEY, UUID.randomUUID().toString());
            logger.debug("Unique trace id in factory: " + uniqueRequestIdBean.getTraceId());
            request.setHeader(HeaderConstants.TRACE_ID_HEADER_KEY, uniqueRequestIdBean.getTraceId().toString());
        };
    }

    protected void setBaseUrlIfNeeded(ApiClient apiClient) {
        if (host != null) {
            logger.info("Creating teacher api client with host " + host);
            apiClient.setHost(host);
        } else {
            logger.warn("Host is not specified for teacher api client");
        }
    }

    protected void logUrl(HttpRequest.Builder request) {
        try {
            logger.info("Making request to teacher api with url " + request.build().uri().toURL().toString());
        } catch (MalformedURLException e) {
            logger.debug("Error creating url to log for request to teacher api", e);
        }
    }
}
