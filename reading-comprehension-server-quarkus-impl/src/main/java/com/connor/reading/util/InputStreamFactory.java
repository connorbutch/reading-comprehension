package com.connor.reading.util;

import com.connor.reading.interceptor.Debug;

import javax.enterprise.context.Dependent;
import java.io.InputStream;
import java.util.Optional;

@Dependent
@Debug
public class InputStreamFactory {
    public Optional<InputStream> getInputStreamForPath(String filePathToBeReadFromClassLoader){
        return Optional.ofNullable(getClass().getClassLoader().getResourceAsStream(filePathToBeReadFromClassLoader));
    }
}