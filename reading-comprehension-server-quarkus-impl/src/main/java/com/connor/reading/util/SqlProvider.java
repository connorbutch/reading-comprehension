package com.connor.reading.util;

import java.util.Optional;
import java.util.function.Function;

@FunctionalInterface
public interface SqlProvider extends Function<String, Optional<String>> {
    //intentionally blank -- all needed functionality is inherited from parent interface (Function)
}
