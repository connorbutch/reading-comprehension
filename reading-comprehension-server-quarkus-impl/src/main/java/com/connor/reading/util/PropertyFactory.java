package com.connor.reading.util;

import javax.enterprise.context.Dependent;
import java.util.Properties;

@Dependent
public class PropertyFactory {
    public Properties getNewPropertiesInstance(){
        return new Properties();
    }
}
