package com.connor.reading.util;

import com.connor.reading.cdi.Cache;
import com.connor.reading.exception.ReadingSystemException;
import com.connor.reading.interceptor.Debug;
import org.jboss.logging.Logger;

import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

@Debug
@Singleton //only want one instance of the cache to avoid redundant loading
public class SqlProviderPropertiesFileImpl implements SqlProvider {
    private static final String SQL_PROPERTIES_FILE_NAME = "sql.properties";
    private static final Object LOCK = new Object();

    private final Logger logger;
    private final InputStreamFactory inputStreamFactory;
    private final Map<String, String> sqlCache;
    private final PropertyFactory propertyFactory;

    SqlProviderPropertiesFileImpl(Logger logger, InputStreamFactory inputStreamFactory, @Cache Map<String, String> sqlCache, PropertyFactory propertyFactory) {
        this.logger = logger;
        this.inputStreamFactory = inputStreamFactory;
        this.sqlCache = sqlCache;
        this.propertyFactory = propertyFactory;
    }

    @Override
    public Optional<String> apply(String sqlKey) {
        if (sqlCache.isEmpty()) {
            synchronized (LOCK) { //not sure if singleton is thread safe, so do our own synchronization
                if (sqlCache.isEmpty()) {
                    logger.info("Cache is empty, attempting to reload it.");
                    loadProperties();
                }
            }
        }
        return Optional.ofNullable(sqlCache.get(sqlKey));
    }

    protected void loadProperties() {
        try (InputStream inputStream = inputStreamFactory.getInputStreamForPath(SQL_PROPERTIES_FILE_NAME)
                .orElseThrow(() -> new ReadingSystemException(String.format("No properties file present at %s", SQL_PROPERTIES_FILE_NAME)))) {
                Properties properties = propertyFactory.getNewPropertiesInstance();
                properties.load(inputStream);
                properties.forEach((key, value) -> sqlCache.put((String)key, (String)value));
        } catch (IOException e) {
            String errorMessage = String.format("There was an error when reading in input stream for filepath %s", SQL_PROPERTIES_FILE_NAME);
            throw new ReadingSystemException(errorMessage, e);
        }
    }
}
