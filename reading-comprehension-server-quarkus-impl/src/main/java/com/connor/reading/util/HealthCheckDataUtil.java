package com.connor.reading.util;

import com.connor.reading.dto.Assessment;
import com.connor.reading.dto.Question;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.service.ValidationService;

import javax.enterprise.context.Dependent;
import java.util.Arrays;

@Dependent
@Debug
public class HealthCheckDataUtil {
    public Assessment buildAssessmentForHealthCheck(){
        return new Assessment()
                .isbn(ValidationService.DISALLOWED_ISBN)
                .title("Title")
                .authorFirstName("First")
                .authorLastName("Last")
                .numberOfPoints(8.0F)
                .readingLevel(8.0F)
                .createdTeacherId(2);
    }

    public Question buildQuestionForHealthCheck(){
        return new Question()
                .correctAnswer("Dummy")
                .questionText("Text")
                .incorrectAnswers(Arrays.asList("Incorrect one", "incorrect two", "incorrect three"));
    }
}
