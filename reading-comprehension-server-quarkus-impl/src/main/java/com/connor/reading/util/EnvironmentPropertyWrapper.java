package com.connor.reading.util;

import com.connor.reading.interceptor.Debug;

import javax.enterprise.context.Dependent;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

@Dependent
@Debug
public class EnvironmentPropertyWrapper {
    public Optional<String> getEnvironmentProperty(String key) {
        return Optional.ofNullable(System.getenv(key));
    }

    public String getEnvironmentProperty(String key, String defaultValue) {
        return getEnvironmentProperty(key)
                .orElse(defaultValue);
    }

    public <T> Optional<T> getEnvironmentProperty(String key, Function<String, T> transformer) {
        return getEnvironmentProperty(key)
                .map(transformer);
    }

    public <T> Optional<T> getEnvironmentProperty(String key, Predicate<String> evaluator, Function<String, T> transformer) {
        return getEnvironmentProperty(key)
                .filter(evaluator)
                .map(transformer);
    }

    public <T> T getEnvironmentProperty(String key, Predicate<String> evaluator, Function<String, T> transformer, T defaultValue) {
        return getEnvironmentProperty(key, evaluator, transformer)
                .orElse(defaultValue);
    }
}
