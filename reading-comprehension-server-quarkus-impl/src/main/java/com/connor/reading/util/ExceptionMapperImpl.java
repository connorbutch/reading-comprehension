package com.connor.reading.util;

import com.connor.reading.dto.DownstreamDependency;
import com.connor.reading.dto.ErrorResponse;
import com.connor.reading.dto.ReadinessHealthResponse;
import com.connor.reading.exception.ReadingBadRequestException;
import com.connor.reading.exception.ReadingDuplicateResourceException;
import com.connor.reading.exception.ReadingNotFoundException;
import com.connor.reading.exception.ReadingServiceUnavailableException;
import com.connor.reading.model.ErrorEnum;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Provider
public class ExceptionMapperImpl implements ExceptionMapper<Throwable> {
    @Inject
    Logger logger;

    @Override
    public Response toResponse(Throwable throwable) {
        logger.error("Handling error (NOTE: this might not be a server error, it might be when the client submits bad data)", throwable);
        Response errorResponse = null;
        if(throwable instanceof ReadingBadRequestException){
            errorResponse = buildBadRequestErroResponse((ReadingBadRequestException)throwable);
        }else if(throwable instanceof ReadingNotFoundException){
            errorResponse = buildCustomNotFoundErrorResponse((ReadingNotFoundException)throwable);
        }else if(throwable instanceof NotFoundException){
            errorResponse = buildNotFoundErrorResponse((NotFoundException)throwable);
        }else if(throwable instanceof ReadingDuplicateResourceException){
            errorResponse = buildDuplicateKeyException();
        } else if(throwable instanceof ReadingServiceUnavailableException){
            errorResponse = buildServiceUnavailableResponse((ReadingServiceUnavailableException)throwable);
        }else{
            errorResponse = buildUnhandledResponse();
        }
        return errorResponse;
    }

    protected Response buildBadRequestErroResponse(ReadingBadRequestException e){
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ErrorResponse().errorReasons(
                        e.getErrorReasons()
                                .stream()
                                .map(ErrorEnum::convertToErrorReason)
                                .collect(Collectors.toList()))
                        .timestamp(OffsetDateTime.now()))
                .build();
    }

    protected Response buildNotFoundErrorResponse(NotFoundException e) {
        return Response.status(Response.Status.NOT_FOUND)
                .entity(new ErrorResponse().timestamp(OffsetDateTime.now()).errorReasons(
                        Collections.singletonList(ErrorEnum.ENDPOINT_NOT_REGISTERED.convertToErrorReason())
                ))
                .build();
    }

    protected Response buildCustomNotFoundErrorResponse(ReadingNotFoundException e) {
        return Response.status(Response.Status.NOT_FOUND)
                .entity(new ErrorResponse()
                        .timestamp(OffsetDateTime.now())
                        .errorReasons(Collections.singletonList(
                                ErrorEnum.ASSESSMENT_NOT_FOUND.convertToErrorReason()
                        )))
                    .build();
    }

    protected Response buildDuplicateKeyException() {
        return Response.status(Response.Status.CONFLICT).entity(new ErrorResponse().timestamp(OffsetDateTime.now())
        .errorReasons(Collections.singletonList(ErrorEnum.ASSESSMENT_ALREADY_EXISTS.convertToErrorReason()))).build();
    }

    protected Response buildServiceUnavailableResponse(ReadingServiceUnavailableException serviceUnavailableException) {
        List<DownstreamDependency> unavailableDependencies = new ArrayList<>();
        List<DownstreamDependency> availableDependencies = new ArrayList<>();
        serviceUnavailableException.getDownstreamDependencies().forEach(downstreamDependency -> {
            if (downstreamDependency.getIsAvailable()) {
                availableDependencies.add(downstreamDependency);
            } else {
                unavailableDependencies.add(downstreamDependency);
            }
        });
        unavailableDependencies.sort(Comparator.comparing(DownstreamDependency::getName));
        availableDependencies.sort(Comparator.comparing(DownstreamDependency::getName));
        return Response.status(Response.Status.SERVICE_UNAVAILABLE)
                .entity(new ReadinessHealthResponse()
                        .isAvailable(false)
                        .availableDependencies(availableDependencies)
                        .unavailableDependencies(unavailableDependencies))
                .build();
    }

    protected Response buildUnhandledResponse() {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(new ErrorResponse()
                        .timestamp(OffsetDateTime.now())
                        .errorReasons(Collections.singletonList(
                            ErrorEnum.UNEXPECTED.convertToErrorReason()
                        )))
                        .build();
    }
}
