package com.connor.reading.filter.request;

import com.connor.reading.filter.CustomFilterPriorities;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.model.HeaderConstants;
import org.jboss.logging.Logger;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Optional;

/**
 * This filter exists ONLY to show that knative can (and does) spin up new containers in response to
 * concurrent requests.  However, with how fast quarkus is, it is difficult to demonstrate this locally
 * without load testing scripts.  Hence, we add the functionality to allow the user to simulate a long-running
 * request by passing in a header that forces a sleep.
 */
@Debug
@Provider
@Priority(CustomFilterPriorities.THIRD_USER_FILTER) //should execute after health check and logging
public class ContainerRequestFilterSleepImpl implements ContainerRequestFilter {
    private static final long MAX_ALLOWED_SLEEP_TIME_MS = 10_000;
    private static final long DEFAULT_SLEEP_TIME_MS = 5_000;

    @Inject
    Logger logger;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if(shouldSleep(requestContext)){
            logger.warn("Received request that should sleep before handling request (this should only be used to show knative functionality)");
            long sleepTimeInMs = getSleepTimeInMs(requestContext);
            logger.debug("Sleep time for request is " + sleepTimeInMs);
            try {
                Thread.sleep(sleepTimeInMs);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                logger.debug("The thread was interrupted when request to sleep was received", e);
            }
        }else{
            logger.debug("Received \"regular\" request, so application will NOT sleep before handling the request");
        }
    }

    protected boolean shouldSleep(ContainerRequestContext requestContext) {
        return requestContext.getHeaders()
                .containsKey(HeaderConstants.REQUEST_SLEEP_HEADER_KEY);
    }

    protected long getSleepTimeInMs(ContainerRequestContext requestContext) {
        return Optional.ofNullable(requestContext.getHeaderString(HeaderConstants.REQUEST_SLEEP_HEADER_KEY))
                .flatMap(this::tryToGetLongFromString)
                .filter(sleepHeaderAmountInMs -> sleepHeaderAmountInMs <= MAX_ALLOWED_SLEEP_TIME_MS)
                .orElse(DEFAULT_SLEEP_TIME_MS);
    }

    protected Optional<Long> tryToGetLongFromString(String value){
        Long longValue = null;
        try{
            longValue = Long.parseLong(value);
        }catch(NumberFormatException e){
            //intentionally silence
        }
        return Optional.ofNullable(longValue);
    }
}
