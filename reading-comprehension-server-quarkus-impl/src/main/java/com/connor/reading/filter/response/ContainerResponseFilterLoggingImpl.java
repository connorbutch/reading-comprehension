package com.connor.reading.filter.response;

import com.connor.reading.filter.CustomFilterPriorities;
import org.jboss.logging.Logger;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.stream.Collectors;

@Provider
@Priority(CustomFilterPriorities.SECOND_USER_FILTER)
public class ContainerResponseFilterLoggingImpl implements ContainerResponseFilter {
    private static final String NULL_STR = "NULL";

    @Inject
    Logger logger;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        log(new StringBuilder("Request received ")
                .append(getRequestMethodString(requestContext))
                .append(getRequestHeaderString(requestContext))
                .append(getRequestCookieString(requestContext))
                .append(getRequestQueryParamString(requestContext))
                .append(getRequestUrlString(requestContext))
                .append(" with response")
                .append(getResponseBodyString(responseContext))
                .append(getResponseHeaderString(responseContext))
                .append(getResponseStatusCodeString(responseContext)).toString());
    }

    protected String getRequestHeaderString(ContainerRequestContext requestContext) {
        return "Request headers " + requestContext.getHeaders().entrySet().stream().map(entry ->
                new StringBuilder(entry.getKey()).append(": ").append(entry.getValue().stream().collect(Collectors.joining(", ")))
        ).collect(Collectors.joining("\n"));
    }

    protected String getRequestCookieString(ContainerRequestContext requestContext) {
        return "Request cookies " + requestContext.getCookies().entrySet().stream().map(entry ->
                new StringBuilder(entry.getKey()).append(": ").append(entry.getValue().getValue()).toString())
                .collect(Collectors.joining("\n"));
    }

    protected String getRequestUrlString(ContainerRequestContext requestContext) {
        return "Request url " + requestContext.getUriInfo().getBaseUri().toASCIIString() + requestContext.getUriInfo().getPath();
    }

    protected String getRequestQueryParamString(ContainerRequestContext requestContext) {
        return "Query parameters " + requestContext.getUriInfo().getQueryParameters().entrySet().stream().map(entry ->
            entry.getKey() + "=" + String.join(",", entry.getValue())
        ) .collect(Collectors.joining("\n"));
    }

    protected String getRequestMethodString(ContainerRequestContext requestContext){
        return "Request method " + requestContext.getMethod();
    }

    protected String getResponseStatusCodeString(ContainerResponseContext responseContext) {
        return "Response status code " + String.valueOf(responseContext.getStatus());
    }

    protected String getResponseHeaderString(ContainerResponseContext responseContext) {
        return "Response headers " +  responseContext.getHeaders().entrySet().stream().map(entry ->
                new StringBuilder(entry.getKey()).append(": ").append(entry.getValue().stream().map(Object::toString).collect(Collectors.joining(", ")))
        ).collect(Collectors.joining("\n"));

    }

    protected String getResponseBodyString(ContainerResponseContext responseContext) {
        return "Response body " + toStringNullSafe(responseContext.getEntity());
    }

    protected String toStringNullSafe(Object objectToString){
        return objectToString == null? NULL_STR: objectToString.toString();
    }

    protected void log(String logStr) {
        logger.info(logStr);
    }
}
