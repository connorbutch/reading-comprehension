package com.connor.reading.filter;

import javax.ws.rs.Priorities;

public abstract class CustomFilterPriorities {
    public static final int FIRST_USER_FILTER = Priorities.USER;
    public static final int SECOND_USER_FILTER = Priorities.USER + 1;
    public static final int THIRD_USER_FILTER = Priorities.USER + 2;
    public static final int FOURTH_USER_FILTER = Priorities.USER + 3;
}
