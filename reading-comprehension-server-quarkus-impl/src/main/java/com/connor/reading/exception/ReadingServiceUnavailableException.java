package com.connor.reading.exception;

import com.connor.reading.dto.DownstreamDependency;

import java.util.List;
import java.util.stream.Collectors;

public class ReadingServiceUnavailableException extends RuntimeException {
    private final List<DownstreamDependency> downstreamDependencies;

    public ReadingServiceUnavailableException(List<DownstreamDependency> downstreamDependencies){
        this.downstreamDependencies = downstreamDependencies;
    }

    @Override
    public String getMessage() {
        return "There was at least one service unavailable.  The following dependencies showed as unavailable: "
                + downstreamDependencies.stream()
                .filter(downstreamDependency -> !downstreamDependency.getIsAvailable())
                .map(DownstreamDependency::getName)
                .collect(Collectors.joining(", "));
    }

    public List<DownstreamDependency> getDownstreamDependencies() {
        return downstreamDependencies;
    }
}
