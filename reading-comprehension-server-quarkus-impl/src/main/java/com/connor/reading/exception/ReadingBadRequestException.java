package com.connor.reading.exception;

import com.connor.reading.dto.ErrorReason;
import com.connor.reading.model.ErrorEnum;

import java.util.List;
import java.util.stream.Collectors;

public class ReadingBadRequestException extends RuntimeException {
    private final List<ErrorEnum> errorReasons;

    public ReadingBadRequestException(List<ErrorEnum> errorReasons){
        this.errorReasons = errorReasons;
    }

    @Override
    public String getMessage() {
        return "There were errors with the request " + errorReasons.stream()
                .map(ErrorEnum::convertToErrorReason)
                .map(ErrorReason::toString)
                .collect(Collectors.joining(", "));
    }

    public List<ErrorEnum> getErrorReasons() {
        return errorReasons;
    }
}
