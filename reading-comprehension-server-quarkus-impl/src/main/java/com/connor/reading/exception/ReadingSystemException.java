package com.connor.reading.exception;

public class ReadingSystemException extends RuntimeException {
    public ReadingSystemException(String message){
        super(message);
    }
    public ReadingSystemException(String message, Throwable cause){
        super(message, cause);
    }
}
