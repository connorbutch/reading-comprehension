package com.connor.reading.exception;

public class ReadingNotFoundException extends RuntimeException {
    public ReadingNotFoundException(String message){
        super(message);
    }
}
