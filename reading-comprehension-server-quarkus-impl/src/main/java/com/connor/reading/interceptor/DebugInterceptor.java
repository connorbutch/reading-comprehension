package com.connor.reading.interceptor;

import com.connor.reading.bean.UniqueRequestIdBean;
import org.jboss.logging.Logger;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Interceptor
@Debug
public class DebugInterceptor {
    private static final String NULL_STR = "NULL";

    private final Logger logger;
    private final UniqueRequestIdBean uniqueRequestIdBean;

    DebugInterceptor(Logger logger, UniqueRequestIdBean uniqueRequestIdBean){
        this.logger = logger;
        this.uniqueRequestIdBean = uniqueRequestIdBean;
    }

    @AroundInvoke
    public Object handleAroundInvoke(InvocationContext invocationContext) throws Exception {
        log( getClassAndMethod(invocationContext)  + " invoked with parameters " + getParameterString(invocationContext.getParameters()));
        Object returnValue = null;
        try{
            returnValue = invocationContext.proceed();
        }catch(Exception e){
            //break our log and rethrow rule here for debugging purposes
            log(getClassAndMethod(invocationContext) + "threw exception of type " + e.getClass() + " for parameters " + getParameterString(invocationContext.getParameters()));
            throw e;
        }

        log(getClassAndMethod(invocationContext) + "returning value " + nullSafeToString(returnValue) + " for parameters " + getParameterString(invocationContext.getParameters()));
        return returnValue;
    }

    protected String getClassAndMethod(InvocationContext invocationContext){
        return invocationContext.getMethod().getDeclaringClass().getName() + "." + invocationContext.getMethod().getName();
    }

    protected String getParameterString(Object[] parameters){
        String parameterString = NULL_STR;
        //don't think we need to do this, but just in case
        if(parameters != null){
            if(parameters.length == 0){
                parameterString = "empty (no parameters)";
            }else{
                parameterString = Arrays.stream(parameters).map(this::nullSafeToString).collect(Collectors.joining(", "));
            }
        }
        return parameterString;
    }

    protected String nullSafeToString(Object obj){
        return obj == null? NULL_STR: obj.toString();
    }

    protected boolean isReturnTypeOptional(InvocationContext invocationContext){
        return invocationContext.getMethod().getReturnType() == Optional.class;
    }

    protected Object getFromOptionalSilenceNoSuchElementException(Optional<?> optional){
       Object obj = null;
        try{
            obj= optional.get();
       }catch(NoSuchElementException e){
           //intentionally silence
       }
        return obj;
    }

    protected String getResponseString(Object returnValue, InvocationContext invocationContext){
        String responseValue = null;
        if(isReturnTypeOptional(invocationContext)){
            responseValue = "Nullable with value " +  nullSafeToString(getFromOptionalSilenceNoSuchElementException((Optional)returnValue));
        }else{
            responseValue = nullSafeToString(returnValue);
        }
        return responseValue;
    }

    protected void log(String logString){
        logger.info("Unique id: " + uniqueRequestIdBean.getRequestId() + " " + logString);     //NOTE: can/should change log statements to debug logging here once confirm works with info level
    }
}
