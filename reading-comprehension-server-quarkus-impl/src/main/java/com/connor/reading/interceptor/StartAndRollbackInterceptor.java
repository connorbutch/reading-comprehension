package com.connor.reading.interceptor;

import com.connor.reading.exception.ReadingSystemException;
import org.jboss.logging.Logger;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;
import javax.transaction.Transactional;

@Interceptor
@StartAndRollbackTransaction
public class StartAndRollbackInterceptor {
    private final Logger logger;
    private final TransactionManager transactionManager;

    StartAndRollbackInterceptor(Logger logger, TransactionManager transactionManager) {
        this.logger = logger;
        this.transactionManager = transactionManager;
    }

    @AroundInvoke
    @Transactional(Transactional.TxType.REQUIRES_NEW) //this doesn't seem to start a transaction.... maybe it has to do with interceptor usage....
    public Object handleAroundInvoke(InvocationContext invocationContext) throws Exception {
        logger.info("Using interceptor for transaction");
        Object returnValue = invocationContext.proceed();
        logger.info("Successfully got value from object in interceptor");
        rollbackTransaction();
        logger.info("Successfully rolled back transaction");
        return returnValue;
    }

    protected void rollbackTransaction() {
        try {
            transactionManager.setRollbackOnly();
        } catch (SystemException e) {
            throw new ReadingSystemException("There was an error when rolling back the transaction", e); //TODO
        }
    }
}
