package com.connor.reading.health.impl;

import com.connor.reading.health.api.AbstractBaseHealthChecker;
import com.connor.reading.health.api.HealthCheckName;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import javax.transaction.TransactionManager;
import java.util.Properties;

@Dependent
@HealthCheckName(healthCheckName =  "SystemPropertyChecker")
public class HealthCheckerSystemPropertyImpl extends AbstractBaseHealthChecker<Properties> {

    HealthCheckerSystemPropertyImpl(Logger logger, TransactionManager transactionManager) {
        super(logger, transactionManager);
    }

    @Override
    protected Properties getValue() {
        return System.getProperties();
    }

    @Override
    protected boolean isValid(Properties valueToValidate) {
        logger.debug("Can check if actual properties are existing here in the future");
        return valueToValidate != null && !valueToValidate.isEmpty();
    }
}
