package com.connor.reading.health.api;

import com.connor.reading.exception.ReadingSystemException;
import org.jboss.logging.Logger;

import javax.transaction.SystemException;
import javax.transaction.TransactionManager;
import javax.transaction.Transactional;

/**
 * Most health check implementations should extend this class, rather than implement the HealthChecker interface directly.
 * It provides a flexible, standardized, and vetted framework for handling exceptions, transactions, checking validity of responses and other functionality.
 *
 * For most implementations, it will be sufficient to override the getValue method.  However, if you wish to perform validation
 * on the object after it has been received, or handle errors in a custom way, the other methods can be overwritten.
 *
 * @param <T> the type of the value returned from the check, which can be validated.
 */
public abstract class AbstractBaseHealthChecker<T> implements HealthChecker {
    protected final Logger logger;
    private final TransactionManager transactionManager;

    protected AbstractBaseHealthChecker(Logger logger, TransactionManager transactionManager) {
        this.logger = logger;
        this.transactionManager = transactionManager;
    }

    @Override
    public Boolean get() {
        return doesRequireTransaction() ?
                getWithTransaction() :
                getAndCheckWithExistingTransaction();
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    protected final boolean getWithTransaction() { //if user overrides, there is no check that they keep the annotation for transaction
        logger.info("Executing health check in context of transaction");
        boolean isAvailable = getAndCheckWithExistingTransaction();
        rollbackTransaction();
        logger.info("Successfully rolled back transaction for health check");
        return isAvailable;
    }

    protected boolean getAndCheckWithExistingTransaction() {
        boolean isAvailable = true;
        T value = null;
        try {
            value = getValue();
        } catch (Exception e) {
            isAvailable = false;
            handleException(e);
        }
        if (isAvailable) {
            isAvailable = isValid(value);
            logger.info("Performed health check without any errors, the validation of the response returned " + isAvailable);
        }
        return isAvailable;
    }

    protected boolean isValid(T valueToValidate) {
        logger.info("No validation is being done for an external dependency.  If you would like to perform validation, please override the isValid method in abstract base health checker.");
        return true;
    }

    protected void rollbackTransaction() {
        try {
            transactionManager.setRollbackOnly();
        } catch (SystemException e) {
            throw new ReadingSystemException("There was an error when rolling back the transaction", e);
        }
    }

    protected void handleException(Exception e) {
        logger.info("There was an exception when executing health check", e);
    }

    /**
     * Implement this in any concrete subclass and provide the value to validate.  It is generally considered bad practice
     * to catch exceptions in this method, and instead, they should get propagated up to be handled in a standardized
     * fashion in the invoking method.
     *
     * @return the value returned from performing the check.  For example, this could an assessment gotten from a database.
     */
    protected abstract T getValue();
}
