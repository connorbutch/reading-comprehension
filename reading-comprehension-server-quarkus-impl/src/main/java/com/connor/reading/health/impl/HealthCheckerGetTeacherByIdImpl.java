package com.connor.reading.health.impl;

import com.connor.reading.cdi.GeneratedClient;
import com.connor.reading.health.api.AbstractBaseHealthChecker;
import com.connor.reading.health.api.HealthCheckName;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.util.TeachersApiFactory;
import com.connor.teacher.ApiException;
import com.connor.teacher.dto.Teacher;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import javax.transaction.TransactionManager;

@Debug
@Dependent
@HealthCheckName(healthCheckName = "GetTeacherByIdChecker")
public class HealthCheckerGetTeacherByIdImpl extends AbstractBaseHealthChecker<Teacher> {
    private static final String ERROR_FORMAT_STR = "There was an api exception when getting teacher by id for health check.  The response status code was %d and the response body was %s";

    private final TeachersApiFactory teachersApiFactory;

    HealthCheckerGetTeacherByIdImpl(Logger logger, TransactionManager transactionManager, @GeneratedClient TeachersApiFactory teachersApiFactory) {
        super(logger, transactionManager);
        this.teachersApiFactory = teachersApiFactory;
    }

    @Override
    protected Teacher getValue() {
        return teachersApiFactory.get().getTeacherById(1L);
    }

    @Override
    protected void handleException(Exception e) {
        if(e instanceof ApiException){
            int httpStatusCode = ((ApiException)e).getCode();
            String httpResponseBody = ((ApiException)e).getResponseBody();
            String logString = String.format(ERROR_FORMAT_STR, httpStatusCode, httpResponseBody);
            logger.error(logString, e);
        }else{
            super.handleException(e);
        }
    }
}
