package com.connor.reading.health.impl;

import com.connor.reading.dto.Assessment;
import com.connor.reading.health.api.AbstractBaseHealthChecker;
import com.connor.reading.health.api.HealthCheckName;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.service.AssessmentService;
import com.connor.reading.util.HealthCheckDataUtil;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import javax.transaction.TransactionManager;

@Debug
@Dependent
@HealthCheckName(healthCheckName = "InsertAndReturnAssessmentChecker")
public class HealthCheckerInsertAssessmentImpl extends AbstractBaseHealthChecker<Assessment> {
    private final AssessmentService assessmentService;
    private final HealthCheckDataUtil healthCheckDataUtil;

    HealthCheckerInsertAssessmentImpl(Logger logger,  AssessmentService assessmentService, HealthCheckDataUtil healthCheckDataUtil, TransactionManager transactionManager) {
        super(logger, transactionManager);
        this.assessmentService = assessmentService;
        this.healthCheckDataUtil = healthCheckDataUtil;
    }

    @Override
    public boolean doesRequireTransaction() {
        return true;
    }

    //@StartAndRollbackTransaction -- for some reason, can't get interceptor to work here....
    @Override
    protected Assessment getValue() {
       return assessmentService.addAssessment(healthCheckDataUtil.buildAssessmentForHealthCheck());
    }

    @Override
    protected boolean isValid(Assessment valueToValidate) {
        Assessment expectedAssessmentResult = healthCheckDataUtil.buildAssessmentForHealthCheck();
        return expectedAssessmentResult.equals(valueToValidate);
    }
}
