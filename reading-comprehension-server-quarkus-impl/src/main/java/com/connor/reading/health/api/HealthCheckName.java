package com.connor.reading.health.api;

import java.lang.annotation.*;

/**
 * You can use this interface to annotate a class implementing HealthChecker to give it a different
 * name in the health status (rather than the default, which is the class name).
 */
@Target(ElementType.TYPE)
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface HealthCheckName {
    String healthCheckName();
}
