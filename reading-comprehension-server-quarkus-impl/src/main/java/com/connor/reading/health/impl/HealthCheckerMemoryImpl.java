package com.connor.reading.health.impl;

import com.connor.reading.health.api.AbstractBaseHealthChecker;
import com.connor.reading.health.api.HealthCheckName;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import javax.transaction.TransactionManager;

@Dependent
@HealthCheckName(healthCheckName = "MemoryChecker")
public class HealthCheckerMemoryImpl extends AbstractBaseHealthChecker<Runtime> {

    HealthCheckerMemoryImpl(Logger logger, TransactionManager transactionManager) {
        super(logger, transactionManager);
    }

    @Override
    protected Runtime getValue() {
        return Runtime.getRuntime();
    }

    @Override
    protected boolean isValid(Runtime valueToValidate) {
        //https://stackoverflow.com/questions/12807797/java-get-available-memory
        return valueToValidate.maxMemory() - (valueToValidate.totalMemory() - valueToValidate.freeMemory()) > 0;
    }
}
