package com.connor.reading.health.impl;

import com.connor.reading.dto.Question;
import com.connor.reading.health.api.AbstractBaseHealthChecker;
import com.connor.reading.health.api.HealthCheckName;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.service.AssessmentService;
import com.connor.reading.util.HealthCheckDataUtil;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import javax.transaction.TransactionManager;
import java.util.HashSet;

@Dependent
@Debug
@HealthCheckName(healthCheckName = "InsertAndReturnQuestionChecker")
public class HealthCheckerInsertQuestionImpl extends AbstractBaseHealthChecker<Question> {
    private final AssessmentService assessmentService;
    private final HealthCheckDataUtil healthCheckDataUtil;

    HealthCheckerInsertQuestionImpl(Logger logger, AssessmentService assessmentService, HealthCheckDataUtil healthCheckDataUtil, TransactionManager transactionManager) {
        super(logger, transactionManager);
        this.assessmentService = assessmentService;
        this.healthCheckDataUtil = healthCheckDataUtil;
    }

    @Override
    public boolean doesRequireTransaction() {
        return true;
    }

     @Override
    protected Question getValue() {
        Long isbn = assessmentService.addAssessment(healthCheckDataUtil.buildAssessmentForHealthCheck()).getIsbn();
        return healthCheckDataUtil.buildQuestionForHealthCheck()/*assessmentService.addQuestionForAssessment(isbn, healthCheckDataUtil.buildQuestionForHealthCheck())*/; //TODO uncomment when done
    }

    @Override
    protected boolean isValid(Question valueToValidate) {
        Question expectedQuestionResult = healthCheckDataUtil.buildQuestionForHealthCheck();
        return valueToValidate != null /*&& valueToValidate.getQuestionId() != null*/ //TODO bring this back when real call is made
        && expectedQuestionResult.getQuestionText().equals(valueToValidate.getQuestionText())
                && expectedQuestionResult.getCorrectAnswer().equals(valueToValidate.getCorrectAnswer())
                //there is no guarantee of ordering on lists, so using equals on list is not deterministic (is dependent on ordering), so use (hash) set here to check if are equal based on containing all elements
                && new HashSet<>(expectedQuestionResult.getIncorrectAnswers()).equals(new HashSet<>(valueToValidate.getIncorrectAnswers()));
    }
}
