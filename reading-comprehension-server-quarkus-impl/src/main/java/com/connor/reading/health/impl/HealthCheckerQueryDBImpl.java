package com.connor.reading.health.impl;

import com.connor.reading.dto.Assessment;
import com.connor.reading.health.api.AbstractBaseHealthChecker;
import com.connor.reading.health.api.HealthCheckName;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.service.AssessmentService;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import javax.transaction.TransactionManager;
import java.util.List;

@HealthCheckName(healthCheckName = "QueryForAssessmentsChecker")
@Dependent
@Debug
public class HealthCheckerQueryDBImpl extends AbstractBaseHealthChecker<List<Assessment>> {
    private final AssessmentService assessmentService;

    HealthCheckerQueryDBImpl(Logger logger, TransactionManager transactionManager, AssessmentService assessmentService) {
        super(logger, transactionManager);
        this.assessmentService = assessmentService;
    }

    //reading-comprehension-ws    | Caused by: java.sql.SQLException: Unsupported transaction isolation level '-1'
    //NOTE: this doesn't need it's own transaction like an insert, however, with threading, it associates the thread with the transaction
    //and can end up intermittenly getting the above error; so we give up a little bit of potential performance for stability
    @Override
    public boolean doesRequireTransaction() {
        return true;
    }


    @Override
    protected List<Assessment> getValue() {
        return assessmentService.getAssessments(null, null);
    }

    @Override
    protected boolean isValid(List<Assessment> valueToValidate) {
        return valueToValidate != null;
    }
}
