package com.connor.reading.health.api;

import java.util.function.Supplier;

/**
 * This class provides the base of the health check functionality.  In essence, it should return true if the health check
 * should show as "up" or false if this should show as down.
 *
 * Please try to avoid implementing this class directly.  Instead, whenever possible, please extend AbstractBaseHealthChecker,
 */
@FunctionalInterface
public interface HealthChecker extends Supplier<Boolean> {

    /**
     * With JTA transactions, the check cannot be performed on an executor, or using a future, as the
     * transaction is explicitly associated to a thread.  Only in that case have I found a need to override this
     * method, so the vast majority of implementations should be fine with no changes.
     * @return
     */
    default boolean doesRequireTransaction() {
        return false;
    }

}
