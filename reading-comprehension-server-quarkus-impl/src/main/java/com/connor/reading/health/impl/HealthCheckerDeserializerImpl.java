package com.connor.reading.health.impl;

import com.connor.reading.api.AssessmentsApi;
import com.connor.reading.health.api.AbstractBaseHealthChecker;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.graalvm.collections.Pair;
import org.jboss.logging.Logger;

import javax.transaction.TransactionManager;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

//This is a POC module that ensures all classes that could be used in request/response can be successfully deserialized
//using the object mapper.  This is useful in micronaut/quarkus, as you must explicitly register classes for reflection
//however, the usefulness of this module decreses with acceptance tests, so I put this to a lower priority
//if you would like to continue work on this class, please make sure to bring it back in as Dependent (request) scoped
//@Dependent
public class HealthCheckerDeserializerImpl extends AbstractBaseHealthChecker<Set<Class<?>>> {
    private static final String EMPTY_JSON = "{}";

    private final ObjectMapper objectMapper;

    HealthCheckerDeserializerImpl(Logger logger, TransactionManager transactionManager, ObjectMapper objectMapper) {
        super(logger, transactionManager);
        this.objectMapper = objectMapper;
    }

    @Override
    protected Set<Class<?>> getValue() {
        return Arrays.stream(AssessmentsApi.class.getDeclaredMethods())
                .map(this::getApiResponsesAnnotation)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .flatMap(apiResponses -> Arrays.stream(apiResponses.value()).map(ApiResponse::response))
                .collect(Collectors.toSet());
    }

    @Override
    protected boolean isValid(Set<Class<?>> valueToValidate) {
        valueToValidate.stream()
                .map(this::getClassObjectPair)
                .collect(Collectors.toList());
       return true;
    }

    protected Optional<ApiResponses> getApiResponsesAnnotation(Method method){
        return Optional.ofNullable(method.getAnnotation(ApiResponses.class));
    }

    protected Pair<Class<?>, Object> getClassObjectPair(Class<?> clazz) {
        Object value = null;
        //can use this for responses
        //objectMapper.writeValueAsString(clazz.getConstructor().newInstance());
        try {
            value = objectMapper.readValue(EMPTY_JSON, clazz);
        } catch (JsonProcessingException e) {
            //intentionally silence
        }
        return Pair.create(clazz, value);
    }
}
