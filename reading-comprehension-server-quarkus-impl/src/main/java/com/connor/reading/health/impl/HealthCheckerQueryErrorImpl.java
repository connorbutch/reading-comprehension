package com.connor.reading.health.impl;

import com.connor.reading.dto.DetailedErrorInfo;
import com.connor.reading.health.api.AbstractBaseHealthChecker;
import com.connor.reading.health.api.HealthCheckName;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.service.ErrorService;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import javax.transaction.TransactionManager;

@Debug
@Dependent
@HealthCheckName(healthCheckName = "QueryForErrorByIdChecker")
public class HealthCheckerQueryErrorImpl extends AbstractBaseHealthChecker<DetailedErrorInfo> {
    private static final int ERROR_ID_TO_SEARCH_ON = 2;

    private final ErrorService errorService;

    HealthCheckerQueryErrorImpl(Logger logger, TransactionManager transactionManager, ErrorService errorService) {
        super(logger, transactionManager);
        this.errorService = errorService;
    }

    @Override
    protected DetailedErrorInfo getValue() {
        return errorService.getErrorReasonById(ERROR_ID_TO_SEARCH_ON);
    }

    @Override
    protected boolean isValid(DetailedErrorInfo valueToValidate) {
        return valueToValidate != null && valueToValidate.getErrorId() == ERROR_ID_TO_SEARCH_ON && valueToValidate.getErrorReason().getErrorId() == ERROR_ID_TO_SEARCH_ON;
    }
}
