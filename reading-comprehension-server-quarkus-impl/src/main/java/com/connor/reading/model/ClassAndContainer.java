package com.connor.reading.model;

/**
 * This class exists because for a health check, there is a class, but also a class container
 * in the health check, we want to return the proper response.  If the container is empty, then create
 * an instance of the class itself.  However, if the container is populated (as is the case for a list
 * response),then create a list and add a dummy entry to the list
 *
 * @param <T> the type of class
 */
public class ClassAndContainer<T> {
    private final Class<T> clazz;
    private final String container;

    public ClassAndContainer(Class<T> clazz, String container) {
        this.clazz = clazz;
        this.container = container;
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public String getContainer() {
        return container;
    }
}
