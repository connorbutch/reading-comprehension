package com.connor.reading.model;

import com.connor.reading.dto.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * In the spirit of writing this application to be safe for highly concurrent access
 * there is a need to retrieve the number of assessments associated with a question
 * as well as the questions associated with it.  Querying for if the assessment exists,
 * then making a subsequent query leaves us open to the possibility that the assessment
 * was deleted between queries (please note, another solution to this is getting and
 * maintaining locks, but this is riskier and less performant).  Additionally, it makes
 * two trips to the database, when only one is needed.
 *
 * This object model allows us to retrieve, and store, a list of questions associated
 * with a given assessment, as well as if the assessment exists (to throw a 404)
 */
public class AssessmentCountWithListOfQuestion {
    private boolean doesAssessmentExist;
    private List<Question> questionsAssociatedWithAssessment;

    public boolean doesAssessmentExist() {
        return doesAssessmentExist;
    }

    public void setDoesAssessmentExist(boolean doesAssessmentExist) {
        this.doesAssessmentExist = doesAssessmentExist;
    }

    public List<Question> getQuestionsAssociatedWithAssessment() {
        return questionsAssociatedWithAssessment;
    }

    public void setQuestionsAssociatedWithAssessment(List<Question> questionsAssociatedWithAssessment) {
        this.questionsAssociatedWithAssessment = questionsAssociatedWithAssessment;
    }

    public AssessmentCountWithListOfQuestion doesAssessmentExist(boolean value){
        this.doesAssessmentExist = value;
        return this;
    }

    public AssessmentCountWithListOfQuestion questionsAssociatedWithAssessment(List<Question> questionsAssociatedWithAssessment){
        this.questionsAssociatedWithAssessment = questionsAssociatedWithAssessment;
        return this;
    }

    public AssessmentCountWithListOfQuestion addQuestion(Question questionToAdd){
        if(questionsAssociatedWithAssessment == null){
            questionsAssociatedWithAssessment = new ArrayList<>();
        }
        questionsAssociatedWithAssessment.add(questionToAdd);
        return this;
    }
}
