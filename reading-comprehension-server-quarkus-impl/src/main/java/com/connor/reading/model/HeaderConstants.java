package com.connor.reading.model;

public abstract class HeaderConstants {
    public static final String CONTAINER_ID_HEADER_KEY = "x-container-id";
    public static final String HEALTH_CHECK_REQUEST_HEADER = "x-connectivity-request";
    public static final String HEALTH_CHECK_RESPONSE_HEADER = "x-was-connectivity-request";
    public static final String REQUEST_ID_HEADER_KEY = "x-request-id";
    public static final String REQUEST_SLEEP_HEADER_KEY = "x-request-sleep-time";
    public static final String TRACE_ID_HEADER_KEY = "x-trace-id";
}
