package com.connor.reading.model;

import com.connor.reading.dto.ErrorReason;
import com.connor.reading.service.ValidationService;

import java.util.Locale;

/**
 * This offers a centralized place to view errors.
 *
 * While we could make this enum declared in the open api spec, this is very bad practice, as it breaks clients when a new enum is added.
 * Hence, this is translated to an ErrorReason object (generated from open api) before being returned.
 */
public enum ErrorEnum {
      //get all assessments
      INVALID_LIMIT(1,String.format("Limit must be greater than or equal to %d", ValidationService.MINIMUM_ALLOWED_LIMIT))
    , INVALID_OFFSET(2, String.format("Offset must be greater than or equal to %d", ValidationService.MINIMUM_ALLOWED_OFFSET))

    //add assessment
    , ASSESSMENT_REQUIRED(3, "The assessment to add cannot be null")
    , ISBN_REQUIRED(4, "Isbn must be a not null value")
    , INVALID_ISBN(5, String.format("Isbn must be greater than or equal to %d", ValidationService.MINIMUM_ALLOWED_ISBN))
    , ISBN_DISALLOWED(6, String.format("This particular isbn (%d) is disallowed.", ValidationService.DISALLOWED_ISBN))
    , FIRST_NAME_REQUIRED(7, "Author first name must be a not null value")
    , FIRST_NAME_NOT_BLANK(8, "Author first name must be a non-blank string")
    , FIRST_NAME_EXCEEDS_LENGTH(9, String.format("Author first name must have a length less than %d", ValidationService.MAXIMUM_FIRST_NAME_LENGTH))
    , LAST_NAME_REQUIRED(10, "Author last name must be a not null value")
    , LAST_NAME_NOT_BLANK(11, "Author last must be a non-blank string")
    , LAST_NAME_EXCEEDS_LENGTH(12, String.format("Author last name must have a length less than %d", ValidationService.MAXIMUM_LAST_NAME_LENGTH))
    , TITLE_REQUIRED(13, "Title must be a not null value")
    , TITLE_NOT_BLANK(14, "Title must be a non-blank string")
    , TITLE_EXCEEDS_LENGTH(15, String.format("Title must have a length less than %d", ValidationService.MAXIMUM_TITLE_LENGTH))
    , CREATED_TEACHER_ID_REQUIRED(16, "Created teacher id must be a non-null value")
    , INVALID_CREATED_TEACHER(17, "Created teacher id is not valid.  Please check with owners of the teacher service")
    , TEACHER_ID_DOES_NOT_EXIST(18, "Teacher with id does not exist.  Please check with owners of the teacher service")
    , NUMBER_OF_POINTS_REQUIRED(19, "Number of points must be a non-null value")
    , INVALID_NUMBER_OF_POINTS(20, String.format(Locale.US, "Number of points must be greater than %.2f and less than %.2f", ValidationService.MINIMUM_ALLOWED_NUMBER_OF_POINTS, ValidationService.MAXIMUM_ALLOWED_NUMBER_OF_POINTS))
    , READING_LEVEL_REQUIRED(21, "Reading level must be a not null value")
    , INVALID_READING_LEVEL(22, String.format(Locale.US, "Reading level must be greater than %.2f and less than %.2f", ValidationService.MINIMUM_ALLOWED_READING_LEVEL, ValidationService.MAXIMUM_ALLOWED_READING_LEVEL))
    , ERROR_ID_REQUIRED(23, "Error id is required")
    , INVALID_ERROR_ID(24, String.format("Error id must be greater than %d", ValidationService.MINIMUM_ALLOWED_ERROR_ID))
    , ASSESSMENT_ALREADY_EXISTS(25, "Assessment already exists for that isbn")

    //not found
    , ENDPOINT_NOT_REGISTERED(26, "Endpoint is not valid.  Please check the url and http method")
    , ASSESSMENT_NOT_FOUND(27, "Assessment not found for given isbn")

    //add question
    , QUESTION_REQUIRED(28, "The question to add cannot be null")
    , QUESTION_ID_PROVIDED_WHEN_NOT_ALLOWED(29, "Question id should not be set in request.  it is autogenerated.")
    , QUESTION_TEXT_REQUIRED(30, "Question text must be a not null value")
    , QUESTION_TEXT_NOT_BLANK(31, "Question text must be a non-blank string")
    , QUESTION_TEXT_EXCEEDS_LENGTH(32, String.format("Question text must have length of less than or equal to %d", ValidationService.MAXIMUM_QUESTION_TEXT_LENGTH))
    , INCORRECT_ANSWERS_REQUIRED(33, "Incorrect answers must be a not null value")
    , INCORRECT_ANSWERS_NOT_EMPTY(34, String.format("Incorrect answers must contain at least %d incorrect answers", ValidationService.MINIMUM_NUMBER_OF_INCORRECT_ANSWERS))
    , INCORRECT_ANSWERS_EXCEEDS_MAX(35, String.format("There can be a maximum of %d incorrect answers for a given question", ValidationService.MINIMUM_NUMBER_OF_INCORRECT_ANSWERS))
    , CORRECT_ANSWER_REQUIRED(36, "Correct answer is required")
    , CORRECT_ANSWER_NOT_BLANK(37, "Correct answer must be a non-blank string")
    , CORRECT_ANSWER_EXCEEDS_MAX(38, String.format("Correct answer must have length of less than or equal to %d", ValidationService.MAXIMUM_ALLOWED_CORRECT_ANSWER_LENGTH))


    //retrieve question
    , QUESTION_ID_REQUIRED(39, "Question id is required")
    , INVALID_QUESTION_ID(40, String.format("Question id must be greater than %d", ValidationService.MINIMUM_ALLOWED_QUESTION_ID))

    //unexpected
    , UNEXPECTED(999999999, "Unexpected error occurred")
    ;

    private final int errorId;
    private final String explanation;

    private ErrorEnum(int errorId, String explanation) {
        this.errorId = errorId;
        this.explanation = explanation;
    }

    public int getErrorId() {
        return errorId;
    }

    public ErrorReason convertToErrorReason(){
        return new ErrorReason()
                .errorId(errorId)
                .explanation(explanation);
    }
}
