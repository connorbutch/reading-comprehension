package com.connor.reading.cdi;


import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;
import java.lang.annotation.*;

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target({ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD})
public @interface GeneratedClient {
    @Nonbinding
    String host() default "172.18.0.18";
    @Nonbinding
    String scheme() default "http";
    @Nonbinding
    int port() default 8080;
}
