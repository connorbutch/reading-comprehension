package com.connor.reading.cdi;

import javax.inject.Qualifier;
import java.lang.annotation.*;

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target({ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD})
public @interface Cache {
    //intentionally blank -- is just a cdi qualifier to make injection point resolve to exactly one bean
}
