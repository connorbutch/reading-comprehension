package com.connor.reading.cdi;

import com.connor.quarkus.jdbc.QuarkusJdbcTemplate;
import com.connor.quarkus.jdbc.QuarkusKeyHolder;
import com.connor.quarkus.jdbc.QuarkusKeyHolderLongImpl;
import com.connor.quarkus.jdbc.QuarkusNamedJdbcTemplate;
import com.connor.reading.bean.HealthCheckBean;
import com.connor.reading.bean.UniqueRequestIdBean;
import com.connor.reading.dto.DetailedErrorInfo;
import com.connor.reading.health.api.HealthChecker;
import com.connor.reading.util.TeachersApiFactory;
import io.quarkus.arc.DefaultBean;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CdiProducerUtil {
    @Produces
    public Logger getLogger(InjectionPoint ip) {
        return Logger.getLogger(ip.getMember().getDeclaringClass());
    }

    @Produces
    public ManagedExecutor getManagedExecutor(InjectionPoint ip) {
        return ManagedExecutor.builder().build();
    }

    //This is application scoped for a few reasons:
    //1 - this class is threadsafe, so only one instance allows lower memory footprint in serverless world
    //2 - application scoped (or any other regular scope, such as request scoped) can be replaced with QuarkusMock
    //3 - application scoped can benefit from performance benefits of graalvm ahead of time compilation
   @ApplicationScoped
   public QuarkusJdbcTemplate getQuarkusJdbcTemplate(DataSource dataSource) {
        return new QuarkusJdbcTemplate(dataSource);
    }

    //This is application scoped for a few reasons:
    //1 - this class is threadsafe, so only one instance allows lower memory footprint in serverless world
    //2 - application scoped (or any other regular scope, such as request scoped) can be replaced with QuarkusMock
    //3 - application scoped can benefit from performance benefits of graalvm ahead of time compilation
    @ApplicationScoped
    public QuarkusNamedJdbcTemplate getQuarkusNamedJdbcTemplate(QuarkusJdbcTemplate quarkusJdbcTemplate) {
        return new QuarkusNamedJdbcTemplate(quarkusJdbcTemplate);
    }

    @Cache
    public Map<String, String> getCache(InjectionPoint ip) {
        return new HashMap<>();
    }

    @HealthCheck
    public List<HealthChecker> getHealthCheckers(InjectionPoint ip, Instance<HealthChecker> allHealthCheckers) {
        HealthCheck healthCheckOnInjectionPoint = ip.getAnnotated().getAnnotation(HealthCheck.class);
        return healthCheckOnInjectionPoint.mustRunOnSameThread() ?
                allHealthCheckers.stream().filter(HealthChecker::doesRequireTransaction).collect(Collectors.toList()) :
                allHealthCheckers.stream().filter(healthChecker -> !healthChecker.doesRequireTransaction()).collect(Collectors.toList());
    }

    @Cache
    public Map<Integer, DetailedErrorInfo> getErrorReasonsMap(InjectionPoint ip) {
        return new HashMap<>();
    }

    @Produces
    public QuarkusKeyHolder<Long> getLongQuarkusKeyHolder(InjectionPoint ip) {
        return new QuarkusKeyHolderLongImpl();
    }

   @GeneratedClient
   @DefaultBean //allow to be replaced for verification in integration tests
   public TeachersApiFactory getTeachersApiFactory(InjectionPoint ip, Logger logger, HealthCheckBean healthCheckBean, UniqueRequestIdBean uniqueRequestIdBean){
        String scheme =  ip.getAnnotated().getAnnotation(GeneratedClient.class).scheme();
        String host = ip.getAnnotated().getAnnotation(GeneratedClient.class).host();
        int port =  ip.getAnnotated().getAnnotation(GeneratedClient.class).port();
        logger.info("Returning new teacher api factory with scheme " + scheme + " host " + host + " port " + port);
        return new TeachersApiFactory(logger, healthCheckBean, uniqueRequestIdBean, scheme, host, port);
    }
}
