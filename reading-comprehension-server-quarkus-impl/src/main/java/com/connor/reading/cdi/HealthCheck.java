package com.connor.reading.cdi;


import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;
import java.lang.annotation.*;

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target({ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD})
public @interface HealthCheck {

    /**
     * If this is set to true, then this health check will run on the same thread.
     * This is needed for JTA transactions.
     * @return
     */
    @Nonbinding
    boolean mustRunOnSameThread() default false;
}
