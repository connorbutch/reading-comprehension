package com.connor.reading.bean;

import javax.enterprise.context.RequestScoped;

/**
 * This is a bean that is used to keep track of whether or not a request is a health check.
 *
 * This is useful, because I had originally offered overloaded methods with a flag for is health check (for setting transaction
 * isoloation level), but I accidentally called the wrong endpoint.  So to save the confusion, I added this bean, and it is
 * much harder to "accidentally" call set on this bean.
 */
@RequestScoped
public class HealthCheckBean {
    private boolean isHealthCheck = false;

    public void setHealthCheck(boolean isHealthCheck) {
        this.isHealthCheck = isHealthCheck;
    }

    public boolean isHealthCheck() {
        return isHealthCheck;
    }
}
