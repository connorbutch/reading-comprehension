package com.connor.reading.bean;

import io.quarkus.runtime.Startup;
import org.jboss.logging.Logger;

import javax.inject.Singleton;
import java.util.UUID;

/**
 * This bean exists to give a (unique) id to each application/container.  This exists to primarily show that knative is
 * working and spinning up new pods in response to traffic.
 * *
 * There are two essential things about this bean:
 * 1 - the startup annotation.  Please note that observing the initialization of application scoped does NOT work when
 * using graalvm (as this gets executed at ahead of time compilation build time)
 * 2 - the singleton annotation -- if application scoped annotation is used, a cdi proxy will be created for each time it
 * is injected, and this will lead to the constructor being called multiple times, which can pollute our logs
 */
@Startup
@Singleton
public class ContainerIdBean {
    private final UUID containerId;

    ContainerIdBean(Logger logger){
        containerId = UUID.randomUUID();
        logger.debug("Successfully generated container id of " + containerId + " for this container");
    }

    public UUID getContainerId() {
        return containerId;
    }
}
