package com.connor.reading.bean;

import io.quarkus.flyway.FlywayDataSource;
import io.quarkus.runtime.Startup;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.Location;
import org.jboss.logging.Logger;

import javax.inject.Singleton;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * This bean takes care of (conditionally) performing flyaway migrations.  There are three different flows, which are listed below
 *
 * 1 - "regular" this occurs the container.type config property is NOT equal to acceptance-test and integration-test.  This runs the "regular"
 * flyaway migration (ddl/dml defined in src/main/resources/db/migration).  This will not migrate test data
 * 2 - acceptance test migration -- this migrates the data from the regular migration, in addition, runs dml to populate database for
 * acceptance tests
 * 3 - integration test this represents the application booted by quarkusTest.  In this case, no data is ran as we just verify
 * sql ran and mock responses by replacing quarkusjdbctemplate in arc cdi container and mock responses.  This will run if the
 * container type is "integration-test"
 */
@Startup
@Singleton //Application scoped will have the constructor invoked multiple times (for a proxy for the application scoped), so make singleton to avoid confusing logs
public class FlyawayBean {
    private static final String CONTAINER_TYPE = "container.type";
    private static final String ACCEPTANCE_TEST_VALUE = "acceptance-test";
    private static final String INTEGRATION_TEST_VALUE = "integration-test";

    private final Logger logger;
    private final String containerTypeString;

    FlyawayBean(Logger logger, @ConfigProperty(name = CONTAINER_TYPE)
            String containerTypeString, Flyway flyway, @FlywayDataSource("samedbfortest") Flyway secondFlyawayForTestData) {
        this.logger = logger;
        this.containerTypeString = containerTypeString;
        logger.info("Container type string " + containerTypeString);
        if(!isIntegrationTestEnvironment()) {
            migrateRegularFlyaway(flyway);
            if (isAcceptanceTestEnvironment()) {
                migrateAcceptanceTestData(secondFlyawayForTestData);
            } else {
                logger.info("This is an actual environment, so no test data is migrated");
            }
        }else{
            logger.warn("This is an integration test environment (booted by quarkus test), so NO flyaway migration performed (for either regular ddl or test data dml)");
        }
    }

    protected boolean isIntegrationTestEnvironment(){
        return INTEGRATION_TEST_VALUE.equalsIgnoreCase(containerTypeString);
    }

    protected boolean isAcceptanceTestEnvironment() {
        return ACCEPTANCE_TEST_VALUE.equalsIgnoreCase(containerTypeString);
    }

    protected void migrateRegularFlyaway(Flyway flyway){
        logger.info("Flyaway locations: " + Arrays.stream(flyway.getConfiguration().getLocations()).map(Location::getPath).collect(Collectors.joining(", ")));
        flyway.migrate();
        logger.info("Successfully migrated \"regular\" flyaway");
    }

    protected void migrateAcceptanceTestData(Flyway secondFlyawayForTestData){
        logger.info("This is an acceptance test environment, so we will migrate test data as well");
        secondFlyawayForTestData.migrate();
        logger.info("Successfully migrated acceptance test data (dml) via flyaway");
    }
}
