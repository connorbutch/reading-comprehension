package com.connor.reading.bean;

import org.jboss.logging.Logger;

import javax.enterprise.context.RequestScoped;
import java.util.UUID;

/**
 * There is some confusion around the difference between request id and trace id.  Please consider the below example
 * for clarification:
 *
 *
 * Say we have three components (webservices) in our distributed architecture.  Service a is invoked by a client, and in order to
 * fulfill the request, it invokes Service b and Service c.
 *
 *                                    Service b
 *                                  /
 * (request id 1, trace id1)       / (request id 2, trace id 1)
 * Client app ---------> Service a
 *                                 \ (request id 3, trace id 1)
 *                                  \ Service c
 *
 * Each request gets a unique request id
 * Each set of "connected" requests shares the same trace id, which is used for tracing and debugging in a distributed world.
 */
@RequestScoped
public class UniqueRequestIdBean {
    private final UUID requestId;
    private UUID traceId;

    UniqueRequestIdBean(Logger logger){
        requestId = UUID.randomUUID();
        logger.info("Generated unique id for request " + requestId.toString());
    }

    public UUID getRequestId() {
        return requestId;
    }

    public UUID getTraceId() {
        return traceId;
    }

    public void setTraceId(UUID traceId) {
        this.traceId = traceId;
    }
}
