package com.connor.reading.controller;

import com.connor.reading.api.AssessmentsApi;
import com.connor.reading.dto.Assessment;
import com.connor.reading.dto.Question;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.service.AssessmentService;
import org.jboss.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/assessments") //We need path annotation per jaxrs specs, so we have written unit tests that ensure these are always the same
@Debug
@RequestScoped
public class AssessmentsApiImpl implements AssessmentsApi {
    private static final String CREATED_ASSESSMENT_HEADER_FORMAT_STR = "/assessments/%d";
    private static final String CREATED_QUESTION_HEADER_FORMAT_STR = "/assessments/%d/questions/%d";

    @Inject
    Logger logger;
    @Inject
    AssessmentService assessmentService;

    @Override
    public Response getAssessments(Integer limit, Integer offset) {
        logger.info("Received request to get assessments with limit " + limit + " and offset " + offset);
        List<Assessment> responseBody = assessmentService.getAssessments(limit, offset);
        logger.debug("Successfully  made call to get all assessments.  now to get total number in db");
        int totalNumberOfAssessments = assessmentService.getTotalNumberOfAssessments();
        logger.debug("Successfully made call to get total number of assessments");
        return Response.ok(responseBody)
                .header("x-total-assessments", totalNumberOfAssessments)
                .build();
    }

    @Override
    public Response createAssessment(Assessment assessment) {
        logger.info("Received request to add assessment " + assessment);
        Assessment assessmentFromService = assessmentService.addAssessment(assessment);
        logger.debug("Successfully added (and retrieved) assessment");
        return Response.created(buildUriForAssessment(assessmentFromService))
                .entity(assessmentFromService)
                .build();
    }

    @Override
    public Response getAssessmentByIsbn(Long isbn) {
        logger.info("Received request to get assessment by isbn " + isbn);
        Assessment assessmentForIsbn = assessmentService.getAssessmentByIsbn(isbn);
        logger.debug("Successfully retrieved assessment " + assessmentForIsbn + " for isbn " + isbn);
        return Response.ok(assessmentForIsbn).build();
    }

    @Override
    public Response getQuestionsForAssessment(Long isbn) {
        logger.info("Received request to get all assessments for isbn " + isbn);
        return Response.ok(assessmentService.getQuestionsForIsbn(isbn)).build();
    }

    @Override
    public Response createQuestion(Long isbn, Question question) {
        logger.info("Received a request to create a new question for isbn " + isbn + " and question to add " + question);
        Question questionFromService = assessmentService.addQuestionForAssessment(isbn, question);
        return Response.created(buildUriForQuestion(isbn, questionFromService.getQuestionId()))
                .entity(questionFromService)
                .build();
    }

    protected URI buildUriForAssessment(Assessment assessmentFromService) {
        return URI.create(String.format(CREATED_ASSESSMENT_HEADER_FORMAT_STR, assessmentFromService.getIsbn()));
    }

    protected URI buildUriForQuestion(Long isbn, Long questionId) {
        //NOTE: in the future, see if there is a better way to construct this
        return URI.create(String.format(CREATED_QUESTION_HEADER_FORMAT_STR, isbn, questionId));
    }
}
