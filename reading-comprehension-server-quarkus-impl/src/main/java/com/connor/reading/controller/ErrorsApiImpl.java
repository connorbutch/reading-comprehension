package com.connor.reading.controller;

import com.connor.reading.api.ErrorsApi;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.service.ErrorService;
import org.jboss.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/errors")
@Debug
@RequestScoped
public class ErrorsApiImpl implements ErrorsApi  {
    @Inject
    Logger logger;
    @Inject
    ErrorService errorService;

    @Override
    public Response getErrorById(Integer errorId) {
        logger.info("Received request to get more information on an error by id " + errorId);
        return Response.ok(errorService.getErrorReasonById(errorId)).build();
    }
}
