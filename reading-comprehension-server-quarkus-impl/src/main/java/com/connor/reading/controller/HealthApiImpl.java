package com.connor.reading.controller;

import com.connor.reading.api.HealthApi;
import com.connor.reading.bean.HealthCheckBean;
import com.connor.reading.dto.HealthResponse;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.service.HealthService;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/health") //We need path annotation per jaxrs specs, so we have written unit tests that ensure these are always the same
@Debug
public class HealthApiImpl implements HealthApi {
    private static final String OK_STATUS_STR = "ok";

    @Inject
    Logger logger;
    @Inject
    HealthService healthService;
    @Inject
    HealthCheckBean healthCheckBean;
    @ConfigProperty(name = "quarkus.flyway.locations")
    String quarkusLocations;

    @Override
    public Response livelinessCheck() {
        logger.info("Received liveliness health check request");
        return Response.ok(new HealthResponse().healthStatus(OK_STATUS_STR)).build();
    }

    @Override
    public Response readinessCheck(Integer xTimeoutValue) {
        logger.info("Received readiness health check request");
        healthCheckBean.setHealthCheck(true);
        return Response.ok(healthService.checkDownstreamDependencies(xTimeoutValue)).build();
    }
}
