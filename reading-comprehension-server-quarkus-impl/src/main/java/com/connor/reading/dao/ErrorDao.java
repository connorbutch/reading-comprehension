package com.connor.reading.dao;

import com.connor.reading.cdi.Cache;
import com.connor.reading.dto.DetailedErrorInfo;
import com.connor.reading.model.ErrorEnum;
import org.jboss.logging.Logger;

import javax.inject.Singleton;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

/**
 * Note, while some people might implement this as an enum, this breaks generated clients if you add more fields to the enum and they don't update their version.
 * Hence, it is preferable (and more dynamic) to use a set/list/etc.
 */
@Singleton
public class ErrorDao {
    private static final Object LOCK = new Object();

    private final Logger logger;
    private final Map<Integer, DetailedErrorInfo> errorReasonsMappedToId;

    ErrorDao(Logger logger, @Cache Map<Integer, DetailedErrorInfo> errorReasonsMappedToId) {
        this.logger = logger;
        this.errorReasonsMappedToId = errorReasonsMappedToId;
        initializeErrorReasons();
        logger.info("Successfully initialized error reason cache");
    }

    public Optional<DetailedErrorInfo> getDetailedErrorInfoById(int errorIdToSearchOn){
        return Optional.ofNullable(errorReasonsMappedToId.get(errorIdToSearchOn));
    }

    protected void initializeErrorReasons() {
        //not sure if singleton is threadsafe, so synchronize to be safe
        synchronized (LOCK){
            Arrays.stream(ErrorEnum.values())
                    .map(ErrorEnum::convertToErrorReason)
                    //TODO populate rest of information -- if get time, this is an extra project that may help people in the future, and not business critical
                    .map(errorReason -> new DetailedErrorInfo().errorReason(errorReason).errorId(errorReason.getErrorId()))
                    .forEach(detailedErrorInfo -> errorReasonsMappedToId.put(detailedErrorInfo.getErrorId(), detailedErrorInfo));
        }
    }
}
