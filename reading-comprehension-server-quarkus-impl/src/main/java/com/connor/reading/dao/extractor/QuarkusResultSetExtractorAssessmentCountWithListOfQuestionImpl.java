package com.connor.reading.dao.extractor;

import com.connor.quarkus.jdbc.QuarkusResultSetExtractor;
import com.connor.reading.dao.SqlConstants;
import com.connor.reading.dto.Question;
import com.connor.reading.exception.ReadingSystemException;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.model.AssessmentCountWithListOfQuestion;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@Debug
@Dependent
public class QuarkusResultSetExtractorAssessmentCountWithListOfQuestionImpl implements QuarkusResultSetExtractor<AssessmentCountWithListOfQuestion> {
    private final Logger logger;

    QuarkusResultSetExtractorAssessmentCountWithListOfQuestionImpl(Logger logger) {
        this.logger = logger;
    }

    @Override
    public AssessmentCountWithListOfQuestion extractData(ResultSet rs) throws SQLException {
        logger.debug("Attempting to extract AssessmentCountWithListOfQuestion from result set");
        AssessmentCountWithListOfQuestion assessmentCountWithListOfQuestion = new AssessmentCountWithListOfQuestion();
        Map<Long, Question> questionsMappedToDatabasePrimaryKey = new HashMap<>();
        boolean isFirstRun = true;
        while (rs.next()) {
            if (isFirstRun) {
                logger.debug("This is the first row in the result set, so checking if assessment exists");
                assessmentCountWithListOfQuestion.setDoesAssessmentExist(rs.getInt(SqlConstants.ASSESSMENT_COUNT_KEY) > 0);
                isFirstRun = false;
            }
            Question questionForId = questionsMappedToDatabasePrimaryKey.computeIfAbsent(rs.getObject(SqlConstants.QUESTION_ID, Long.class),
                    questionId -> buildQuestionFromResultSet(rs, questionId));
            List<String> incorrectAnswers = getIncorrectAnswersOrInitializeIfNull(questionForId);
            Optional.ofNullable(rs.getString(SqlConstants.INCORRECT_ANSWER_KEY)).ifPresent(incorrectAnswers::add);
            questionForId.setIncorrectAnswers(incorrectAnswers);
        }
        return assessmentCountWithListOfQuestion
                .questionsAssociatedWithAssessment(new ArrayList<>(questionsMappedToDatabasePrimaryKey.values()
                        .stream()
                        .filter(question -> question.getQuestionId() != null)
                        .collect(Collectors.toList())));
    }

    protected Question buildQuestionFromResultSet(ResultSet rs, Long questionId){
        return new Question()
                .questionId(questionId)
                .correctAnswer(getStringThrowUncheckedException(rs, SqlConstants.CORRECT_ANSWER_KEY))
                .questionText(getStringThrowUncheckedException(rs, SqlConstants.QUESTION_TEXT_KEY));
    }

    protected String getStringThrowUncheckedException(ResultSet rs, String key) {
        try {
            return rs.getString(key);
        } catch (SQLException e) {
            String errorMessage = String.format("There was an error when attempting to read key %s from result set", key);
            throw new ReadingSystemException(errorMessage, e);
        }
    }

    protected List<String> getIncorrectAnswersOrInitializeIfNull(Question questionForId){
        List<String> incorrectAnswers = questionForId.getIncorrectAnswers();
        if (incorrectAnswers == null) {
            incorrectAnswers = new ArrayList<>();
        }
        return incorrectAnswers;
    }
}
