package com.connor.reading.dao;

import com.connor.quarkus.jdbc.*;
import com.connor.reading.bean.HealthCheckBean;
import com.connor.reading.dto.Assessment;
import com.connor.reading.dto.Question;
import com.connor.reading.exception.ReadingSystemException;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.model.AssessmentCountWithListOfQuestion;
import com.connor.reading.util.SqlProvider;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Debug
@Dependent
public class AssessmentDao {
    private static final String GET_ALL_ASSESSMENTS_SQL_KEY = "GET_ALL_ASSESSMENTS";
    private static final String GET_ASSESSMENT_FOR_LIMIT_AND_OFFSET_SQL_KEY = "GET_ASSESSMENT_LIMIT_OFFSET";
    private static final String GET_ASSESSMENT_COUNT_SQL_KEY = "GET_ASSESSMENT_COUNT";
    private static final String ADD_ASSESSMENT_SQL_KEY = "ADD_ASSESSMENT";
    private static final String GET_ASSESSMENT_BY_ISBN_KEY = "GET_ASSESSMENT_BY_ISBN";
    private static final String GET_QUESTIONS_FOR_ISBN_KEY = "GET_QUESTIONS_FOR_ISBN";
    private static final String ADD_QUESTION_SQL_KEY = "ADD_QUESTION";
    private static final String GET_QUESTION_BY_ID_SQL_KEY = "GET_QUESTION_BY_ID";

    private static final String MISSING_SQL_FORMAT_STR = "No sql found for key %s";

    private final Logger logger;
    private final SqlProvider sqlProvider;
    private final QuarkusJdbcTemplate quarkusJdbcTemplate;
    private final QuarkusNamedJdbcTemplate quarkusNamedJdbcTemplate;
    private final QuarkusRowMapper<Assessment> assessmentRowMapper;
    private final QuarkusRowMapper<Question> questionRowMapper;
    private final QuarkusKeyHolder<Long> longQuarkusKeyHolder;
    private final HealthCheckBean healthCheckBean;
    private final QuarkusResultSetExtractor<AssessmentCountWithListOfQuestion> assessmentCountWithListOfQuestion;

    AssessmentDao(Logger logger, SqlProvider sqlProvider, QuarkusJdbcTemplate quarkusJdbcTemplate, QuarkusNamedJdbcTemplate quarkusNamedJdbcTemplate, QuarkusRowMapper<Assessment> assessmentRowMapper, QuarkusRowMapper<Question> questionRowMapper, QuarkusKeyHolder<Long> longQuarkusKeyHolder, HealthCheckBean healthCheckBean, QuarkusResultSetExtractor<AssessmentCountWithListOfQuestion> assessmentCountWithListOfQuestion) {
        this.logger = logger;
        this.sqlProvider = sqlProvider;
        this.quarkusJdbcTemplate = quarkusJdbcTemplate;
        this.quarkusNamedJdbcTemplate = quarkusNamedJdbcTemplate;
        this.assessmentRowMapper = assessmentRowMapper;
        this.questionRowMapper = questionRowMapper;
        this.longQuarkusKeyHolder = longQuarkusKeyHolder;
        this.healthCheckBean = healthCheckBean;
        this.assessmentCountWithListOfQuestion = assessmentCountWithListOfQuestion;
    }

    public List<Assessment> getAllAssessments() {
        String sql = sqlProvider.apply(GET_ALL_ASSESSMENTS_SQL_KEY)
                .orElseThrow(() -> new ReadingSystemException(String.format(MISSING_SQL_FORMAT_STR, GET_ALL_ASSESSMENTS_SQL_KEY)));
        logger.debug("Successfully got sql to execute for get all assessments " + sql);
        try {
            return quarkusJdbcTemplate.query(sql, assessmentRowMapper);
        }catch(QuarkusDataAccessException e){
            String errorMessage = String.format("There was an error when querying the database for all assessments.  The sql ran was %s", sql);
            throw new ReadingSystemException(errorMessage, e);
        }
    }

    public List<Assessment> getAssessmentsForLimitAndOffset(int limit, int offset) {
        String sql = sqlProvider.apply(GET_ASSESSMENT_FOR_LIMIT_AND_OFFSET_SQL_KEY)
                .orElseThrow(() -> new ReadingSystemException(String.format(MISSING_SQL_FORMAT_STR, GET_ASSESSMENT_FOR_LIMIT_AND_OFFSET_SQL_KEY)));
        logger.debug("Successfully got sql to execute for get all assessments with limit and offset" + sql);
        Map<String, ?> parameters = Map.of(SqlConstants.LIMIT_PARAMETER, limit, SqlConstants.OFFSET_PARAMETER, offset);
        try{
            return quarkusNamedJdbcTemplate.query(sql, parameters, assessmentRowMapper);
        }catch(QuarkusDataAccessException e){
            String errorMessage = String.format("There was an error when querying for assessments for limit and offset.  The sql ran was %s and the param map was %s", sql, buildParametersFromMap(parameters));
            throw new ReadingSystemException(errorMessage, e);
        }
    }

    public int getTotalNumberOfAssessments() {
        String sql = sqlProvider.apply(GET_ASSESSMENT_COUNT_SQL_KEY)
                .orElseThrow(() -> new ReadingSystemException(String.format(MISSING_SQL_FORMAT_STR, GET_ASSESSMENT_COUNT_SQL_KEY)));
        logger.debug("Successfully got sql for get assessment count " + sql);
        try{
            return quarkusJdbcTemplate.queryForInt(sql);
        }catch (QuarkusDataAccessException e){
            String errorMessage = String.format("There was an error when getting assessment count.  The sql ran was %s", sql);
            throw new ReadingSystemException(errorMessage, e);
        }
    }

    /**
     * With this method we need to determine if it was a duplicate key exception (so we can return a 409) as opposed to a
     * general sql exception, which will get marshalled as a 500
     * @param assessment
     * @return true if inserted successfully; or false in the case of duplicate (primary) key exception
     */
    public boolean addAssessment(Assessment assessment) {
        boolean didInsertSuccessfully = true;
        String sql = sqlProvider.apply(ADD_ASSESSMENT_SQL_KEY)
                .orElseThrow(() -> new ReadingSystemException(String.format(MISSING_SQL_FORMAT_STR, ADD_ASSESSMENT_SQL_KEY)));
        Map<String, ?> parameters = Map.of(SqlConstants.ISBN_KEY, assessment.getIsbn()
                , SqlConstants.CREATED_TEACHER_ID_KEY, assessment.getCreatedTeacherId()
                , SqlConstants.FIRST_NAME_KEY, assessment.getAuthorFirstName()
                , SqlConstants.LAST_NAME_KEY, assessment.getAuthorLastName()
                , SqlConstants.NUMBER_OF_POINTS_KEY, assessment.getNumberOfPoints()
                , SqlConstants.READING_LEVEL_KEY, assessment.getReadingLevel()
                , SqlConstants.TITLE_KEY, assessment.getTitle()
                );
        try{
            quarkusNamedJdbcTemplate.update(sql, parameters);
        }catch(QuarkusDataAccessException e){
            if(e.getCause() instanceof SQLIntegrityConstraintViolationException){
               logger.debug("There is already an assessment with isbn " + assessment.getIsbn());
                didInsertSuccessfully = false;
            }else{
                String errorMessage = String.format("There was an error when inserting an assessment count.  The sql ran was %s and the parameter map was %s", sql, buildParametersFromMap(parameters));
                throw new ReadingSystemException(errorMessage, e);
            }
          }
        return didInsertSuccessfully;
    }

    public Optional<Assessment> getAssessmentByIsbn(long isbn) {
        boolean isHealthCheck = healthCheckBean.isHealthCheck();
        String sql = sqlProvider.apply(GET_ASSESSMENT_BY_ISBN_KEY)
                .orElseThrow(() -> new ReadingSystemException(String.format(MISSING_SQL_FORMAT_STR, GET_ASSESSMENT_BY_ISBN_KEY)));
        Map<String, ?> parameters = Map.of(SqlConstants.ISBN_KEY, isbn);
        try{
            return Optional.ofNullable(quarkusNamedJdbcTemplate.queryForObject(sql, parameters, assessmentRowMapper, isHealthCheck));
        }catch(QuarkusDataAccessException e){
            String errorMessage = String.format("There was an error when querying for a single assessment, the sql ran was %s and parameters were %s", sql, buildParametersFromMap(parameters));
            throw new ReadingSystemException(errorMessage, e);
        }
    }

    public AssessmentCountWithListOfQuestion getQuestionsForIsbn(long isbn) {
        String sql = sqlProvider.apply(GET_QUESTIONS_FOR_ISBN_KEY)
                .orElseThrow(() -> new ReadingSystemException(String.format(MISSING_SQL_FORMAT_STR, GET_QUESTIONS_FOR_ISBN_KEY)));
        //add the same parameter twice (with two different keys) since the named template can't substitute values twice
        Map<String, ?> parameters = Map.of(SqlConstants.ISBN_KEY, isbn, SqlConstants.SECOND_ISBN, isbn);
        try{
            return quarkusNamedJdbcTemplate.query(sql, parameters, assessmentCountWithListOfQuestion);
        }catch(QuarkusDataAccessException e){
            String errorMessage = String.format("There was an error when querying for a all questions for an assessment, the sql ran was %s and parameters were %s", sql, buildParametersFromMap(parameters));
            throw new ReadingSystemException(errorMessage, e);
        }
    }

    public long addQuestion(long isbn, Question question) {
        int numberOfRowsUpdated = -1;
        String sql = sqlProvider.apply(ADD_QUESTION_SQL_KEY)
                .orElseThrow(() -> new ReadingSystemException(String.format(MISSING_SQL_FORMAT_STR, ADD_QUESTION_SQL_KEY)));
        Map<String, ?> parameters = null; //TODO
        try{
            numberOfRowsUpdated = quarkusNamedJdbcTemplate.update(sql, parameters, longQuarkusKeyHolder);
        }catch(QuarkusDataAccessException e){
            throw new ReadingSystemException("There was an error when adding question (and retrieving generated key)", e);
        }
        logger.debug("Successfully added question (which involved updating " + numberOfRowsUpdated + " rows), now to add incorrect answer for each");
        question.getIncorrectAnswers()
                .forEach(incorrectAnswer -> addIncorrectAnswer(longQuarkusKeyHolder.getKeyValue(), incorrectAnswer));
        return longQuarkusKeyHolder.getKeyValue();
    }

    public Optional<Question> getQuestionById(long isbn, long createdQuestionId) {
        String sql = sqlProvider.apply(GET_QUESTION_BY_ID_SQL_KEY)
                .orElseThrow(() -> new ReadingSystemException(String.format(MISSING_SQL_FORMAT_STR, GET_QUESTION_BY_ID_SQL_KEY)));
        Map<String, ?> parameters = null; //TODO
        try{
            return Optional.ofNullable(quarkusNamedJdbcTemplate.queryForObject(sql, parameters, this.questionRowMapper));
        }catch(QuarkusDataAccessException e){
            throw new ReadingSystemException("There was an error when getting question by id", e);
        }
    }

    protected String buildParametersFromMap(Map<String, ?> parameters){
        return parameters
                .entrySet()
                .stream()
                .map(entryInMap -> entryInMap.getKey() + ": " + entryInMap.getValue())
                .collect(Collectors.joining(", "));
    }

    protected void addIncorrectAnswer(long questionId, String incorrectAnswer){
        //TODO
    }
}
