package com.connor.reading.dao.extractor;

import com.connor.quarkus.jdbc.QuarkusDataAccessException;
import com.connor.quarkus.jdbc.QuarkusResultSetExtractor;
import com.connor.reading.dao.SqlConstants;
import com.connor.reading.dto.Question;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Dependent
public class QuarkusResultSetExtractorListQuestionImpl implements QuarkusResultSetExtractor<List<Question>> {
    private static final String ERROR_FORMAT_STR = "There was an error when extracting %s from result set (using key %s)";

    private final Logger logger;

    QuarkusResultSetExtractorListQuestionImpl(Logger logger) {
        this.logger = logger;
    }

    @Override
    public List<Question> extractData(ResultSet rs) throws SQLException {
        Map<Long, Question> questionsMappedToQuestionId = new HashMap<>();
        while (rs.next()) {
            Long questionId = getQuestionIdFromRs(rs);
            Question questionToUse = questionsMappedToQuestionId.computeIfAbsent(questionId, idOfQuestion -> new Question()
                    .questionId(questionId)
                    .questionText(getQuestionTextFromRs(rs))
                    .correctAnswer(getCorrectAnswerFromRs(rs)));
            questionToUse.setIncorrectAnswers(addToListAndReturn(questionToUse.getIncorrectAnswers(), getIncorrectAnswerFromRs(rs)));
            questionsMappedToQuestionId.put(questionId, questionToUse);
        }
        return new ArrayList<>(questionsMappedToQuestionId.values());
    }

    protected long getQuestionIdFromRs(ResultSet rs) {
        long questionId;
        try {
            questionId = rs.getLong(SqlConstants.QUESTION_ID);
        } catch (SQLException e) {
            String errorMessage = String.format(ERROR_FORMAT_STR, "question id", SqlConstants.QUESTION_ID);
            throw new QuarkusDataAccessException(errorMessage, e);
        }
        logger.debug("Successfully read question id " + questionId);
        return questionId;
    }

    protected String getQuestionTextFromRs(ResultSet rs) {
        String questionText;
        try {
            questionText = rs.getString(SqlConstants.QUESTION_TEXT_KEY);
        } catch (SQLException e) {
            String errorMessage = String.format(ERROR_FORMAT_STR, "question text", SqlConstants.QUESTION_TEXT_KEY);
            throw new QuarkusDataAccessException(errorMessage, e);
        }
        logger.debug("Successfully read question text " + questionText);
        return questionText;
    }

    protected String getCorrectAnswerFromRs(ResultSet rs) {
        String correctAnswer;
        try {
            correctAnswer = rs.getString(SqlConstants.CORRECT_ANSWER_KEY);
        } catch (SQLException e) {
            String errorMessage = String.format(ERROR_FORMAT_STR, "correct answer text", SqlConstants.CORRECT_ANSWER_KEY);
            throw new QuarkusDataAccessException(errorMessage, e);
        }
        logger.debug("Successfully read correct answer " + correctAnswer);
        return correctAnswer;
    }

    protected String getIncorrectAnswerFromRs(ResultSet rs) {
        String incorrectAnswer;
        try {
            incorrectAnswer = rs.getString(SqlConstants.INCORRECT_ANSWER_KEY);
        } catch (SQLException e) {
            String errorMessage = String.format(ERROR_FORMAT_STR, "incorrect answer text", SqlConstants.INCORRECT_ANSWER_KEY);
            throw new QuarkusDataAccessException(errorMessage, e);
        }
        logger.debug("Successfully read incorrect answer " + incorrectAnswer);
        return incorrectAnswer;
    }

    protected <R> List<R> addToListAndReturn(List<R> list, R valueToAdd) {
        if (list != null) {
            list = new ArrayList<>();
        }
        list.add(valueToAdd);
        return list;
    }
}
