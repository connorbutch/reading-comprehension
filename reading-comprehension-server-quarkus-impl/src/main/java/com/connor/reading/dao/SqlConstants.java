package com.connor.reading.dao;

public abstract class SqlConstants {
    //assessment table
    public static final String FIRST_NAME_KEY = "AUTHOR_FIRST_NAME";
    public static final String LAST_NAME_KEY = "AUTHOR_LAST_NAME";
    public static final String CREATED_TEACHER_ID_KEY = "CREATED_TEACHER_ID";
    public static final String ISBN_KEY = "ISBN";
    public static final String NUMBER_OF_POINTS_KEY = "NUMBER_OF_POINTS";
    public static final String READING_LEVEL_KEY = "READING_LEVEL";
    public static final String TITLE_KEY = "TITLE";

    //Question table
    public static final String QUESTION_ID = "QUESTION_SID";
    public static final String QUESTION_TEXT_KEY = "QUESTION_TEXT";
    public static final String CORRECT_ANSWER_KEY = "CORRECT_ANSWER";
    public static final String INCORRECT_ANSWER_KEY = "INCORRECT_ANSWER_TEXT";

    //used in select all assessments
    public static final String LIMIT_PARAMETER = "LIMIT";
    public static final String OFFSET_PARAMETER = "OFFSET";

    //required because named template cannot insert same parameter twice
    public static final String SECOND_ISBN = "SAMEISBN";

    //used in select assessments and questions for assessment
    public static final String ASSESSMENT_COUNT_KEY = "ASSESSMENT_COUNT";
}
