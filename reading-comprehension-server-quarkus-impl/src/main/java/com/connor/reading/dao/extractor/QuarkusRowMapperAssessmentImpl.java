package com.connor.reading.dao.extractor;

import com.connor.quarkus.jdbc.QuarkusRowMapper;
import com.connor.reading.dao.SqlConstants;
import com.connor.reading.dto.Assessment;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import java.sql.ResultSet;
import java.sql.SQLException;

@Dependent
public class QuarkusRowMapperAssessmentImpl implements QuarkusRowMapper<Assessment> {
    private final Logger logger;

    QuarkusRowMapperAssessmentImpl(Logger logger){
        this.logger = logger;
    }

    @Override
    public Assessment mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Assessment()
                .authorFirstName(getFirstNameFromRs(rs))
                .authorLastName(getLastNameFromRs(rs))
                .createdTeacherId(getCreatedTeacherIdFromRs(rs))
                .isbn(getIsbnFromRs(rs))
                .numberOfPoints(getNumberOfPointsFromRs(rs))
                .readingLevel(getReadingLevelFromRs(rs))
                .title(getTitleFromRs(rs));
    }

    protected String getFirstNameFromRs(ResultSet rs) throws SQLException {
        String firstName = rs.getString(SqlConstants.FIRST_NAME_KEY);
        logger.debug("Successfully read first name " + firstName);
        return firstName;
    }

    protected String getLastNameFromRs(ResultSet rs) throws SQLException {
        String lastName = rs.getString(SqlConstants.LAST_NAME_KEY);
        logger.debug("Successfully read first name " + lastName);
        return lastName;
    }

    protected Integer getCreatedTeacherIdFromRs(ResultSet rs) throws SQLException {
        Integer createdTeacherId = rs.getObject(SqlConstants.CREATED_TEACHER_ID_KEY, Integer.class);
        logger.debug("Successfully read created teacher id " + createdTeacherId);
        return createdTeacherId;
    }

    protected Long getIsbnFromRs(ResultSet rs) throws SQLException {
        Long isbn = rs.getObject(SqlConstants.ISBN_KEY, Long.class);
        logger.debug("Successfully read isbn " + isbn);
        return isbn;
    }

    protected Float getNumberOfPointsFromRs(ResultSet rs) throws SQLException {
        Float numberOfPoints = rs.getFloat(SqlConstants.NUMBER_OF_POINTS_KEY);
        logger.debug("Successfully read number of points " + numberOfPoints);
        return numberOfPoints;
    }

    protected Float getReadingLevelFromRs(ResultSet rs) throws SQLException {
        Float readingLevel = rs.getFloat(SqlConstants.READING_LEVEL_KEY);
        logger.debug("Successfully read reading level " + readingLevel);
        return readingLevel;
    }

    protected String getTitleFromRs(ResultSet rs) throws SQLException {
        String title = rs.getString(SqlConstants.TITLE_KEY);
        logger.debug("Successfully read title " + title);
        return title;
    }
}
