package com.connor.reading.service;


import com.connor.reading.dao.AssessmentDao;
import com.connor.reading.dto.Assessment;
import com.connor.reading.dto.Question;
import com.connor.reading.exception.ReadingDuplicateResourceException;
import com.connor.reading.exception.ReadingNotFoundException;
import com.connor.reading.exception.ReadingSystemException;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.model.AssessmentCountWithListOfQuestion;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import javax.transaction.Transactional;
import java.util.List;

@Dependent
@Debug
public class AssessmentService {
    private static final String ASSESSMENT_NOT_FOUND_ERR_FORMAT_STR = "Assessment not found for isbn %d";
    private static final String QUESTION_NOT_FOUND_FORMAT_STR = "Question not found for question id %d";

    private final Logger logger;
    private final ValidationService validationService;
    private final AssessmentDao assessmentDao;

    AssessmentService(Logger logger, ValidationService validationService, AssessmentDao assessmentDao) {
        this.logger = logger;
        this.validationService = validationService;
        this.assessmentDao = assessmentDao;
    }

    public List<Assessment> getAssessments(Integer limit, Integer offset) {
        validationService.validateGetAllAssessments(limit, offset);
        List<Assessment> assessments;
        logger.debug("Limit " + limit + " and offset " + offset + " are valid");
        if (limit == null) {
            logger.debug("Limit is null, so getting all assessments");
            assessments = assessmentDao.getAllAssessments();
        } else {
            logger.debug("Limit is not null, so fetching only part of assessments");
            int offsetForDao = offset == null ? 0 : offset;
            assessments = assessmentDao.getAssessmentsForLimitAndOffset(limit, offsetForDao);
        }
        return assessments;
    }

    public int getTotalNumberOfAssessments() {
        return assessmentDao.getTotalNumberOfAssessments();
    }

    @Transactional(Transactional.TxType.REQUIRED)
    //MUST Use this transaction level to ensure rollback and to work with health check
    public Assessment addAssessment(Assessment assessment) {
        validationService.validateAddAssessment(assessment);

        /*
         * This is important.  While most applications will check here if the entity with a given key exists and throw a 409
         * based on that result, this is error prone in a highly concurrent environment, as the entity could have been
         * added in the meantime (since the check).  Hence, the more reliable, and faster way is to just make a single call
         * and have the database return whether or not it inserted.  Note, the only time the db should return false instead
         * of wrapping and throwing is for a dupl;icate key exception, not for any other exception.
         */

        if(!assessmentDao.addAssessment(assessment)){
            throw new ReadingDuplicateResourceException();
        }
        logger.debug("Successfully added assessment, now to retrieve it by isbn to verify get will work . . . " + assessment.getIsbn());
        try {
            return getAssessmentByIsbn(assessment.getIsbn());
        } catch (Exception e) {
            throw new ReadingSystemException("There was an unexpected error when fetching from the database after inserting an assessment", e);
        }
    }

    public Assessment getAssessmentByIsbn(Long isbn) {
        validationService.validateGetAssessmentByIsbn(isbn);
        logger.debug("Get assessment by isbn " + isbn + " is valid");
        return assessmentDao.getAssessmentByIsbn(isbn)
                .orElseThrow(() -> new ReadingNotFoundException(String.format(ASSESSMENT_NOT_FOUND_ERR_FORMAT_STR, isbn)));
    }

    public List<Question> getQuestionsForIsbn(Long isbn) {
        validationService.validateGetQuestionsForIsbn(isbn);

        /*
         * This is important.  While most applications will check here if an assessment exists for a given isbn, and throw a 404
         * if it does not exist, this leaves us open to returning the wrong status in a highly concurrent application (say we make
         * our check, then another thread deletes it in the meantime).  This could be avoided with locking, but that is less
         * performant and requires multiple database trips.  This shows how to make the application safe for high
         * levels of concurrency by returning the values in a single query.
         */

        AssessmentCountWithListOfQuestion assessmentCountWithListOfQuestion = assessmentDao.getQuestionsForIsbn(isbn);
        if(!assessmentCountWithListOfQuestion.doesAssessmentExist()){
            throw new ReadingNotFoundException(String.format(ASSESSMENT_NOT_FOUND_ERR_FORMAT_STR, isbn));
        }
        logger.debug("Assessment with isbn " + isbn + " exists.  We will now return the questions associated with it");
        return assessmentCountWithListOfQuestion.getQuestionsAssociatedWithAssessment();
    }

    @Transactional(Transactional.TxType.REQUIRED)
    //MUST Use this transaction level to ensure rollback and to work with health check
    public Question addQuestionForAssessment(Long isbn, Question question) {
        validationService.validateAddQuestionForAssessment(isbn, question);
        long createdQuestionId = assessmentDao.addQuestion(isbn, question);
        logger.debug("Successfully added question " + question + " for isbn " + isbn + " we will now try to retrieve by isbn and question id or rollback");
        try {
            return getQuestionById(isbn, createdQuestionId);
        } catch (Exception e) {
            throw new ReadingSystemException("There was an unexpected error when fetching from the database after inserting a question", e);
        }
    }

    public Question getQuestionById(Long isbn, Long questionId) {
        validationService.validateGetQuestionById(isbn, questionId);
        return assessmentDao.getQuestionById(isbn, questionId)
                .orElseThrow(() -> new ReadingNotFoundException(String.format(QUESTION_NOT_FOUND_FORMAT_STR, questionId)));
    }
}
