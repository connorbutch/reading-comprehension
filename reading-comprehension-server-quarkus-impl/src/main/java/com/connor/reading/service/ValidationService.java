package com.connor.reading.service;

import com.connor.reading.bean.HealthCheckBean;
import com.connor.reading.cdi.GeneratedClient;
import com.connor.reading.dto.Assessment;
import com.connor.reading.dto.Question;
import com.connor.reading.exception.ReadingBadRequestException;
import com.connor.reading.exception.ReadingSystemException;
import com.connor.reading.interceptor.Debug;
import com.connor.reading.model.ErrorEnum;
import com.connor.reading.util.TeachersApiFactory;
import com.connor.teacher.ApiException;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.connor.reading.model.ErrorEnum.*;


@Dependent
@Debug
public class ValidationService {
    //acceptance test-related
    public static final long DISALLOWED_ISBN = 1234L; //used for acceptance tests
    public static final long ISBN_THAT_CANNOT_BE_DELETED = 9781976530739L;

    //assessment-related
    public static final int MINIMUM_ALLOWED_LIMIT = 1;
    public static final int MINIMUM_ALLOWED_OFFSET = 0;
    public static final float MINIMUM_ALLOWED_NUMBER_OF_POINTS = 0.5F;
    public static final float MAXIMUM_ALLOWED_NUMBER_OF_POINTS = 125F;
    public static final float MAXIMUM_ALLOWED_READING_LEVEL = 12.0F;
    public static final float MINIMUM_ALLOWED_READING_LEVEL = 0.5F;
    public static final int MINIMUM_ALLOWED_ISBN = 1;
    public static final int MAXIMUM_FIRST_NAME_LENGTH = 32;
    public static final int MAXIMUM_LAST_NAME_LENGTH = 32;
    public static final int MAXIMUM_TITLE_LENGTH = 128;

    //errorInfo-related
    public static final int MINIMUM_ALLOWED_ERROR_ID = 0;

    //question-related
    public static final long MINIMUM_ALLOWED_QUESTION_ID = 1L;
    public static final int MAXIMUM_QUESTION_TEXT_LENGTH = 256;
    public static final int MINIMUM_NUMBER_OF_INCORRECT_ANSWERS = 1;
    public static final int MAXIMUM_NUMBER_OF_INCORRECT_ANSWERS = 5;
    public static final int MAXIMUM_ALLOWED_INCORRECT_ANSWER_LENGTH = 256;
    public static final int MAXIMUM_ALLOWED_CORRECT_ANSWER_LENGTH = 256;

    private final Logger logger;
    private final HealthCheckBean healthCheckBean;
    private final TeachersApiFactory teachersApiFactory;

    ValidationService(Logger logger, HealthCheckBean healthCheckBean, @GeneratedClient TeachersApiFactory teachersApiFactory) {
        this.logger = logger;
        this.healthCheckBean = healthCheckBean;
        this.teachersApiFactory = teachersApiFactory;
    }

    public void validateGetAllAssessments(Integer limit, Integer offset) {
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if (limit != null && limit < MINIMUM_ALLOWED_LIMIT) {
            errorEnums.add(INVALID_LIMIT);
        }
        if (offset != null && offset < MINIMUM_ALLOWED_OFFSET) {
            errorEnums.add(INVALID_OFFSET);
        }
        throwExceptionIfNeeded(errorEnums);
        logger.debug("For get all assessments, limit " + limit + " and offset " + offset + " are valid");
    }

    public void validateAddAssessment(Assessment assessment) {
        boolean isHealthCheck = healthCheckBean.isHealthCheck();
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if (assessment == null) {
            errorEnums.add(ASSESSMENT_REQUIRED);
        } else {
            errorEnums.addAll(validateIsbn(assessment.getIsbn(), isHealthCheck));
            errorEnums.addAll(validateAuthorFirstName(assessment.getAuthorFirstName()));
            errorEnums.addAll(validateAuthorLastName(assessment.getAuthorLastName()));
            errorEnums.addAll(validateTitle(assessment.getTitle()));
            errorEnums.addAll(validateCreatedTeacherId(assessment.getCreatedTeacherId()));
            errorEnums.addAll(validateNumberOfPoints(assessment.getNumberOfPoints()));
            errorEnums.addAll(validateReadingLevel(assessment.getReadingLevel()));
        }
        throwExceptionIfNeeded(errorEnums);
        logger.debug("Adding assessment " + assessment + " and is health check " + isHealthCheck + " is valid");
    }

    public void validateGetAssessmentByIsbn(Long isbn) {
        boolean isHealthCheck = healthCheckBean.isHealthCheck();
        List<ErrorEnum> errorEnums = new ArrayList<>(validateIsbn(isbn, isHealthCheck));
        throwExceptionIfNeeded(errorEnums);
        logger.debug("Get assessment by isbn " + isbn + " and is health check " + isHealthCheck + " is valid");
    }

    public void validateGetErrorById(Integer errorIdToSearchOn) {
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if (errorIdToSearchOn == null) {
            errorEnums.add(ERROR_ID_REQUIRED);
        } else if (errorIdToSearchOn < MINIMUM_ALLOWED_ERROR_ID) {
            errorEnums.add(INVALID_ERROR_ID);
        }
        throwExceptionIfNeeded(errorEnums);
        logger.debug("Get error by id " + errorIdToSearchOn + " is valid");
    }

    public void validateGetQuestionsForIsbn(Long isbn) {
        List<ErrorEnum> errorEnums = new ArrayList<>(validateIsbn(isbn, false));
        throwExceptionIfNeeded(errorEnums);
        logger.debug("Get questions by isbn " + isbn + " is valid");
    }

    public void validateAddQuestionForAssessment(Long isbn, Question question) {
        boolean isHealthCheck = healthCheckBean.isHealthCheck();
        List<ErrorEnum> errorEnums = new ArrayList<>(validateIsbn(isbn, isHealthCheck));
        if (question == null) {
            errorEnums.add(QUESTION_REQUIRED);
        } else {
            errorEnums.addAll(validateQuestionId(question.getQuestionId(), true));
            errorEnums.addAll(validateCorrectAnswer(question.getCorrectAnswer()));
            errorEnums.addAll(validateIncorrectAnswers(question.getIncorrectAnswers()));
            errorEnums.addAll(validateQuestionText(question.getQuestionText()));
        }
        throwExceptionIfNeeded(errorEnums);
        logger.debug("Add question " + question + " for isbn " + isbn + " is valid");
    }

    public void validateGetQuestionById(Long isbn, Long questionId) {
        boolean isHealthCheck = healthCheckBean.isHealthCheck();
        List<ErrorEnum> errorEnums = validateIsbn(isbn, isHealthCheck);
        errorEnums.addAll(validateQuestionId(questionId, false));
        throwExceptionIfNeeded(errorEnums);
        logger.debug("Get question by isbn " + isbn + " and question id " + questionId + " is valid");
    }

    protected List<ErrorEnum> validateIsbn(Long isbn, boolean isHealthCheck) {
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if (isbn == null) {
            errorEnums.add(ISBN_REQUIRED);
        } else {
            if (!isHealthCheck && DISALLOWED_ISBN == isbn) {
                errorEnums.add(ISBN_DISALLOWED);
            }
            if (isbn < MINIMUM_ALLOWED_ISBN) {
                errorEnums.add(INVALID_ISBN);
            }
        }
        return errorEnums;
    }

    protected List<ErrorEnum> validateAuthorFirstName(String authorFirstName) {
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if (authorFirstName == null) {
            errorEnums.add(FIRST_NAME_REQUIRED);
        } else if (authorFirstName.isBlank()) {
            errorEnums.add(FIRST_NAME_NOT_BLANK);
        } else if (authorFirstName.length() > MAXIMUM_FIRST_NAME_LENGTH) {
            errorEnums.add(FIRST_NAME_EXCEEDS_LENGTH);
        }
        return errorEnums;
    }

    protected List<ErrorEnum> validateAuthorLastName(String authorLastName) {
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if (authorLastName == null) {
            errorEnums.add(LAST_NAME_REQUIRED);
        } else if (authorLastName.isBlank()) {
            errorEnums.add(LAST_NAME_NOT_BLANK);
        } else if (authorLastName.length() > MAXIMUM_LAST_NAME_LENGTH) {
            errorEnums.add(LAST_NAME_EXCEEDS_LENGTH);
        }
        return errorEnums;
    }

    protected List<ErrorEnum> validateTitle(String title) {
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if (title == null) {
            errorEnums.add(TITLE_REQUIRED);
        } else if (title.isBlank()) {
            errorEnums.add(TITLE_NOT_BLANK);
        } else if (title.length() > MAXIMUM_TITLE_LENGTH) {
            errorEnums.add(TITLE_EXCEEDS_LENGTH);
        }
        return errorEnums;
    }

    protected List<ErrorEnum> validateCreatedTeacherId(Integer createdTeacherId) {
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if (createdTeacherId == null) {
            errorEnums.add(CREATED_TEACHER_ID_REQUIRED);
        } else {
            errorEnums.addAll(validateTeacherIdByMakingRequestToTeacherService(createdTeacherId));
        }
        return errorEnums;
    }

    protected List<ErrorEnum> validateNumberOfPoints(Float numberOfPoints) {
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if (numberOfPoints == null) {
            errorEnums.add(NUMBER_OF_POINTS_REQUIRED);
        } else if (numberOfPoints < MINIMUM_ALLOWED_NUMBER_OF_POINTS || numberOfPoints > MAXIMUM_ALLOWED_NUMBER_OF_POINTS) {
            errorEnums.add(INVALID_NUMBER_OF_POINTS);
        }
        return errorEnums;
    }

    protected List<ErrorEnum> validateReadingLevel(Float readingLevel) {
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if (readingLevel == null) {
            errorEnums.add(READING_LEVEL_REQUIRED);
        } else if (readingLevel < MINIMUM_ALLOWED_READING_LEVEL || readingLevel > MAXIMUM_ALLOWED_READING_LEVEL) {
            errorEnums.add(INVALID_READING_LEVEL);
        }
        return errorEnums;
    }

    protected List<ErrorEnum> validateQuestionId(Long questionId, boolean isCreate) {
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if (isCreate) {
            if (questionId != null) {
                errorEnums.add(QUESTION_ID_PROVIDED_WHEN_NOT_ALLOWED);
            }
        } else {
            if (questionId == null) {
                errorEnums.add(QUESTION_ID_REQUIRED);
            }else if(questionId < MINIMUM_ALLOWED_QUESTION_ID){
                errorEnums.add(INVALID_QUESTION_ID);
            }
        }
        return errorEnums;
    }

    protected List<ErrorEnum> validateQuestionText(String questionText) {
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if(questionText == null){
            errorEnums.add(QUESTION_TEXT_REQUIRED);
        }else if(questionText.isBlank()){
            errorEnums.add(QUESTION_TEXT_NOT_BLANK);
        }else if(questionText.length() > MAXIMUM_QUESTION_TEXT_LENGTH){
            errorEnums.add(QUESTION_TEXT_EXCEEDS_LENGTH);
        }
        return errorEnums;
    }

    protected List<ErrorEnum> validateIncorrectAnswers(List<String> incorrectAnswers) {
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if(incorrectAnswers == null){
            errorEnums.add(INCORRECT_ANSWERS_REQUIRED);
        }else{
             if(incorrectAnswers.isEmpty()){
                errorEnums.add(INCORRECT_ANSWERS_NOT_EMPTY);
            }else if(incorrectAnswers.size() >= MAXIMUM_NUMBER_OF_INCORRECT_ANSWERS){
                errorEnums.add(INCORRECT_ANSWERS_EXCEEDS_MAX);
            }
            errorEnums.addAll(incorrectAnswers.stream()
                    .map(this::validateIncorrectAnswer)
                    .reduce(new ArrayList<>(), (oldList, newList) -> Stream.of(oldList, newList)
                            .flatMap(Collection::stream)
                            .collect(Collectors.toList())));
        }
        return errorEnums;
    }

    protected List<ErrorEnum> validateCorrectAnswer(String correctAnswer) {
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if(correctAnswer == null){
            errorEnums.add(CORRECT_ANSWER_REQUIRED);
        }else if(correctAnswer.isBlank()){
            errorEnums.add(CORRECT_ANSWER_NOT_BLANK);
        }else if(correctAnswer.length() > MAXIMUM_ALLOWED_CORRECT_ANSWER_LENGTH){
            errorEnums.add(CORRECT_ANSWER_EXCEEDS_MAX);
        }
        return errorEnums;
    }

    protected List<ErrorEnum> validateIncorrectAnswer(String incorrectAnswer){
        List<ErrorEnum> errorEnums = new ArrayList<>();
        if(incorrectAnswer == null){
            errorEnums.add(INCORRECT_ANSWERS_REQUIRED);
        }else if(incorrectAnswer.isBlank()){
            errorEnums.add(INCORRECT_ANSWERS_NOT_EMPTY);
        }else if(incorrectAnswer.length() > MAXIMUM_ALLOWED_INCORRECT_ANSWER_LENGTH){
            errorEnums.add(INCORRECT_ANSWERS_EXCEEDS_MAX);
        }
        return errorEnums;
    }

    protected List<ErrorEnum> validateTeacherIdByMakingRequestToTeacherService(Integer createdTeacherId){
        List<ErrorEnum> errorEnums = new ArrayList<>();
        try {
            teachersApiFactory.get().getTeacherById(Long.valueOf(createdTeacherId));
        }catch (ApiException e){
            handleApiExceptionGettingTeacherById(e, createdTeacherId, errorEnums);
        }
        logger.info("Successfully made a request for a valid teacher api for id " + createdTeacherId + " This means the teacher id is valid");
        return errorEnums;
    }

    protected void handleApiExceptionGettingTeacherById(ApiException e, Integer createdTeacherId, List<ErrorEnum> errorEnums) {
        if(e.getCode() == Response.Status.BAD_REQUEST.getStatusCode()){
            errorEnums.add(INVALID_CREATED_TEACHER);
        }else if(e.getCode() == Response.Status.NOT_FOUND.getStatusCode()){
            errorEnums.add(TEACHER_ID_DOES_NOT_EXIST);
        }else{
            String errorMessage = String.format("There was a (non-4xx) error when making a request to the teacher api for created teacher id %d.  The response status code was %d and the response body was %s", createdTeacherId, e.getCode(), e.getResponseBody());
            throw new ReadingSystemException(errorMessage, e);
        }
    }

    protected void throwExceptionIfNeeded(List<ErrorEnum> errorEnums) {
        if (errorEnums != null && !errorEnums.isEmpty()) {
            throw new ReadingBadRequestException(errorEnums);
        }
    }
}
