package com.connor.reading.service;

import com.connor.reading.cdi.HealthCheck;
import com.connor.reading.dto.DownstreamDependency;
import com.connor.reading.dto.ReadinessHealthResponse;
import com.connor.reading.exception.ReadingServiceUnavailableException;
import com.connor.reading.health.api.HealthCheckName;
import com.connor.reading.health.api.HealthChecker;
import com.connor.reading.interceptor.Debug;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Dependent
@Debug
public class HealthService {
    private static final long DEFAULT_MAX_TIMEOUT_WHEN_NOT_SET = 5000;

    private final Logger logger;
    private final List<HealthChecker> healthCheckersThatCanBeRanOnExecutor;
    private final ManagedExecutor managedExecutor;
    private final long maxAllowedTimeoutInMs;
    private final List<HealthChecker> healthCheckersThatMustRunOnSameThread;

    HealthService(Logger logger, @HealthCheck List<HealthChecker> healthCheckersThatCanBeRanOnExecutor, ManagedExecutor managedExecutor, @ConfigProperty(name = "healthcheck.timeout", defaultValue="5000")String maxAllowedTimeoutInMs, @HealthCheck(mustRunOnSameThread = true) List<HealthChecker> healthCheckersThatMustRunOnSameThread){
        this.logger = logger;
        this.healthCheckersThatCanBeRanOnExecutor = Collections.unmodifiableList(healthCheckersThatCanBeRanOnExecutor);
        this.managedExecutor = managedExecutor;
        this.maxAllowedTimeoutInMs = getMaxAllowedTimeoutInMsAsLong(maxAllowedTimeoutInMs);
        this.healthCheckersThatMustRunOnSameThread = Collections.unmodifiableList(healthCheckersThatMustRunOnSameThread);
    }

    public ReadinessHealthResponse checkDownstreamDependencies(Integer timeoutInMsBigDecimal){
        long timeoutInMs = getTimeoutInMsAsLong(timeoutInMsBigDecimal);
        List<DownstreamDependency> allDependencyStatuses = runHealthChecksInParallel(timeoutInMs);
        validateHealthCheckResponses(allDependencyStatuses);
        allDependencyStatuses.sort(Comparator.comparing(DownstreamDependency::getName));
        return new ReadinessHealthResponse()
                .availableDependencies(allDependencyStatuses)
                .unavailableDependencies(Collections.emptyList())
                .isAvailable(true);
    }

    protected long getMaxAllowedTimeoutInMsAsLong(String maxAllowedTimeoutInMs) {
        long maxAllowedTimeoutInMsAsLong = DEFAULT_MAX_TIMEOUT_WHEN_NOT_SET;
        try{
            maxAllowedTimeoutInMsAsLong = Long.parseLong(maxAllowedTimeoutInMs);
        }catch(NumberFormatException e){
            logger.warn("Please set a valid timeout value (a valid long).  The value passed was " + maxAllowedTimeoutInMs);
        }
        return maxAllowedTimeoutInMsAsLong;
    }

    protected long getTimeoutInMsAsLong(Integer timeoutInMsInteger){
        return Optional.ofNullable(timeoutInMsInteger)
                .map(timeoutValue -> (long) timeoutValue)
                .filter(timeoutValue -> timeoutValue > getMaxAllowedTimeoutInMs())
                .orElseGet(this::getMaxAllowedTimeoutInMs);
    }

    protected List<DownstreamDependency> runHealthChecksInParallel(long timeoutInMs){
        //first we get the names, so even if the check availability times out, we have the names for the requests
        List<DownstreamDependency> dependenciesWithNamesOnly = healthCheckersThatCanBeRanOnExecutor.stream()
                .map(Object::getClass)
                .map(this::getHealthCheckerName)
                .map(healthCheckName -> new DownstreamDependency().name(healthCheckName))
                .collect(Collectors.toList());

        Collection<Callable<Boolean>> healthStatusesCallables = healthCheckersThatCanBeRanOnExecutor.stream()
                .map(healthChecker -> (Callable<Boolean>) healthChecker::get)
                .collect(Collectors.toList());;

        //get the availabilities, which can time out.  In case of timeout, we default to false (for now)
        List<Boolean> availabilities = executeChecksInParallel(healthStatusesCallables, timeoutInMs)
                .stream()
                .map(this::getBooleanFromExecution)
                .collect(Collectors.toList());

        //get the names of health checks that cannot be ran on the executor
        List<DownstreamDependency> downstreamDependenciesThatMustRunOnSameThread = healthCheckersThatMustRunOnSameThread.stream()
                .map(Object::getClass)
                .map(this::getHealthCheckerName)
                .map(healthCheckName -> new DownstreamDependency().name(healthCheckName))
                .collect(Collectors.toList());

        //execute the health checks that MUST execute on the same thread (JTA reasons most likely) and collect the results
        List<Boolean> healthStatusesFromSameThread = healthCheckersThatMustRunOnSameThread.stream().map(HealthChecker::get).collect(Collectors.toList());

        //combine the names and statuses of dependencies that run on same thread
        setStatusesOnDependencies(downstreamDependenciesThatMustRunOnSameThread, healthStatusesFromSameThread);

        //combine the names and statuses and return the resulting list
        return Stream.of(setStatusesOnDependencies(dependenciesWithNamesOnly, availabilities), setStatusesOnDependencies(downstreamDependenciesThatMustRunOnSameThread, healthStatusesFromSameThread))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    protected void validateHealthCheckResponses(List<DownstreamDependency> dependencyStatuses) {
        if(dependencyStatuses.stream().anyMatch(downstreamDependency -> downstreamDependency == null || !downstreamDependency.getIsAvailable())){
          throw new ReadingServiceUnavailableException(dependencyStatuses);
        }
    }

    protected <T> List<Future<T>> executeChecksInParallel(Collection<Callable<T>> callables, long timeoutInMs){
        List<Future<T>> futuresOfT = new ArrayList<>(callables.size());
        try {
            futuresOfT = managedExecutor.invokeAll(callables, timeoutInMs, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return futuresOfT;
    }

    protected List<DownstreamDependency> setStatusesOnDependencies(List<DownstreamDependency> dependencies, List<Boolean> availabilities){
        for(int i = 0; i < dependencies.size(); ++i){
            dependencies.get(i).setIsAvailable(availabilities.get(i));
        }
        return dependencies;
    }

    protected boolean getBooleanFromExecution(Future<Boolean> booleanFuture){
        boolean isAvailable = false;
        try {
            isAvailable = booleanFuture.get();
        } catch (InterruptedException e) {
           Thread.currentThread().interrupt();
           logger.debug("Thread was interrupted when checking health.  This will make service show as down", e);
        } catch (ExecutionException e) {
            logger.warn("An uncaught exception occurred when checking health of dependency.  This will cause it to show as down.", e);
        }catch(CancellationException e){
            logger.warn("Time ran out when executing health check.  This will cause dependency to show as down.", e);
        }
        return isAvailable;
    }

    protected String getHealthCheckerName(Class<?> clazz){
        return Optional.ofNullable(clazz.getAnnotation(HealthCheckName.class))
                .map(HealthCheckName::healthCheckName)
                .filter(Objects::nonNull) //not sure if i need this, but just in case
                .filter(healthCheckName -> !healthCheckName.isBlank())
                .orElseGet(clazz::getName);
    }

    protected long getMaxAllowedTimeoutInMs(){
        return maxAllowedTimeoutInMs;
    }
}
