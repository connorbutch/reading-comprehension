package com.connor.reading.service;

import com.connor.reading.dao.ErrorDao;
import com.connor.reading.dto.DetailedErrorInfo;
import com.connor.reading.exception.ReadingNotFoundException;
import org.jboss.logging.Logger;

import javax.enterprise.context.Dependent;

@Dependent
public class ErrorService {
    private static final String ERROR_NOT_FOUND_FORMAT_STR = "Error not found with id %d";

    private final Logger logger;
    private final ValidationService validationService;
    private final ErrorDao errorDao;

    ErrorService(Logger logger, ValidationService validationService, ErrorDao errorDao) {
        this.logger = logger;
        this.validationService = validationService;
        this.errorDao = errorDao;
    }

    public DetailedErrorInfo getErrorReasonById(Integer errorIdToSearchOn){
        validationService.validateGetErrorById(errorIdToSearchOn);
        logger.debug("Get error reason by id " + errorIdToSearchOn + " is valid (NOTE: this doesn't mean it exists)");
        return errorDao.getDetailedErrorInfoById(errorIdToSearchOn)
                .orElseThrow(() -> new ReadingNotFoundException(String.format(ERROR_NOT_FOUND_FORMAT_STR, errorIdToSearchOn)));
    }
}
