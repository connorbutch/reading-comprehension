package com.connor.reading.query;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"}, features = {"src/integration-test/resources/features/GetAllAssessments.feature"})
public class RunGetAllAssessmentsIT {
}
