# reading-comprehension-server-quarkus-impl
## Motivation
Containers/container orchestration software (and tools built on top of them) allow for many benefits.  Serverless also allows
for many benefits.  In order to reap the benefits of both, we would like our application to run on knative.  In order to do this,
our application should have a small memory footprint, so we chose to implement a quarkus implementation of our api.

## Functionality
This module is a webservice that can be deployed as a native image with incredibly fast startup time, while maintaining benefits of java.

## Technologies Involved
This project uses various technologies, some of which include:
- java (11)
- gradle
- open api specification
- code generation
- jaxrs

## Usage Instructions
1. Build the project with gradle (gradle clean build)  
2. (Optional)Run the application in dev mode (command will be listed here)
3. Build the docker image, which will build the application as a native image for linux using graalvm (run ./build.sh)
4. (Optional) Run the image locally using Docker (command will be listed here) * NOTE: this will be impossible once a database is required, when you should skip to the next step *
5. (Optional) Run the image in docker-compose (command will be listed here)
6. Run the integration tests against the running image using gradle (gradle integrationTest -DBASE_URL=<baseurl here, likely localhost>)

## From the README.md from quarkus (ported here)
### Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```
./gradlew quarkusDev
```

### Packaging and running the application

The application can be packaged using `./gradlew quarkusBuild`.
It produces the `reading-comprehension-quarkus-server-impl-1.0.0-SNAPSHOT-runner.jar` file in the `build` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `build/lib` directory.

The application is now runnable using `java -jar build/reading-comprehension-quarkus-server-impl-1.0.0-SNAPSHOT-runner.jar`.

If you want to build an _über-jar_, just add the `--uber-jar` option to the command line:
```
./gradlew quarkusBuild --uber-jar
```

### Creating a native executable

You can create a native executable using: `./gradlew build -Dquarkus.package.type=native`.

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: `./gradlew build -Dquarkus.package.type=native -Dquarkus.native.container-build=true`.

You can then execute your native executable with: `./build/reading-comprehension-quarkus-server-impl-1.0.0-SNAPSHOT-runner`

## General Notes
As always, feedback is greatly appreciated, so feel free to visit my youtube channel, or email me with feedback.
